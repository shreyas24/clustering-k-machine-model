\section{Technical Preliminaries}
\label{section:preliminaries}

Since the input metric is only implicitly provided, as an edge-weighted graph, computing shortest path distances to learn
parts of the metric space turns out to be a key element of our algorithms.
The \textit{Single Source Shortest Path (SSSP)} problem has been considered in the $k$-machine model in Klauck et al.~\cite{KlauckNPRSODA15}
%They consider two versions of the problem; the \textit{distance} version in which the output consists only of shortest path distances
%to the source and the \textit{tree} version in which the output also includes a shortest path tree.
%(Here we only need the distance version of the problem.)
%For both versions of the problem Klauck et al.~ 
and they describe a $(1+\epsilon)$-approximation algorithm that runs in the $k$-machine model in 
$\to(n/\sqrt{k})$ rounds.
This is too slow for our purpose, since we are looking for an overall running time of $\to(n/k)$.
We instead turn to a recent result of Becker at al.~\cite{BeckerKKLdisc17} and using this we can easily obtain an $\to(n/k)$-round SSSP
algorithm.
Becker et al.~do not work in the $k$-machine model; their result relevant to us is in the 
\textit{Broadcast Congested Clique} model.
Informally speaking, the \textit{Congested Clique} model can be thought of as a 
special case of the $k$-machine model with $k = n$.
The \textit{Broadcast Congested Clique} model imposes the additional restriction on communication
that in each round each machine sends the \textit{same} message (i.e., broadcasts) to the 
remaining $n-1$ machines.
For a precise and detailed description of the Congested Clique models, see \cite{HegemanPPSSPODC15,DruckerKOPODC2014}.

%While Becker et al.~do not work in the $k$-machine model, they present the following result that is useful to us.
\begin{theorem} \textbf{(Becker et al.~\cite{BeckerKKLdisc17})}
For any $0 < \epsilon \le  1$, in the Broadcast Congested Clique model, a deterministic $(1 + \epsilon)$-approximation to 
the SSSP problem in undirected graphs with non-negative edge-weights can be computed in 
	$\mbox{poly}\,(\log n)/\mbox{poly}\,(\epsilon)$ rounds. 
\end{theorem}
It is easy to see that any Broadcast Congested Clique algorithm that runs in $T$ rounds can be simulated in the $k$-machine model in
$T \cdot \to(n/k)$ rounds. 
%This is because each of the $T$ Congested Clique rounds can be simulated in $\to(n/k)$ rounds 
%in the $k$-machine model.
%In this simulation, each machine will compute and communicate on behalf of each of the vertices it hosts, sequentially.
%However, all $k$ machines will do this in parallel. Thus, a Congested Clique round can be simulated in the $k$-machine
%model is as many rounds as the maximum number of vertices that a machine hosts. Since this is $\to(n/k)$ w.h.p.~the claim 
%follows.
A more general version of this claim is proved in Klauck et al.~in the Conversion Theorem (Theorem 4.1 \cite{KlauckNPRSODA15}).
This leads to the following result about the SSSP problem in the $k$-machine model.
\begin{corollary} 
\label{theorem:SSSP}
For any $0 < \epsilon \le  1$, there is a deterministic $(1 + \epsilon)$-approximation algorithm in the $k$-machine model for solving 
the SSSP problem in undirected graphs with non-negative edge-weights in
	$O((n/k) \cdot \mbox{poly}(\log n)/\mbox{poly}(\epsilon))$ rounds.
\end{corollary} 

%We denote the approximate distances returned by the SSSP algorithm with the notation \(\widetilde{dist}\). When we need the size to query \(| \Gamma_{\le d}(v) \cap W |\) for some vertex \(v\) and \(W \subseteq V\), we will get a size estimate with respect to \(\widetilde{dist}\) and not the actual distances.

In addition to SSSP, our clustering algorithms require an efficient solution to a more general problem that we
call \textit{Multi-Source Shortest Paths} (in short, MSSP).
The input to MSSP is an edge-weighted graph $G = (V, E)$, with non-negative edge-weights, and a set $T \subseteq V$ of sources.
The output is required to be, for each vertex $v$, the distance $d(v, T)$ (i.e., $\min\{d(v, u) \mid u \in T\}$) 
and the vertex $v^* \in T$ that realizes this distance.
The following lemma uses ideas from Thorup \cite{Thorup2001} to show that MSSP can be reduced to a single call to SSSP 
and can be solved in an approximate sense in the $k$-machine model in $\to(n/k)$ rounds. 

\begin{lemma} \label{lemma:MSSP}
Given a set \(T \subseteq V\) of sources known to the machines (i.e., each machine $m_j$ knows $T \cap H(m_j)$), we can, for any value $0 \le \epsilon \le 1$,
compute a $(1 + \epsilon)$-approximation to MSSP in $\to(1/\mbox{poly}(\epsilon) \cdot n/k)$ rounds, w.h.p.
Specifically, after the algorithm has ended, for each $v \in V \setminus T$, the machine $m_j$ that hosts $v$ knows a pair
$(u, \tilde{d}) \in T \times \mathbb{R}^+$, such that
	\(d(v, u) \le \tilde{d} \le (1+\epsilon) \cdot d(v, T)\).
\end{lemma}
\begin{proof}
	  First, as in \cite{Thorup2001}, we add a dummy source vertex $s$, and connecting $s$ to each vertex $u \in T$ by $0$-weight edges. The shortest path distance from $s$ to any other vertex $v \in V$, is same as $d(v, T)$ in the original graph. 
This dummy source can be hosted by an arbitrary machine and the edge information can be exchanged in \(\to(n/k)\) rounds % Furthermore, consider the shortest path tree rooted at $s$. For any vertex $v$, the vertex $u \in U$ that realizes $d(v, U)$ (in the original graph), is the second-to-last vertex on the unique path in this tree. Note that the vertices of $U$ belong to the first level of this tree.

	     Using Theorem \ref{theorem:SSSP}, we can compute approximate shortest path distance $\tilde{d}$ that satisfies the first property of the lemma, in $\tilde{O}(n/k)$ rounds. By \cite{BeckerKKLdisc17} (section 2.3) we can compute an approximate shortest path tree in addition to approximate distances in the Broadcast Congested Clique in $O(\mbox{poly}(\log n)/\mbox{poly}\,(\epsilon))$ rounds w.h.p.~and hence in the \(k\)-machine model in $\tilde{O}(1/\mbox{poly}(\epsilon) \cdot n/k)$ rounds w.h.p.

	Since a tree contains linear (in $n$) number of edges, all machines 
	can exchange this information in $\to(n/k)$ rounds so that every machine knows the computed approximate shortest path tree. Now, each machine $m_j$ can determine locally, for each vertex $v \in H(m_j)$ the vertex $u \in T$ which satisfies the properties stated in the lemma.
	   %Thus, the second property of the lemma is also satisfied. However the vertex $v$ does not yet know the ``approximate nearest neighbor'' $u$.
	   % In the following, we show that this can be computed in $O(\log n)$ more SSSP computations. For this, we use an idea that was used by Thorup in \cite{Thorup2001} for a related problem.
\end{proof}

Note that in the solution to MSSP, for each $v \in T$, $d(v, T) = 0$.
For our algorithms, we also need the solution to a variant of MSSP that we call \textsc{ExclusiveMSSP} in which
for each $v \in T$, we are required to output $d(v, T\setminus\{v\})$ and the vertex $u^* \in T \setminus \{v\}$ that realizes this
distance. 
The following lemma uses ideas from Thorup \cite{Thorup2001} to show that \textsc{ExclusiveMSSP} can 
be solved by making $O(\log n)$ calls to a subroutine that solves SSSP.
\begin{lemma} \label{lemma:ExclusiveMSSP}
Given a set \(T \subseteq V\) of sources known to the machines (i.e., each machine $m_j$ knows $T \cap H(m_j)$), we can, for any value $0 \le \epsilon \le 1$,
compute a $(1 + \epsilon)$-approximation to \textsc{ExclusiveMSSP} in $\to(1/\mbox{poly}\,(\epsilon) \cdot n/k)$ rounds, w.h.p.
Specifically, after the algorithm has ended, for each $v \in T$, the machine $m_j$ that hosts $v$ knows a pair
	$(u, \tilde{d}) \in T \setminus \{v\} \times \mathbb{R}^+$, such that $d(v, u) \le \tilde{d} \le (1+\epsilon) \cdot d(v, T \setminus \{v\})$.
\end{lemma}

\begin{proof}
  Breaking ties by machine ID, each vertex in \(T\) is assigned a \(\log |T|\) size bit vector. We create \(2\log |T|\) subsets of \(T\) by making two sets \(T_i^0\) and \(T_i^1\) for each bit position \(i\). The set \(T_i^b\) contains vertices whose \(i^{th}\) bit value is \(b\). Note that for all pairs of vertices \(v, w\), there is at least one set \(T_i^b\) such that \(v \in T_i^b\) and \(w \notin T_i^b\). Now we run an MSSP algorithm for each \(T_i^b\) using lemma \ref{lemma:MSSP}. Now for each vertex \(v \in T\) \(\tilde{d}\) is the smallest \(d(v, T_i^b)\) such that \(v \notin T_i^b\) and the vertex \(u\) is an arbitrary vertex that realizes the distance \(\tilde{d}\).

  % For node \(x\) let \(d_x = \dist(x, T \setminus \{x\})\) and let \(u\) be the closest node to \(x\). For each node \(x\) the machines can locally retrieve the bit vector associated with the closest node in \(T \setminus \{x\}\) as follows -- The \(i^{th}\) bit of \(u\)'s vector is \(b\) if \(\dist(x, T_i^b) = d_x\) (if this condition fails for both \(b= 0\) and \(b=1\) then the \(i^{th}\) bit of \(u\)'s vector is the \(i^{th}\) bit of \(x\)'s vector)

  % All machines can know the bit vectors and the nodes that they are assigned to in \(\tilde{O}(n/k)\) rounds. This can be done by machines just broadcasting the node ID's and vectors in each round. Therefore, the observation follows.

  % Let $T$ be the shortest path tree obtained from the previous paragraph. This $T$ is a subgraph of the original graph $G$. Hereafter, we will restrict the executions of the algorithms on $T$. This can be done by setting the weights of the edges that do not belong to $T$ to $W$, where $W$ is $\infty$, or some sufficiently large quantity. Now, create $2 \log |U|$ subsets of $U$, such that for each distinct ordered pair of vertices $(u, v)$, there exists a subset $U'$, such that $u \in U'$, but $v \notin U'$. These subsets are generated as follows. Arbitrarily assign $\log |U|$ bit binary IDs to the vertices in $U$, and broadcast them -- this can be done in $\tilde{O}(n/k)$ rounds. For a bit position $i$, the subsets $U(i, 0)$ (resp. $U(i, 1)$) contains all the vertices whose IDs have a $0$ (resp. $1$) at the $i$th bit position. Now for each subset $U(i, \cdot)$, we perform a slightly modified version of the ``dummy source vertex'' trick -- we add a dummy source vertex $s$, connect it to each vertex in $U(i, \cdot)$ by $0$ weight edges, and to each vertex in $U \setminus U(i, \cdot)$ by weight $W$ edges. Then, we use $\tilde{O}(n/k)$ round shortest path algorithm of Theorem \ref{theorem:SSSP}, from source $s$.
  % We can consider the subsets $U(i, \cdot)$ in a specific order, which is known to all machines before. For each such SSSP computation, every node $u$ remembers the shortest distance achieved, and the corresponding subset $U(i, \cdot)$. Now, consider a vertex $v \in V$. There will be exactly $\log |V|$ iterations in which $v$ will achieve a distance $\tilde{d}'$ that is within $(1+\epsilon)$ factor from $\tilde{d}$ (which it computed in the previous paragraph). These $\log |V|$ iterations correspond exactly to those subsets to which the vertex $u$ belongs. Then, the vertex $v$ can uniquely recreate the ID of the vertex $u$, from the corresponding subsets locally.
\end{proof}
%\begin{proof}
%	The idea is to introduce a dummy vertex \(s\), with zero-weight edges to each vertex \(u \in T\).
%	We pick a machine (arbitrarily) as the host of $s$ and this machine broadcasts a label for $s$.
%	Then each machine broadcasts the label of each vertex in $T$ that it hosts. This takes $\to(n/k)$ rounds.
%	Now the modified graph, with the new vertex $s$ and its incident edges, are known to the machines in the
%	format required by the $k$-machine model.
%	Now, we start an approximate SSSP computation with source \(s\) and the distance value that each vertex
%	$v$ acquires will be \(d(v, T)\).
%\end{proof}

%  LocalWords:  Klauck Becker poly MSSP Thorup ExclusiveMSSP
