\documentclass{article}



\usepackage{booktabs} % For formal tables
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{hyperref}
%\tolerance=1000
\usepackage{algorithmicx}
\usepackage[]{algpseudocode}
\usepackage[algo2e,linesnumbered]{algorithm2e}
\usepackage{verbatim}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{mdframed}



\newtheorem{theorem}{Theorem}[section]
\newtheorem{observation}[theorem]{Observation}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}


\newboolean{short}
\setboolean{short}{false}


\providecommand{\Pr}{\text{Pr}}
\renewcommand{\Pr}{\text{Pr}}
% \providecommand{\R}{\mathbb{R}}
% \renewcommand{\R}{\mathbb{R}}
\renewcommand{\r}{\mbox{rank}}


\providecommand{\dist}{\text{dist}}
\renewcommand{\dist}{\text{dist}}
\providecommand{\tvalue}{\text{value}}
\renewcommand{\tvalue}{\text{value}}
\providecommand{\fcost}{\text{f-cost}}
\renewcommand{\fcost}{\text{f-cost}}
\newcommand{\facloc}{\textsc{FacLoc}}
\newcommand{\pmedian}{$p$\textsc{Median}}
\newcommand{\pcenter}{$p$\textsc{Center}}
\renewcommand{\to}{\tilde{O}}

\providecommand{\rdown}{r}
\renewcommand{\rdown}{r}
\providecommand{\dup}{d}
\renewcommand{\dup}{d}
\providecommand{\rsim}{r}
\renewcommand{\rsim}{r}

\newcommand{\real}{\mathbb{R}}
\renewcommand{\natural}{\mathbb{N}}
\newcommand{\X}{X}
\newcommand{\q}{\mathbf{q}}
\renewcommand{\i}{\iota}
\newcommand{\R}{\mathcal{R}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\T}{\mathcal{T}}
\renewcommand{\ij}{\i j}
\renewcommand{\S}{\mathcal{R}}
\renewcommand{\cdots}{\ldots}
\newcommand{\lr}[1]{\left(#1\right)}
\newcommand{\LR}[1]{\left\{#1\right\}}
\newcommand{\Red}[1]{{\color{red} #1}}

\newcommand{\cm}{\mathcal{CONGEST}}
\newcommand{\Ghat}{\hat{G}} 
\newcommand{\ehat}{\hat{E}}
\newcommand{\ct}{\mathcal{T}}
\newcommand{\cthat}{\hat{\mathcal{T}}}
\newcommand{\lra}{Lenzen's routing protocol}
\renewcommand{\ll}{\log\log n}
\renewcommand{\lll}{\log\log\log n}
\newcommand{\aspect}{growth-bounded property}

\DeclareMathOperator{\poly}{poly}
\DeclareMathOperator{\degree}{degree}
\DeclareMathOperator*{\E}{\mathbf{E}}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
\newcommand{\ehh}{\mathpzc{h}}
\newcommand*{\Union}{\bigcup}
\newcommand*{\union}{\mathop{\cup}}

\newcommand{\cost}{\mathsf{cost}}
\renewcommand{\epsilon}{\varepsilon}
\newcommand{\LP}{\textsf{LP}\xspace}
\DeclareMathOperator*{\argmin}{arg\,min}
\newcommand{\kmm}{\(k\)-machine model\xspace}
\renewcommand{\r}{\mbox{rank}}

\newcommand*\samethanks[1][\value{footnote}]{\footnotemark[#1]}

\begin{document}
\title{Near-Optimal Clustering in the $k$-machine model}

\author{Sayan Bandyapadhyay \thanks{Department of Computer Science, The University of Iowa \texttt{\{sayan-bandyapadhyay, tanmay-inamdar, shreyas-pai, sriram-pemmaraju\}@uiowa.edu}} \and Tanmay Inamdar \samethanks[1] \and Shreyas Pai \samethanks[1] \and Sriram V.~Pemmaraju \samethanks[1]}


\maketitle

\begin{abstract}
	The \textit{clustering problem}, in its many variants, has numerous applications in operations research and computer science (e.g., in applications in bioinformatics, image processing, social network analysis, etc.).
	As sizes of data sets have grown rapidly, researchers have focused on designing algorithms for clustering problems in 
	models of computation suited for large-scale computation such as MapReduce, Pregel, and streaming models.
	The \textit{$k$-machine model} (Klauck et al., SODA 2015) is a simple, message-passing model for large-scale distributed graph processing.
	This paper considers three of the most prominent examples of clustering problems: the \textit{uncapacitated facility location}
	problem, \textit{facility location with outliers} problem, the \textit{$p$-median} problem, and the \textit{$p$-center} problem and presents $O(1)$-factor approximation
	algorithms for these problems running in $\tilde{O}(n/k)$ rounds in the $k$-machine model. 
	These algorithms are optimal upto polylogarithmic factors because this paper also shows $\tilde{\Omega}(n/k)$ lower bounds for
	obtaining $\mbox{poly}(n)$-factor approximation algorithms for these problems.
	These are the first results for clustering problems in the $k$-machine model.

	We assume that the metric provided as input for these clustering problems in only 
	implicitly provided, as an edge-weighted graph and in a nutshell, our main technical contribution is to show that constant-factor approximation algorithms for all clustering
	problems mentioned above can be obtained by learning only a small portion of the input metric.
\end{abstract}


% \IncMargin{0.5em}
% Section 1
\input{introduction}
% Section 2
\input{preliminaries}
% Section 4
\input{sequential_algorithms}
% Section 5
\input{facility_location}
% Section 5
\input{robustfl_implicit}
% Section 5
\input{penalties_implicit}
% Section 5
\input{p_median_graph}
% Section 5
\input{p_center}
% Section 4
\input{lower_bound}

%\input{appendix}

\section{Conclusions and Future work}
\label{section:conclusions}

This paper initiates the study of clustering problems in the $k$-machine model
and presents near-optimal (in rounds) constant-factor approximation algorithms for
these problems.
The near-optimality of our algorithms is established via almost-matching lower bounds on
on the number of rounds needed to solve these problems in the $k$-machine model.
However, the lower bounds critically depend a certain assumption regarding how the output
of the clustering algorithms is to be represented.
Specifically, we require that every machine with an open facility knows all clients
connecting to that facility.
This requirement forces some machines to learn a large volume of information distributed
across the network and this leads to our lower bounds.

We could alternately, impose a rather ``light weight'' output requirement and, for example,
require each machine with an open facility to simply know the \textit{number} of clients connecting to
it or the aggregate connection cost of all the clients connecting to it.
(Of course, independent of this change, the output requires that each client know the facility it connects to.)
So the main open question that follows from our work is whether we can design
optimal $k$-machine algorithms under this relaxed output requirement.
$\Omega(n/k^2)$ lower bounds do not seem difficult to prove in this setting, but to obtain
$\tilde{O}(n/k^2)$-round constant-approximation algorithms seems much harder.
Alternately, can we prove stronger lower bounds even in this, more relaxed, setting?

\bibliographystyle{plain}
\bibliography{references} 

%\newpage

%\input{appendix}

\end{document}
