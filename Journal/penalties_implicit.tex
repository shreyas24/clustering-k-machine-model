\section{Facility Location with Penalties in \(\tilde{O}(n/k)\) rounds}

In this section we describe how to implement Algorithm \ref{alg:penaltiesSeq} in the \kmm. Since the radius computation phase for \textsc{FacLoc} with Penalties is different from the one for \textsc{FacLoc} and Robust \textsc{FacLoc} (Algorithm \ref{alg:RC}), we first show how to modify Algorithm \ref{alg:RC} in order to compute approximate radii for the Penalty version.

\subsection{Radius Computation}
\label{sec:pen-imp-radcomp}
In \textsc{FacLoc} with Penalties, the definition of radii differs from that in Robust \textsc{FacLoc} (or the standard Facility Location algorithm) due to the penalties of the clients. In particular, for a vertex $v$, the radius is defined as $r_v$ satisfying the following equation: $f_v = \sum_{u \in V} \max\{\min\{r_v - d(u, v), p_u - d(u, v) \}, 0\}$. Throughout this section, we assume that such an $r_v$ exists -- otherwise, it can be shown that excluding $v$ as a candidate facility does not affect the cost of any solution. We now show how to appropriately modify the neighborhood computation and the radius computation subroutines. 

The key idea is to divide vertices into $O(\log n)$ classes, such that the penalties of the vertices belonging to a particular class are within $1+O(\epsilon)$ factor of each other. Then, for any vertex $v$, and for each penalty class, we estimate the number of vertices from that penalty class, in $(1+\epsilon)^i$-neighborhood of $v$. Once we have these estimates for each range of neighborhoods, they can be used for computation of approximate computation of radii. We formalize this in the following.

First, we assume that we have normalized $f_i, c_{ij}, p_j$, such that any positive quantity is at least $1$. Note that we can normalize any given input in this manner in $O(1)$ rounds of the \kmm. Let $P_0 \coloneqq \{j \in V \mid p_j = 0 \}$, and for any integer $t \ge 1$, let $P_b \coloneqq \{j \in V \mid (1+\epsilon)^b \le p_j < (1+\epsilon)^{b+1}\}$. By assumption, the penalties are polynomially bounded in $n$, and hence the total number of penalty classes is $O(\log n)$.

Let \textsc{NbdSizeEstimates}($G, \epsilon, b$) be a modified version (of the original algorithm, Algorithm 3 in \cite{BandyapadhyayArxiv2017}, see Algorithm \ref{alg:ModifiedCohenEstimates}) that takes an additional parameter $b \ge 0$, wherein only the vertices in $P_b$ participate. That is, random ranks (as in the original version) are chosen only for the vertices in $P_b$. However, the neighborhood size estimates are computed for \emph{all} vertices. The details are straightforward, and are therefore omitted. 

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{NbdSizeEstimates}\((G, \epsilon, b)\)\label{alg:ModifiedCohenEstimates}}
	$\epsilon' := \epsilon/(\epsilon + 4)$; $t = \lceil 2 \log_{1+\epsilon'} n \rceil$; $\ell := \lceil c \log n/(\epsilon')^2 \rceil$\\
	\For{\(j := 1, \ldots, \ell\)} {
		\textbf{Local Computation.} Each machine $m_j$ picks a rank $\r(v)$, for each vertex $v \in H(m_j) \cap P_b$,
		chosen uniformly at random from $[0, 1]$. Machine $m_j$ then rounds $\r(v)$ down to the closest 
		$(1+\epsilon')^i/n^2$ for integer $i \ge 0$\label{alg2.1:ChooseRanks} \\
		\For{$i := 0, 1, \ldots, t-1$}{
			$T_i := \{v \in P_b \mid \r(v) = (1+\epsilon')^i/n^2\}$\\
			Compute a \((1+\epsilon)\)-approximate solution to MSSP using \(T_i\) as the set of sources \label{alg2.1:MSSP}; let $\tilde{d}(v, T_i)$ denote the computed approximate distances\\
			\textbf{Local Computation.} Machine $m_j$ stores $\tilde{d}(v, T_i)$ for each \(v \in H(m_j)\)\\
		}
	}
\end{algorithm2e}

For a vertex $v \in V$, and for parameters $b, r \ge 0$, let $B(v, r, b) \coloneqq B(v, r) \cap P_b$. Then, let $Q(v, r, b)$ denote the query ``What is the size of $B(v, r) \cap P_b$?''. The details of how to answer this query from the output of \textsc{NbdSizeEstimates} are same as in the original version.

\begin{lemma} \label{lem:nbd-with-penalties}
	For any vertex $v \in V$, for any $r, b \ge 0$, and for any $\epsilon > 0$, the modified \textsc{NbdSizeEstimates} algorithm satisfies the following properties.
	\begin{itemize}
		\item For the query $Q(v, r/(1+\epsilon), b)$, the algorithm returns an output that is at most $|B(v, r) \cap P_b| \cdot (1+\epsilon)$.
		\item For the query $Q(v, r(1+\epsilon), b)$, the algorithm returns an output that is at most $|B(v, r) \cap P_b|/(1+\epsilon)$.
	\end{itemize}
\end{lemma}

We define the following quantities with respect to any vertex $v \in V$. Let $\alpha(v, r, b) \coloneqq \sum_{u \in B(v, r, b)} \max\{\min\{r - d(u, v), p_u - d(u, v)\}, 0 \}$, and let $\alpha(v, r) \coloneqq \sum_{u \in B(v, r)} \max\{\min\{r - d(u, v), p_u - d(u, v)\}, 0 \}$. It is easy to see that $\alpha(v, r) = \sum_{b \ge 0} \alpha(v, r, b)$. Finally, let $q_{i, b}(v) \coloneqq |B(v, (1+\epsilon)^i, b)|$.

\begin{lemma} \label{lem:alpha-lower-bound}
	If $t > b$, then $\alpha(v, (1+\epsilon)^t, b) \ge \sum_{i = 0}^{t-1} q_{i, b} \lr{(1+\epsilon)^{i+1} - (1+\epsilon)^i}$. Otherwise, $\alpha(v, (1+\epsilon)^t, b) \ge \sum_{i = 0}^{b-1}\ q_{i, b}(v)\lr{(1+\epsilon)^b - (1+\epsilon)^{i+1}} + \sum_{i = b}^{t-1} q_{i, b} \lr{(1+\epsilon)^{i+1} - (1+\epsilon)^i}$.
\end{lemma}
\begin{proof}
	First, let $t > b$. And consider,
        \begingroup
        \allowdisplaybreaks
	\begin{align*}
		\alpha(v, (1+\epsilon)^t, b) &= \sum_{i = 0}^{t -1} \alpha(v, (1+\epsilon)^{i+1}, b) - \alpha(v, (1+\epsilon)^i, b)
		\\&\ge \sum_{i = 0}^{t - 1} \sum_{u \in B(v, (1+\epsilon)^i, b)} \bigg( \min\{(1+\epsilon)^{i+1} - d(u, v), (1+\epsilon)^b - d(u, v)\} \\
		&\qquad - \min\{(1+\epsilon)^{i} - d(u, v), (1+\epsilon)^{b+1} - d(u, v)\}\bigg)
		\\&= \sum_{i = 0}^{t-1} \sum_{u \in B(v, (1+\epsilon)^i, b)} (1+\epsilon)^{i+1} - d(u, v) - ((1+\epsilon)^i - d(u, v))
		\\&= \sum_{i = 0}^{t-1} \sum_{u \in B(v, (1+\epsilon)^i)} (1+\epsilon)^{i+1} - (1+\epsilon
		)^i
		\\&= \sum_{i = 0}^{t-1}q_{i, b} \lr{(1+\epsilon)^{i+1} - (1+\epsilon)^i}.
	\end{align*}
	Now, if $t \le b$, then
	\begin{align*}
	\alpha(v, (1+\epsilon)^t, b) &= \alpha(v, (1+\epsilon)^b, b) + \sum_{i = b}^{t -1} \alpha(v, (1+\epsilon)^{i+1}, b) - \alpha(v, (1+\epsilon)^i, b)
	\\&\ge \alpha(v, (1+\epsilon)^b, b) + \sum_{i = b}^{t-1} q_{i, b} \lr{(1+\epsilon)^{i+1} - (1+\epsilon)^i}	
	\end{align*}
        \endgroup
	Where final step uses similar arguments from the previous case. Now, we consider,
	\begin{align*}
		\alpha(v, (1+\epsilon)^b, b) &= \sum_{u \in B(v, (1+\epsilon)^b, b)} (1+\epsilon)^b - d(u, v) 
		\\&= \sum_{i = 0}^{b-1}\   \sum_{u \in B(v, (1+\epsilon)^{i+1}, b) \setminus  B(v, (1+\epsilon)^i, b)} (1+\epsilon)^b - d(u, v)
		\\&\ge \sum_{i = 0}^{b-1}\ q_{i, b}(v)\lr{(1+\epsilon)^b - (1+\epsilon)^{i+1}}
	\end{align*}
	
\end{proof}

For a vertex $v$, and for any $t, b \ge 0$, let $\lambda(v, t, b)$ denote the appropriate lower bound on $\alpha(v, (1+\epsilon)^t, b)$, given by Lemma \ref{lem:alpha-lower-bound} (based on two different cases). Let $\tilde{\lambda}(v, t, b)$ be the quantity obtained by replacing $q_{i, b}(v)$ by the approximate neighborhood estimate $\tilde{q}_{i, b}(v)$ in the lower bound $\lambda(v, t, b)$. We now state the Radius Computation algorithm.

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{RadiusComputation} Algorithm (Version 3)\label{alg:RC3}}
	\textbf{Neighborhood-Size Computation.} Call the \textsc{NbdSizeEstimates} algorithm (Algorithm \ref{alg:CohenEstimates}) to obtain \textit{approximate} neighborhood-size estimates $\tilde{q}_{i, b}(v)$
	for all integers $i \ge 0, b \ge 0$ and for all vertices $v$.\\
	\textbf{Local Computation.} Each machine $m_j$ computes $\tilde{r}_v$ locally, for all vertices $v \in H(m_j)$ using the formula $\tilde{r}_v := (1+\epsilon)^{t-1}$ where $t \ge 1$ is the smallest integer for which $\sum_{b \ge 0} \tilde{\lambda}(v, t, b) > f_v$. If there is no such integer, define $\tilde{r}_v = \infty$.
\end{algorithm2e}


We have the following bounds on the approximate radius computed by the algorithm.

\begin{lemma} \label{lem:penalty-approx-r}
	For every $v \in V$, $\frac{r_v}{(1+\epsilon)^2} \le \tilde{r}_v \le (1+\epsilon)^2 \cdot r_v$
\end{lemma}
\begin{proof}
	 By Lemma \ref{lem:nbd-with-penalties}, we have the following for $i \ge 1$:
	$$\frac{1}{(1+\epsilon)} \cdot q_{i-1, b}(v) \le \tilde{q}_{i, b}(v) \le (1+\epsilon) \cdot q_{i+1, b}(v).$$
	
	%Now, for a fixed $t$, it can be shown, using the previous inequality appropriately and using the definitions, that
	%$$\sum_{i=0}^{t-1}q_{i-1,b}((1+\epsilon)^i - (1+\epsilon)^{i-1}) \le \sum_{i = 0}^{t-1}\tilde{q}_{i,b}(v) ((1+\epsilon)^{i+1} - (1+\epsilon)^i) \le \sum_{i = 0}^{t-1} q_{i+1,b}(v) ( (1+\epsilon)^{i+2} - (1+\epsilon)^{i+1} )$$
	
	%$$\frac{1}{1+\epsilon} \cdot \lambda(v, t-1, b) \le \tilde{\lambda}(v, t, b) \le (1+\epsilon) \cdot \lambda(v, t+1, b)$$
	
	Now, let $t$ be the smallest integer for which $\sum_{b \ge 0}\tilde{\lambda}(v, t, b) > f_v$. Now, by Lemma \ref{lem:alpha-lower-bound}, we have that $\lambda(v, t, b) \ge \alpha(v, (1+\epsilon)^t, b)$. Now, using arguments very similar to those in the proof of Lemma 8 of \cite{BandyapadhyayArxiv2017}, one can show the following inequality.
	
	$$\alpha(v, (1+\epsilon)^{t-2}) \le \sum_{b \ge 0}\tilde{\lambda}(v, t, b) \le \alpha(v, (1+\epsilon)^{t+1}).$$
	
	Recall that $\alpha(v, r) = \sum_{b \ge 0} \alpha(v, r, b)$.
	
	Therefore, there must exist a value $r_v \in [(1+\epsilon)^{t-3}, (1+\epsilon)^{t+1}]$, such that $\alpha(v, r_v) = f_v$. The Lemma follows, because $\tilde{r}_v = (1+\epsilon)^{t-1}$.
\end{proof}





\begin{algorithm2e}\caption{\textsc{PenaltyFacLoc}\((G, F, C, p)\)}\label{alg:penaltieskmm}
  \tcc{Radius Computation Phase:}
  Compute $r_i$ for each $i \in F$ satisfying $f_i = \sum_{j \in C} \max\LR{\min\LR{r_i - c_{ij}, p_j - c_{ij}}, 0}.$ \\
  \tcc{Greedy Phase:}
  $C' \gets \emptyset$, \quad $F' \gets \emptyset$, \quad $O' \gets \emptyset$. \\
  \For{\(i = 0, 1, 2, \dots\)}{
    Let \(W\) be the set of vertices \(w \in F\) across all machines with \(\tilde{r}_w = \tilde{r} = (1 + \epsilon)^i\) \\
    Using Lemma \ref{lemma:MSSP}, remove all vertices from \(W\) within distance \(2(1 + \epsilon)^2\cdot \tilde{r}\) from \(F'\) \label{alg5:remove-close-facs}\\
    \(I \leftarrow \textsc{ApproximateMIS}(G, W, 2(1 + \epsilon)^3 \cdot \tilde{r}, \epsilon)\) \\
    \(F' \leftarrow F' \cup I\)
  }
  \tcc{Outlier Determination Phase:}
  Using Lemma \ref{lemma:MSSP}, add to \(C'\) all clients \(j\) having distance to \(F'\) less than \(p_j\) and add the rest to \(O'\) \label{alg5:remove-clients}\\
  \Return $(C', F')$ as the solution.
\end{algorithm2e}

Our \kmm implementation of the \textsc{FacLoc} with Penalties algorithm is summarized in Algorithm \ref{alg:penaltieskmm}. The correctness proof is similar to that of Algorithm \ref{alg:penaltiesSeq} but is complicated by the fact that we compute \((1 + \epsilon)\)-approximate distances instead of exact distances. This analysis appears in the next section, and as a result we get the following theorem the proof of which is similar to Theorem \ref{thm:kmm-OutlierGuarantee}.

\begin{theorem} \label{thm:kmm-PenaltyGuarantee}
	In \(\tilde{O}(\text{poly}(1/\epsilon) \cdot n/k)\) rounds, whp, Algorithm \ref{alg:penaltieskmm} finds a factor \(5 + O(\epsilon)\) approximate solution \((C', F')\) to the \textsc{FacLoc} with Penalties problem.
\end{theorem}

\subsection{Analysis of the Algorithm}

We state the standard primal and dual linear programming relaxations for facility location with penalties in Figure \ref{fig:penalty-primal-dual}. For $j \in C$ and $i \in F$, define $w_{ij} \coloneqq \max\LR{\min \LR{r_i - c_{ij}, p_j - c_{ij}}, 0}$ and for $j \in C$, let $v'_j \coloneqq \min_{i \in F} c_{ij} + w_{ij}$. Note that $v'_j = \min_{i \in F}\max\LR{c_{ij}, \min\LR{r_i, p_j}}$. If $\i$ is a facility realizing the minimum $v'_j = c_{\i j} + w_{\i j}$, then we say that $\i$ is the bottleneck of $j$.

To make the analysis easier, we consider a more costly solution $(\tilde{C}', F')$ where the set of clients $\tilde{C}'$ is constructed using the following modified outlier determination phase: for each client $j$, if $c_{\ij} \le p_j$ then $j \in \tilde{C}'$ and otherwise $j \in \tilde{O}'$ where $\i$ is the bottleneck of $j$.

It is easy to see by an exchange argument that the $\cost_e(C', F') \le \cost_e(\tilde{C}', F')$, the outliers determined in the algorithm are at least as far from \(F'\) as ones in the modified outlier determination phase. Henceforth, we analyze the cost of the solution $(\tilde{C}', F')$ by comparing it to the cost of a feasible dual \LP solution and in order to alleviate excessive notation, we will henceforth refer to the solution $(\tilde{C}', F')$ as $(C', F')$ and $\tilde{O}'$ as $O'$.

Because of the way we choose the outliers in the solution we consider for the analysis \((C', F')\) we have the following property (where $\i$ is the bottleneck of $j$) --
$$v_j = \begin{cases}
  \max\LR{c_{\ij}, r_\i} &\text{if } j \in C' \\
  p_j  &\text{if } j \in C \setminus C'
\end{cases}$$

Throughout this section, we condition on the event that the outcome of all the randomized algorithms is as expected (i.e. the ``bad'' events do not happen). Note that this happens with w.h.p. We first need the following facts along the lines of \cite{Thorup2001}. We skip the proofs as they are identical to the corresponding facts in the previous section.

\begin{lemma}[Modified From Lemma 8 Of \cite{Thorup2001}] \label{lem:apdx-p-lem8}
  There exists a total ordering $\prec$ on the facilities in $F$ such that $u \prec v \implies \tilde{r}_u \le \tilde{r}_v$, and $v$ is added to $F'$ if and only if there is no previous $u \prec v$ in $F'$ such that $c_{u v} \le 2(1+\epsilon)^2 \tilde{r}_v$.
\end{lemma}
% \begin{proof}[Proof Sketch]
%   The ordering is obtained by enumerating the facilities processed in each iteration (with arbitrary order given to facilities in the same iteration). The facilities in $I$ that are included in $F'$ before the rest of the vertices of $W$. The lemma follows because of line \ref{alg5:remove-close-facs} (if $u$ and $v$ are processed in different iterations) the definition of $(\epsilon, d)$-approximate MIS (if $u$ and $v$ are processed in the same iteration).
% \end{proof}


\begin{claim}[Modified From Claim 9.2 Of \cite{Thorup2001}] \label{cl:apdx-p-cl9.2}
  For any two distinct vertices $u, v \in F'$, we have that $c_{u v} > 2(1+\epsilon)^2 \cdot \max\{\tilde{r}_u, \tilde{r}_v\}$.
\end{claim}
% \begin{proof}[Proof Sketch]
%   Without loss of generality, assume that $u \prec v$, so $\tilde{r}_u \le \tilde{r}_v$. From Lemma \ref{lem:apdx-lem8} we have $c_{u v} > 2(1+\epsilon)^2 \tilde{r}_v \ge 2(1+\epsilon)^2 \tilde{r}_v$
% \end{proof}

We simultaneously prove feasibility of the dual solution we constructed, and show how it can be used to pay for the integral solution. We consider different cases regarding a fixed client $j \in C$ with bottleneck facility $\i$. We first prove a few straightforward claims.

\begin{claim} \label{cl:apdx-p-4}
  For any $i \in F$ and $j \in C$, $\tilde{r}_i \le (1 + \epsilon)^2 (c_{ij} + w_{ij})$. Furthermore if for some \(i \in F\) and \(j \in C\), \(w_{ij} > 0\), then \(\tilde{r}_i \ge (1 + \epsilon)^{-2} (c_{ij} + w_{ij})\)
\end{claim}
\begin{proof}
  We have $w_{ij} = \max\{0, \min\LR{r_i, p_j} - c_{ij}\} \ge \min\LR{r_i, p_j} - c_{ij}$ and therefore $\tilde{r}_i \le (1 + \epsilon)^2 r_i \le (1 + \epsilon)^2 (c_{ij} + w_{ij})$.

  If for some \(i \in F\) and \(j \in C\), \(w_{ij} > 0\), then \(w_{ij} = r_i - c_{ij}\) which means that \(\tilde{r}_i \ge (1 + \epsilon)^{-2} r_i = (1 + \epsilon)^{-2} (c_{ij} + w_{ij})\)
\end{proof}


\begin{claim} \label{cl:apdx-p-1}
  If $\i \in F'$ is the bottleneck for $j \in C$, then $w_{i'j} = 0$ for all $i' \in F'$, where $i' \neq \i$.
\end{claim}
\begin{proof}
  Suppose there exists a facility $i' \in F'$ with $w_{i'j} > 0$. That is, $\min\LR{r_{i'} - c_{i'j}, p_j - c_{i'j}} > 0$, which further implies that $c_{i'j} < \min\LR{p_j, r_{i'}} \le r_{i'} \le (1 + \epsilon)^2 \tilde{r}_{i'}$.

  If $c_{\ij} \ge r_{\i}$, then $v'_j = c_{\ij} + \max\LR{0, \min\LR{r_{\i} - c_{\ij}, p_j - c_{\ij} }} = c_{\ij}$. However, since $w_{i'j} > 0$, $c_{\ij} = v'_j \le c_{i'j} + w_{i'j} = \min\LR{p_j, r_{i'}} \le r_{i'} \le (1 + \epsilon)^2 \tilde{r}_{i'}$

  Otherwise, $c_{\ij} <  r_{\i} \le (1 + \epsilon)^2 \tilde{r}_{\i}$.

  Therefore in either case, $c_{\i i'} \le c_{\ij} + c_{i'j} \le 2 (1 + \epsilon)^2 \max\LR{\tilde{r}_{i'}, \tilde{r}_{\i}}$, which is a contradiction to Claim \ref{cl:apdx-p-cl9.2}.
\end{proof}

\begin{claim} \label{cl:apdx-p-2}
  If $\i \not\in F'$ is the bottleneck of $j \in C$ and $\max\LR{r_\i, c_{\ij}} \le p_j$, and $i' \in F'$ caused $\i$ to close, then $c_{i' j} \le 3(1+\epsilon)^4 v_j$.
\end{claim}
\begin{proof}
  Note that since we assume $\max\LR{r_\i, c_{\ij}} \le p_j$, we have $j \in C'$, $v_j = v'_j$, $c_{\ij} \ge p_j$, and $r_\i \le p_j$.
  Since $i' \in F'$ caused $\i$ to close, $\tilde{r}_{i'} \le \tilde{r}_{\i}$. Furthermore, $c_{i' \i} \le 2 (1+\epsilon)^2 \tilde{r}_{\i}$. Therefore, $c_{i' j} \le c_{\i i'} + c_{\i j} \le 2 (1+\epsilon)^2 \tilde{r}_{\i} + c_{\ij} \le 2 (1+\epsilon)^4 r_{\i} + c_{\ij} $.

  If $c_{\ij} \ge r_{\i}$, then $w_{\ij} = 0$, and $c_{\ij} = v_j$. This means $2(1+\epsilon)^4r_{\i} + c_{\ij} \le 3(1+\epsilon)^4c_{\ij} = 3(1+\epsilon)^4v_{j}$
  Otherwise, $c_{\ij} < r_{\i}$. Here, $v_j = \min\LR{r_{\i}, p_j}$ Then $2(1+\epsilon)^4r_{\i} + c_{\ij} < 3(1+\epsilon)^4r_{\i} = 3(1+\epsilon)^4 \min\LR{r_{\i}, p_j} \le 3(1+\epsilon)^4v_{j}$

  In either case, $c_{i' j} \le 3(1+\epsilon)^4v_{j}$.
\end{proof}


\begin{claim} \label{cl:apdx-p-3}
  If $\i$ is the bottleneck of $j$, with $\max\LR{r_\i, c_{\ij}} \le p_j$, then $(1 + \epsilon)^2 v'_j \ge \tilde{r}_\i$.
\end{claim}
\begin{proof}
  Again, since we assume $\max\LR{r_\i, c_{\ij}} \le p_j$, we have $j \in C'$, $v_j = v'_j$, $c_{\ij} \ge p_j$, and $r_\i \le p_j$.

  Recall that $v_j = v'_j = \max\LR{c_{\ij}, \min\LR{r_\i, p_j}} = \max\LR{c_{\ij}, r_{\i}} \ge r_{\i} \ge (1 + \epsilon)^{-2} \tilde{r}_{\i}$ and the claim follows.
\end{proof}

We are now ready to prove the feasibility and approximation guarantee

\begin{lemma}
  The solution $(v, w)$ is a feasible solution to the dual LP relaxation \ref{fig:penalty-primal-dual}.
\end{lemma}
\begin{proof}
  First note that constraints \ref{constr:penlpd-beta}, \ref{constr:penlpd-penalty}, and \ref{constr:penlpd-integral} are satisfied by construction for all $i \in F$ and $j \in C$ and so is constraint \ref{constr:penlpd-alpha} for all $j \in C'$.

  All that is left to show is that constraint \ref{constr:penlpd-alpha} is satisfied for all $j \in O'$. Since $j \in O'$, $\max\LR{r_\i, c_{\ij}} > p_j$.

  We have, $v_j \coloneqq p_j < \max\LR{r_\i, c_{\ij}} =  \max\LR{c_{\ij}, \min\LR{r_{\i}, p_j}} = v'_j \le c_{ij} + w_{ij}$ for any $i \in F$.
\end{proof}
      

\begin{lemma}
  For any $j \in C'$, there is some $i \in F'$ such that $c_{ij} + w_{ij} \le 3 (1+\epsilon)^4 v_j$.
\end{lemma}
\begin{proof}
  In all the cases, we assume that $\max\LR{r_{\i}, c_{\ij}} \le p_j$ and therefore $j \in C'$. This also implies $v_j = v'_j = \max \LR{c_{\ij}, \min\LR{p_j, r_{\i}}} = \max \LR{c_{\ij}, r_{\i}}$. Therefore, we can just disregard the penalties in the analysis.
  \begin{itemize}
      
    \item \textbf{Case 1.} $\i \in F'$
      
      Connect $j$ to $\i$. From Claim \ref{cl:apdx-p-1}, we know that $w_{i'j} = 0$ for all other $i' \in F'$.
      
      \begin{enumerate}
      \item If $c_{\ij} < r_{\i}$, then $v_j = c_{\ij} + w_{\ij}$. In this case, $v_j$ pays for connecting $j$ to $\i$ and also for $j$'s contribution to opening cost of $\i$ which is exactly $w_{\ij}$.
      
      \item Otherwise $c_{\ij} \ge r_{\i}$, then $w_{\ij} = 0$, which is $j$'s contribution towards $\i$. We have $v_j = c_{\ij}$ and therefore $v_j$ pays for connecting $j$ to $\i$.
      \end{enumerate}
      
    \item \textbf{Case 2.} $\i \notin F'$ and $w_{ij} = 0$ for all $i \in F'$.
      
      Let $i'$ be the facility that caused $\i$ to close. Connect $j$ to $i'$. From Claim \ref{cl:apdx-p-2}, we have $c_{i' j} \le 3(1+\epsilon)^4 v_{j}$. Therefore, $3(1+\epsilon)^4 v_j$ pays for the connection to $i'$.
      
    \item \textbf{Case 3.} $\i \notin F'$, there is some $i' \in F'$ with $w_{i'j} > 0$, but $i'$ did not cause $\i$ to close.
      
      We connect $j$ to $i'$. By assumption $w_{i'j} = r_{i'} - c_{i'j} > 0$. Furthermore, let $i$ be the facility that caused $\i$ to close. By Claim \ref{cl:apdx-p-2} we have $c_{ij} \le 3(1+\epsilon)^4 v_j$.
      
      We have $c_{i'j} + w_{i'j} = r_{i'}$. Also, $c_{i i'} > 2(1+\epsilon)^2 \tilde{r}_{i'}$, by Claim \ref{cl:apdx-p-cl9.2} since $i', i$ both were added to $F'$.
      
      Now, $2(c_{i' j} + w_{i'j}) = 2r_{i'} \le 2(1+\epsilon)^2 \tilde{r}_{i'} < c_{i i'} \le c_{i' j} + c_{i j}$.

      Subtracting $c_{i'j}$ from both sides, we get $c_{i' j} + 2w_{i'j} \le c_{i j} \le 3(1+\epsilon)^4 v_j$. Therefore, $3(1+\epsilon)^4 v_j$ pays for the connection cost of $j $ to $i'$ and also for (twice) $j$'s contribution towards opening $i'$.
      
    \item \textbf{Case 4.} $\i \notin F'$ and $i' \in F'$ with $w_{i'j} > 0$ caused $\i$ to close.
      
      We connect $j$ to $i'$. From Claim \ref{cl:apdx-p-3}, we have that $(1+\epsilon)^2 v_j \ge r_\i$.
      
      Since $i'$ caused $\i$ to close, $\tilde{r}_\i \ge \tilde{r}_{i'} \ge (1+\epsilon)^{-2} r_{i'} \ge (1+\epsilon)^{-2} c_{i'j} + w_{i'j}$.

      Therefore, $c_{i'j} + w_{i'j} \le (1+\epsilon)^2\tilde{r}_{\i} \le (1+\epsilon)^4 r_{\i} \le (1+\epsilon)^4v_j$. That is, $(1+\epsilon)^4v_j$ pays for the connection cost of $j$ to $i'$, as well as its contribution towards opening of $i'$.
    \end{itemize}
\end{proof}

Thus, $(v, w)$ is a feasible dual solution. We use the above analysis to conclude with the following theorem.

\begin{theorem}
  $$\cost(C', F') \le 3(1+\epsilon)^4 \cdot \cost(C^*, F^*).$$
\end{theorem}
\begin{proof}
  We show $\cost(C', F') \le 3(1+\epsilon)^4 \lr{\sum_{j \in C} v_j}$, which is sufficient since $(v, w)$ is a feasible dual solution, and cost of any feasible dual solution is a lower bound on the cost of an integral optimal solution.

  As we have argued previously, for any $j \in C \setminus C'$, we have $p_j = v_j$, and that for any $j \in C'$, we have $d(j, F') + s(j)$, where $s(j) \ge 0$ is the contribution of $j$ towards opening a single facility in $F'$. We have also argued that any $j \in C'$ contributes $s(j)$ for at most one open facility from $F'$. It follows that,

  \begin{align*}
    \cost(C', F') &= \sum_{i \in F'} f_i + \sum_{j \in C'} d(j, F') + \sum_{j \in C \setminus C'} p_j
    \\&= \lr{\sum_{j \in C'} s(j) + d(j, F')} + \sum_{j \in C \setminus C'} p_j
    \\&\le 3(1+\epsilon)^4 \sum_{j \in C'} v_j + \sum_{j \in C \setminus C'} v_j
    \\&\le 3(1+\epsilon)^4 \lr{\sum_{j \in C} v_j}
  \end{align*}
\end{proof}

%  LocalWords:  outlier polynomially NbdSizeEstimates boxruled MSSP
%  LocalWords:  RadiusComputation PenaltyFacLoc FacLoc whp outliers
%  LocalWords:  's MPC
