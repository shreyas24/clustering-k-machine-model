\section{Robust Facility Location in \(\tilde{O}(n/k)\) rounds}

In this section we show how to implement the sequential algorithms for the Robust \textsc{FacLoc} in the \kmm. Our \kmm implementation of the Robust \textsc{FacLoc} algorithm is summarized in Algorithm \ref{alg:outlierskmm}. The correctness proof is similar to that of Algorithm \ref{alg:outlierSeq} but is complicated by the fact that we compute \((1 + \epsilon)\)-approximate distances instead of exact distances. 

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{RobustFacLocDist}\((F, C, p)\)\label{alg:outlierskmm}}
  \tcc{Recall $\ell \coloneqq |C| - p$}
  \For{$t = 1, \ldots, O(\log n)$}{
    Let $i_e \in F$ be a most expensive facility from the facilities with opening costs in the range $\left[(1+\epsilon)^t, (1+\epsilon)^{t+1}\right)$ \label{lin:i_e-kmm} \\
    Modify the facility opening costs to be
    $$f'_i = \begin{cases}
      +\infty &\text{ if $f_i > f_{i_e}$}
      \\0 &\text{ if $i = i_e$}
      \\f_i &\text{ otherwise}
    \end{cases}$$ \\
    \tcc{Radius Computation Phase:}
    Call the \textsc{RadiusComputation} algorithm (Algorithm \ref{alg:RC}) to compute  approximate radii. \label{alg4:radius-computation}\\
    
    \tcc{Greedy Phase:}
    Let \(F' = \emptyset\),  \(C' = \emptyset\), \(O' = C\) \\
    \For{\(i = 0, 1, 2, \dots\)}{ \label{alg4:for}
      Let \(W\) be the set of vertices \(w \in F\) across all machines with \(\tilde{r}_w = \tilde{r} = (1 + \epsilon)^i\) \\
      Using Lemma \ref{lemma:MSSP}, remove all vertices from \(W\) within approximate distance \(2(1 + \epsilon)^3\cdot \tilde{r}\) from \(F'\) \label{alg4:remove-close-facs}\\
      \(I \leftarrow \textsc{ApproximateMIS}(G, W, 2(1 + \epsilon)^3 \cdot \tilde{r}, \epsilon)\) \\
      \(F' \leftarrow F' \cup I\) \\
      Using Lemma \ref{lemma:MSSP}, move from \(O'\) to \(C'\) all vertices that are within distance \((1 + \epsilon) \cdot \tilde{r}\) from \(F_i\), the set of facilities processed up to iteration \(i\) \label{alg4:remove-clients}\\
      \textbf{if} $|O'| \le \ell$ \textbf{then break} \label{alg4:break} \\
    }\label{alg4:endfor}
    
    \tcc{Outlier Determination Phase:}
    \If{$|O'| > \ell$} {
      Using Lemma \ref{lemma:MSSP} find $O_1 \subseteq O'$, a set of $|O'| - \ell$ clients that are closest to facilities in $F'$. \\
      $C' \gets C' \cup O_1$, \quad $O' \gets O' \setminus O_1$.
    }
    \ElseIf{$|O'| < \ell$} {
      Using Lemma \ref{lemma:MSSP} find $O_2 \subseteq C \setminus O'$, a set of $(\ell - |O'|)$ clients that are farthest away from facilities in $F'$ \\
      $C' \gets C' \setminus O_2$, \quad $O' \gets O' \cup O_2$.
    }
    Let $(C'_t, F'_t) \gets (C', F')$
  }
  \Return $(C'_t, F'_t)$ with a minimum cost \label{lin:minimum-cost}
\end{algorithm2e}

Again, as in the analysis of the sequential algorithm, we abuse the notation so that (i) $(C', F')$ refers to a minimum-cost solution returned by the algorithm, (ii) $i_e$ refers to the facility chosen in the line \ref{lin:i_e-kmm} of the algorithm, and (iii) the modified instance with original facility costs. This analysis appears in the next section, and as a result we get the following theorem.

\begin{theorem} \label{thm:kmm-OutlierGuarantee}
  In \(\tilde{O}(\text{poly}(1/\epsilon) \cdot n/k)\) rounds, whp, Algorithm \ref{alg:outlierskmm} finds a factor \(5 + O(\epsilon)\) approximate solution \((C', F')\) to the Robust \textsc{FacLoc} problem for any constant \(\epsilon > 0\).
\end{theorem}
\ifthenelse{\boolean{short}}{}{
  \begin{proof}
    There are \(O(\log_{(1 + \epsilon)} \frac{f_{\max}}{f_{\min}}) = O(\log n)\) iterations of the outer for loop, where a facility with the highest opening cost from the range $\left[ (1+\epsilon)^t, (1+\epsilon)^{t+1} \right)$. The guess can be broadcast to all the machines, and they can modify their part of the instance appropriately (without actually removing the facilities from the metric graph). This extra factor is absorbed by the tilde notation, provided that each iteration of the for loop takes $\tilde{O}(n/k)$ rounds. We can also estimate the cost of a solution within a factor of $(1+O(\epsilon))$ factor in $\tilde{O}(n/k)$ rounds -- the details can be found in \cite{BandyapadhyayArxiv2017}. Since there are $O(\log n)$ candidate solutions to find a minimum-cost solution from, in line \ref{lin:minimum-cost}, this step can also be implemented in $\tilde{O}(n/k)$ rounds. 
    
    Each iteration of the for loop \ref{alg:outlierskmm} consists of two phases namely, the Radius Computation and Greedy Phases. We bound the running time of both these phases separately. By Lemma \ref{lem:facloc-guarantee} we know that the radius computation phase of Algorithm \ref{alg:outlierskmm} requires \(\tilde{O}(n/k)\) rounds. In the for loop on line \ref{alg4:for} there are at most $O(\log_{1 + \epsilon}{nN}) = O(\log nN) = O(\log n)$ possible values of \(i\) and hence at most $O(\log n)$ iterations (where \(N = \mbox{poly}(n)\) is the largest edge weight). Each individual step in the greedy phase of Algorithm \ref{alg:outlierskmm} takes \(\tilde{O}(n/k)\) rounds therefore we conclude that the overall running time is \(\tilde{O}(n/k)\) rounds.The proof of the approximation guarantee appears in the \ifthenelse{\boolean{short}}{full version \cite{InamdarPPArxiv2018}}{next section}.
  \end{proof}
  
  
  
  \subsection{Analysis of the Algorithm}
  % Note that we we exit the greedy phase if we either process all facilities or we break at line \ref{alg1:break}, each of which corresponds to the cases -- (i) $|O| > \ell$ and (ii) $|O| < \ell$ in the outlier determination phase(we are done if $|O| = \ell$).
  
  % Let \(C''\) and \(O''\) denote the sets \(C'\) and \(O'\) just before the outlier detection phase. To make the analysis easier, we consider a more costly solution $(\tilde{C}', F')$ where the set of clients $\tilde{C}'$ is constructed using the following modified outlier determination phase:
  
  % \begin{enumerate}
  % \item If $|O''| > \ell$, then let $O_1 \subseteq O''$ be a set of $|O''| - \ell$ clients that have smallest $v'_j$-values. In this case, we let $\tilde{C'} \gets C'' \cup O_1$, \quad $\tilde{O}' \gets O'' \setminus O_1$.
  
  % \item Otherwise, if $|O''| < \ell$. Let $O_2 \subseteq B(i, r_i) \cup C_i$ be the set of clients with largest $(\ell - |O''|)$ $v'_j$-values ($i$ is last iteration). In this case, we let $\tilde{C}' \gets C'' \setminus O_2$, \quad $\tilde{O}' \gets O'' \cup O_2$.
  % \end{enumerate}
  
  % It is easy to see that the $\cost_e(C', F') \le \cost_e(\tilde{C}', F')$, the outliers determined in the algorithm are at least as far from \(F'\) as ones in the modified outlier determination phase.
  
  Similar to the sequential algorithm analysis, we analyze the cost of the corresponding costlier solution $(\tilde{C}', F')$. In order to alleviate excessive notation, we will henceforth refer to the solution $(\tilde{C}', F')$ as $(C', F')$ and $\tilde{O}'$ as $O'$. We now restate the standard primal and dual for the Robust Facility Location problem.
  
  
  Let \(r_i\) be the radius value of \(i\) satisfying $f_i = \sum_{j \in B(i, r_i)} (r_i - c_{i j})$ and let \(\tilde{r}_i\) be the approximate radius value of \(i\) computed during Algorithm \ref{alg:outlierskmm}
  
  First, we construct a feasible dual solution $(v, w, \q)$. For a facility $i \in F$ and client $j \in C$, let  $w_{ij} \coloneqq \max\{0, r_i - c_{ij}\}$. Let $\q \coloneqq \max_{j \in C'} v'_{j}/(1 + \epsilon)^4$ (recall that  $v'_j \coloneqq \min_{i \in F} \max\{r_i, c_{ij}\} = \min_{i \in F} c_{ij} + w_{ij}$). Now, for a client $j \in C$, define $v_j$ as follows:
  $$v_j = \begin{cases}
    v'_j/(1 + \epsilon)^4 &\text{if } j \in C'
    \\\q &\text{if } j \in O'
  \end{cases}$$
  
  
  \begin{claim} \label{cl:apdx-bound_vj}
    If a client \(j \in C''\) then \(v_j' \le (1 + \epsilon)^2 \tilde{r}_i\) and if a client \(j \in O''\) then \(v_j' \ge (1 + \epsilon)^{-2} \tilde{r}_i\) where \(i\) is the last iteration of the for loop (lines \ref{alg4:for}-\ref{alg4:endfor})
  \end{claim}
  \begin{proof}
    If \(j \in C''\) then it must be added to \(C_{i'}\) for some iteration \(i' \le i\). Let us assume wlog that \(j\) was added to \(C_i\). Therefore, there must be some \(i' \in F_i\) such that \(j \in B(i', (1 + \epsilon) \tilde{r}_i)\). This means that \(c_{i'j} \le  (1 + \epsilon) \tilde{r}_i\) 
    \begin{align*}
      v'_j &= \min_{i \in F}{\max\{r_{i}, c_{ij}\}} \le \max\{r_{i'}, c_{i'j}\} \\
           &\le \max\{(1 + \epsilon)^{2} \tilde{r}_{i'}, (1 + \epsilon) \tilde{r}_{i'}\} \\
           &\le (1 + \epsilon)^2 \tilde{r}_i \tag{since we process facilities in increasing value of \(\tilde{r}\)}
    \end{align*}
    
    Since we compute approximate shortest paths, if client \(j\) is not added to any \(C_i\) then for all \(i' \in F_i\), \(j \notin B(i', \tilde{r}_i)\) (otherwise we would add \(j\) to \(C_i\)). Therefore if \(j \in O''\), for all \(i' \in F_i\), \(c_{i'j} > \tilde{r}_i\). So we have,
    \[v'_j = \min_{i \in F}{\max\{r_{i}, c_{ij}\}} \ge (1 + \epsilon)^{-2} \min_{i' \in F_i}{\max\{r_{i'}, c_{i'j}\}}\]
    
    Because for facilities \(i'' \in F \setminus F_i\), \(r_{i''} \ge (1 + \epsilon)^{-2} \tilde{r}_{i''} \ge (1 + \epsilon)^{-2} \tilde{r}_i\) as we process facilities in increasing order of \(\tilde{r}\). Therefore,
    \begin{align*}
      v'_j &\ge (1 + \epsilon)^{-2} \min_{i' \in F_i}{\max\{r_{i'}, \tilde{r}_i\}} \\
           &\ge (1 + \epsilon)^{-2} \min_{i' \in F_i}{\max\{(1 + \epsilon)^{-2}\tilde{r}_{i'}, \tilde{r}_i\}} \\
           &= (1 + \epsilon)^{-2} \tilde{r}_{i}
    \end{align*}
  \end{proof}
  
  
  \begin{lemma}
    The solution $(v, w, \q)$ is a feasible solution to the dual LP relaxation \ref{fig:outlier-primal-dual}.
  \end{lemma}
  \begin{proof}
    Note that constraints \ref{constr:fllpd-beta}, \ref{constr:fllpd-alphaub}, and \ref{constr:fllp-integral} of the dual are satisfied by construction and so is constraint \ref{constr:flld-alpha} for clients $j \in C'$. Therefore, in order to show that the solution $(v, w, \q)$ is feasible, we have to show that constraint \ref{constr:flld-alpha} is satisfied for all clients $j \in O'$. To this end, we consider the following two cases.
    
    \textbf{Case 1.} We enter the outlier determination phase after iterating over all facilities in $F$. Therefore, we have $|O''| > \ell$. This means that we identified a set \(O_1 \subset O''\) of size \(|O''| - \ell\) to be marked as non-outliers.
    
    As we iterate over all the facilities, by Claim \ref{cl:apdx-bound_vj}, for \(j \in O''\), we get $v'_j \ge \max_{i \in F}{\tilde{r}_i/(1 + \epsilon)^2}$.
    
    We put in \(O_1\) the clients from \(O''\) with smallest \(v'_j\)-value \footnote{Note this is not what the algorithm does but we have argued that the algorithm's solution is better than the solution we are analyzing}. This means that for all \(j \in O'\):
    \begin{align*}
      v'_j &\ge \max_{j \in O_1}{v'_j} \\
           &= \max\LR{\max_{j \in C''}{\frac{v'_j}{(1 + \epsilon)^4}}, \max_{j \in O_1}{v'_j}} \tag{\(\max_{j \in C''} {v'_j} \le \max_{i \in F} {(1 + \epsilon)^2\tilde{r}_i}\) by Claim \ref{cl:apdx-bound_vj}} \\
           &\ge \frac{1}{(1 + \epsilon)^4} \max\LR{\max_{j \in C''}{v'_j}, \max_{j \in O_1}{v'_j}} \\
           &\ge \frac{1}{(1 + \epsilon)^4} \max_{j \in C'}{v'_j} \\
           &\ge \q
    \end{align*}
    
    Therefore, we conclude that for any $j \in O'$ and $i \in F$, $c_{ij} + w_{ij} \ge v'_j \ge \q = v_j$.
    
    \textbf{Case 2.} We enter the outlier determination phase because of the break statement on line \ref{alg4:break}. Here, $|O''| \le \ell$ and \(C' \subseteq C''\)
    
    Let \(i^*\) be the last iteration of the for loop. Therefore \(F_{i^*}\) is the set of facilities we consider in the for loop and \(\max_{i \in F_{i^*}} {r_i} \le (1 + \epsilon)^2\tilde{r}_{i^*}\) We show that $\q \le (1 + \epsilon)^2\tilde{r}_{i^*}$.
    
    % Fix a client $j \in C$, and let $\i \in F$ be its bottleneck. Then $c_{\i j} + w_{\i j} = \max\{c_{\i j}, r_{\i}\} = v'_j$. Also, since \(v'_j = \max\{r_{\i}, c_{\i j}\}\), we get $v'_j \ge r_{\i} \ge \tilde{r}_i / (1 + \epsilon)^2$, and $v'_j \ge c_{\i j}$.
    
    Recall that by the case assumption we have $|O''| \le \ell$ and hence \(C' \subseteq C''\). All clients $j \in C''$ were part of \(C_i\) for some \(i \in F_{i^*}\) and by Claim \ref{cl:apdx-bound_vj} we have \(v'_j \le (1 + \epsilon)^2 \tilde{r}_i \le (1 + \epsilon)^2 \tilde{r}_{i^*}\). Therefore,
    $$\q = \max_{j \in C'} \frac{v'_j}{(1 + \epsilon)^4} \le \max_{j \in C''} \frac{v'_j}{(1 + \epsilon)^4} \le \max_{i \in F_{i^*}} \frac{\tilde{r}_i}{(1 + \epsilon)^2} = \frac{\tilde{r}_{i^*}}{(1 + \epsilon)^2}$$
    
    Let $j \in O'$ be an outlier client. If $j \in O''$, then for any facility $i \in F$,
    \begin{align*}
      c_{ij} + w_{ij} &\ge v'_j \\
                      &\ge (1 + \epsilon)^{-2} \tilde{r}_{i^*} \tag{by Claim \ref{cl:apdx-bound_vj}} \\
                      &\ge \q \\
                      &= v_j
    \end{align*}
    
    Otherwise, $j \in O_2$ and was added to $O'$ because it had highest $v'_j$ value among $C_{i^*}$. Therefore, by Claim \ref{cl:apdx-bound_vj} it follows that for any facility $i \in F$, $c_{ij} + w_{ij} \ge v'_j \ge \max_{j \in C'} v'_{j} (1 + \epsilon)^{-4} = \q = v_j$
    
    From the case analysis it follows that $v_j \le c_{ij} + w_{ij}$ for all $j \in O'$ and for all $i \in F$. Therefore, we have shown that $(v, w, \q)$ is a dual feasible solution.
  \end{proof}
  
  For the approximation guarantee we can now focus just on the clients in $C'$ because the only contribution that the clients in $O'$ make to the dual objective function is to cancel out the $- \ell \q$ term and hence they do not affect the approximation guarantee.We call a facility $\i$ the \emph{bottleneck} of $j$ if \(v'_j = \max\{r_{\i}, c_{\i j}\} =  c_{\i j} + w_{\i j}\).
  
  Throughout this section, we condition on the event that the outcome of all the randomized algorithms is as expected (i.e. the ``bad'' events do not happen). Note that this happens with w.h.p. We first need the following facts along the lines of \cite{Thorup2001}. 
  
  \begin{lemma}[Modified From Lemma 8 Of \cite{Thorup2001}] \label{lem:apdx-lem8}
    There exists a total ordering $\prec$ on the facilities in $F$ such that $u \prec v \implies \tilde{r}_u \le \tilde{r}_v$, and $v$ is added to $F'$ if and only if there is no previous $u \prec v$ in $F'$ such that $c_{u v} \le 2(1+\epsilon)^2 \tilde{r}_v$.
  \end{lemma}
  \begin{proof}[Proof Sketch]
    The ordering is obtained by enumerating the facilities processed in each iteration (with arbitrary order given to facilities in the same iteration). The facilities in $I$ that are included in $F'$ before the rest of the vertices of $W$. The lemma follows because of line \ref{alg4:remove-close-facs} (if $u$ and $v$ are processed in different iterations) the definition of $(\epsilon, d)$-approximate MIS (if $u$ and $v$ are processed in the same iteration).
  \end{proof}
  
  
  \begin{claim}[Modified From Claim 9.2 Of \cite{Thorup2001}] \label{cl:apdx-cl9.2}
    For any two distinct vertices $u, v \in F'$, we have that $c_{u v} > 2(1+\epsilon)^2 \cdot \max\{\tilde{r}_u, \tilde{r}_v\}$.
  \end{claim}
  \begin{proof}[Proof Sketch]
    Without loss of generality, assume that $u \prec v$, so $\tilde{r}_u \le \tilde{r}_v$. From Lemma \ref{lem:apdx-lem8} we have $c_{u v} > 2(1+\epsilon)^2 \tilde{r}_v \ge 2(1+\epsilon)^2 \tilde{r}_v$
  \end{proof}
  
  
  We now prove a few claims about the dual solution.
  
  \begin{claim} \label{cl:apdx-apx-1}
    For any $i \in F$ and $j \in C$, $\tilde{r}_i \le (1 + \epsilon)^2 (c_{ij} + w_{ij})$. Furthermore if for some \(i \in F\) and \(j \in C\), \(w_{ij} > 0\), then \(\tilde{r}_i \ge (1 + \epsilon)^{-2} (c_{ij} + w_{ij})\)
  \end{claim}
  \begin{proof}
    We have $w_{ij} = \max\{0, r_i - c_{ij}\} \ge r_i - c_{ij}$ and therefore $\tilde{r}_i \le (1 + \epsilon)^2 r_i \le (1 + \epsilon)^2 (c_{ij} + w_{ij})$.
    
    If for some \(i \in F\) and \(j \in C\), \(w_{ij} > 0\), then \(w_{ij} = r_i - c_{ij}\) which means that \(\tilde{r}_i \ge (1 + \epsilon)^{-2} r_i = (1 + \epsilon)^{-2} (c_{ij} + w_{ij})\)
  \end{proof}
  
  \begin{claim} \label{cl:apdx-apx-2}
    If $\i$ is a bottleneck for $j \in C'$, then $(1 + \epsilon)^2 v_j \ge \tilde{r}_{\i}$.
  \end{claim}
  \begin{proof}
    For $j \in C'$, we have
    \begin{align*}
      (1 + \epsilon)^2 v_j &= (1 + \epsilon)^{-2} v'_j \\
                           &= (1 + \epsilon)^{-2} \max\LR{c_{\i j}, r_{\i}} \\
                           &\ge (1 + \epsilon)^{-2} r_{\i} \ge \tilde{r}_{\i}
    \end{align*}
  \end{proof}
  
  \begin{claim} \label{cl:apdx-apx-3}
    If $\i \in F'$ is a bottleneck for $j \in C'$, then $w_{i'j} = 0$ for all $i' \in F'$, where $i' \neq \i$.
  \end{claim}
  \begin{proof}
    Assume for contradiction that $w_{i'j} > 0$, i.e., $r_{i'} \ge c_{i'j}$ for some $i' \in F', i' \neq \i$.
    
    If $r_{\i} \ge c_{\i j}$, then $v'_j = r_{\i} \le \max\LR{c_{i'j}, r_{i'}} = r_{i'}$. In this case, $c_{\i i'} \le c_{\i j} + c_{i' j} \le 2 r_{i'}$.
    
    Otherwise, if $c_{\i j} > r_{\i}$, then $v'_j = c_{\i j} \le \max\LR{c_{i'j}, r_{i'}} = r_{i'}$. Here too we have, $c_{\i i'} \le c_{\i j} + c_{i' j} \le 2r_{i'}$.
    
    In either case, $c_{\i i'} \le 2r_{i'} \le 2 \max\LR{r_{i'}, r_{\i}} \le 2 (1 + \epsilon)^2 \max\LR{\tilde{r}_{i'}, \tilde{r}_{\i}} $, which is a contradiction to Claim \ref{cl:apdx-cl9.2} since at most one of $i', \i$ can be added to $F'$.
  \end{proof}
  
  \begin{claim} \label{cl:apdx-apx-4}
    If a closed facility $\i \not\in F'$ is the bottleneck for $j \in C'$, and if an open facility $i' \in F'$ caused $\i$ to close, then $c_{i'j} \le 3(1+\epsilon)^8 \cdot v_j$.
  \end{claim}
  \begin{proof}
    \begin{align*}
      c_{i'j} &\le c_{i'\i} + c_{\i j} \tag{Triangle inequality} \\
              &\le 2(1+\epsilon)^2 \tilde{r}_{\i} + c_{\i j} \tag{$i'$ caused $i$ to close, so using Lemma \ref{lem:apdx-lem8}.} \\
              &\le 2(1+\epsilon)^2 \cdot (1+\epsilon)^2 (w_{\i j} + c_{\i j}) + c_{\i j} \tag{Using Claim \ref{cl:apdx-apx-1}.} \\
              &\le 2(1+\epsilon)^4 w_{\i j} + (2(1+\epsilon)^4 + 1) \cdot c_{\i j} \\
              &\le \max \{2(1+\epsilon)^4, 2(1+\epsilon)^4 + 1 \} \cdot v'_j \tag{Since $\i$ is the bottleneck for $j$} \\
              &\le 3(1+\epsilon)^8 \cdot v_j \tag{By definition of $v_j$}
    \end{align*}
  \end{proof}
  
  Now we state the main lemma that uses the dual variables for analyzing the cost.
  
  \begin{lemma} \label{lem:apdx-outlier-lemma}
    For any $j \in C'$, there is some $i \in F'$ such that $c_{ij} + w_{ij} \le 3(1 + \epsilon)^8 v_j$.
  \end{lemma}
  \begin{proof}
    Fix a client $j \in C'$, and let $\i$ be its bottleneck. We consider different cases. Here, $c_{ij}$ should be seen as the connection cost of $j$, and $w_{ij}$, the cost towards opening of a facility (if $w_{ij} > 0$).
    
    \begin{itemize}
    \item \textbf{Case 1:} $i \in F'$.
      
      In this case, by Claim \ref{cl:apdx-apx-3}, $w_{i'j} = 0$ for all other $i' \in F'$.
      
      If $c_{\i j} < r_{\i}$, then $w_{\i j} > 0$, and $v'_j = c_{\i j} + w_{\i j}$. Therefore, $(1 + \epsilon)^4v_j = v'_j$ pays for the connection cost as well as towards the opening cost of $\i$.
      
      Otherwise, if $c_{\i j} \ge r_{\i}$, then $w_{\i j} = 0$. Also, $w_{i'j} = 0$ for all other $i' \in F'$. Therefore, $j$ does not contribute towards opening of any facility in $F'$. Also, we have $v'_j = \max\LR{c_{\i j}, r_{\i}} = c_{\i j}$, i.e., $(1 + \epsilon)^4v_j$ pays for $j$'s connection cost.
      
    \item \textbf{Case 2:} $\i \notin F'$ and $w_{ij} = 0$ for all $i \in F'$.
      
      Let $i' \in F'$ be the facility that caused $\i$ to close. From Claim \ref{cl:apdx-apx-4}, we have $c_{i' j} \le 3(1 + \epsilon)^8 v_j$, i.e. $3(1 + \epsilon)^8 v_j$ pays for the connection cost of $j$.
      
    \item \textbf{Case 3:} $\i \notin F'$, and there is some $i' \in F'$ with $w_{i'j} > 0$. But $i'$ did not cause $\i$ to close.
      
      Since $w_{i'j} > 0$, by Claim \ref{cl:apdx-apx-1} $\tilde{r}_{i'}\ge (1 + \epsilon)^{-2} (c_{i'j} + w_{i'j})$.
      
      Let $i$ be the facility that caused $\i$ to close. Therefore, by Claim \ref{cl:apdx-apx-4}, we have $c_{i j} \le  3(1 + \epsilon)^8 v_j$.
      
      Now, since $i$ and $i'$ both belong to $F'$, by Claim \ref{cl:apdx-cl9.2}, $c_{i' i} > 2(1+\epsilon)^2 \max \LR{\tilde{r}_i, \tilde{r}_{i'}} \ge 2\tilde{r}_{i'} \ge 2(c_{i' j} + w_{i'j})$.
      
      Therefore, by triangle inequality, $c_{ij} + c_{i'j} \ge c_{i'i} > 2(c_{i' j} + w_{i'j})$ which implies $c_{ij} > c_{i'j} + 2w_{i'j}$.
      
      It follows that, $c_{i' j} + w_{i'j} \le c_{i' j} + 2w_{i'j} < c_{ij} \le 3(1 + \epsilon)^8 v_j$. Therefore, $3(1 + \epsilon)^8v_j$ pays for the connection cost of $j$ to $i'$, and its contribution towards opening of $i'$.
      
    \item \textbf{Case 4:} $\i \not\in F'$, but for $i' \in F'$ that caused $\i$ to close has $w_{i'j} > 0$.
      
      Again, since $w_{i'j} > 0$, by Claim \ref{cl:apdx-apx-1} $\tilde{r}_{i'}\ge (1 + \epsilon)^{-2} (c_{i'j} + w_{i'j})$.
      
      From Claim \ref{cl:apdx-apx-2}, we have that $(1 + \epsilon)^2 v_j \ge \tilde{r}_{\i}$. Then, since $i'$ caused $\i$ to close, we have $\tilde{r}_{\i} \ge \tilde{r}_{i'} \ge (1 + \epsilon)^{-2} (c_{i'j} + w_{i'j})$. This implies $(1 + \epsilon)^4 v_j \ge c_{i'j} + w_{i'j}$, i.e., $(1 + \epsilon)^4 v_j$ pays for the connection cost of $j$ to $i'$, and its contribution towards opening of $i'$.
    \end{itemize}  
  \end{proof}
  
  Now we are ready to prove the approximation guarantee of the algorithm.
  \begin{theorem} \label{thm:apdx-outlier-theorem}
    $\cost_e(C', F') \le 3(1 + \epsilon)^8 \cdot \cost_e(C^*_e, F^*_e) + f_{i_e}$
  \end{theorem}
  \begin{proof}
    Recall that $f_{i_e}$ denotes the cost of the most expensive facility in an optimal solution. Furthermore, notice that for any facility $i \in F' \setminus \{i^*\}$, the clients in the ball $B(i, r_i) \subseteq C'$. However, if $i^* \in F'$, some of the clients in $B(i^*, r_{i^*})$ may have been removed in the outlier determination phase, and therefore it may not get paid completely by the dual variables $v_j$. Therefore,
    
    \begin{align*}
      \cost_e(C', F') &= \sum_{j \in C'} d(j, F) + \sum_{i \in F' \setminus \{i^*\} } f_i + f_{i^*}
      \\&\le 3 (1 + \epsilon)^8 \cdot \sum_{j \in C'} v_j + f_{i^*} \tag{From Lemma \ref{lem:apdx-outlier-lemma}}
      \\&= 3(1 + \epsilon)^8 \cdot \lr{\sum_{j \in C} v_j - \q\ell} + f_{i^*} \tag{For $j \in O'$, $v_j = \q$, and $|O'| = \ell$}
      \\&\le 3(1 + \epsilon)^8 \cdot \lr{\sum_{j \in C} v_j - \q\ell} + f_{i_e} \tag{Since $i_e$ is the most expensive facility}
    \end{align*}
    Since $(v, w, \q)$ is a feasible dual solution, its cost is a lower bound on the cost of any integral optimal solution. Therefore, the theorem follows.
  \end{proof}
  
  Therefore, we can apply corollary \ref{cor:modified-cost} with $\alpha = 1 + \epsilon, \beta = 3(1 + \epsilon)^8, \gamma = 1$ to get the following approximation guarantee --
  
  \begin{theorem}
    The solution returned by Algorithm \ref{alg:outlierskmm} is a \(5 + O(\epsilon)\) approximation to the Robust Facility Location problem
  \end{theorem}
}
%  LocalWords:  FacLoc MPC SSSP undirected Multi MSSP ExclusiveMSSP
%  LocalWords:  boxruled RadiusComputation discretizing MIS subgraph
%  LocalWords:  whp NbdSizeEstimates ApproximateMIS RobustFacLocDist
%  LocalWords:  Outlier wlog outlier outliers 's et al BCC
