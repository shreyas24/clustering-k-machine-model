\section{Sequential Algorithms for Facility Location}

At the heart of our $k$-machine algorithm for \facloc\ is the well-known sequential algorithm of Mettu and Plaxton \cite{MettuPlaxtonSICOMP2003}, that computes a 
3-approximation for \facloc.
To describe the Mettu-Plaxton algorithm (Algorithm \ref{alg:MP}, henceforth, MP algorithm), we need some notation. 
The algorithm first computes a ``radius'' $r_i$ for each facility $i \in F$ and it then greedily picks facilities to open in non-decreasing order
of radii provided no previously opened facility is too close.
The ``radius'' of a facility $i$ is the amount that each client is charged for the opening of facility $i$.
Clients pay towards this charge after paying towards the cost of connecting to facility $i$; clients that have a large connection cost to $i$
pay nothing towards this charge.
\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{FacilityLocationMP}\((F, C)\)}\label{alg:MP}
	\tcc{Radius Computation Phase:}
	For each $i \in F$, compute $r_i \ge 0$, satisfying $f_i = \sum_{j \in C} \max\{0, r_i - c_{ij}\}$. \\
	\tcc{Greedy Phase:}
	Sort and renumber facilities in the non-decreasing order of $r_i$. \\
	$F' \gets \emptyset$ \Comment{Solution set}\\
	\For{$i = 1, 2, \ldots$} { 
		\If{there is no facility in $F'$ within distance $2r_i$ from $i$} {
			$F' \gets F' \cup \{i\}$
		}
	}
	Connect each client $j$ to its closest facility in $F'$.
\end{algorithm2e}

It is shown in \cite{MettuPlaxtonSICOMP2003} using a charging argument that Algorithm \ref{alg:MP} is 3-approximation for the Metric \textsc{FacLoc} problem. Later on, \cite{ArcherRSESA2003} gave a primal-dual analysis, showing the same approximation guarantee, by comparing the cost of the solution to a dual feasible solution. We use the latter analysis approach as it can be easily modified to work for the algorithms with outliers. 

For a facility $i \in F$ and a client $j \in C$, we use the shorthand $c_{ij} \coloneqq d(i, j)$. Also, for a facility $i \in F$ and a radius $r \ge 0$, let $B(i, r)$ denote the set of clients within the distance $r$, i.e., $B(i, r) \coloneqq \{j \in C\mid c_{ij} \le r \}$.

\subsection{Robust Facility Location}
\label{sec:robust-fl-seq}
Since we use the primal dual analysis of \cite{ArcherRSESA2003} to get a bounded approximation factor, we need to address the fact 
that the standard linear programming relaxation for Robust \textsc{FacLoc} has unbounded integrality gap. 
To fix this we modify the instance in a similar manner to \cite{CharikarSODA2001}.
Let $(C^*, F^*)$ be a fixed optimal solution, and let $i_* \in F$ be a facility in that solution with the maximum opening cost $f_{i_*}$. 
We begin by assuming that we are given a facility, say $i_e$ with opening cost $f_{i_e}$, such that, $f_{i_*} \le f_{i_e} \le \alpha f_{i_*}$, 
where $\alpha \ge 1$ is a constant. 
Now, we modify the original instance by changing the opening costs of the facilities as follows. 

$$f'_i = \begin{cases}
+\infty &\text{ if $f_i > f_{i_e}$}
\\0 &\text{ if $i = i_e$}
\\f_i &\text{ otherwise}
\end{cases}$$

Note that we can remove the facilities with opening cost $+\infty$ without affecting the cost of an optimal solution, and hence we assume that w.l.o.g. all the modified opening costs $f'_i$ are finite.

Let $(C^*_e, F^*_e)$ be an optimal solution for this modified instance, and let $\cost_e(C^*_e, F^*_e)$ be its cost using the modified opening costs. Observe that without loss of generality, we can assume that $i_e \in F^*_e$, since its opening cost $f'_{i_e}$ equals $0$.
We obtain the following lemma and its simple corollary.

\begin{lemma} \label{lem:preproc}
	$\cost_e(C^*_e, F^*_e)  \le \cost(C^*, F^*)$.
\end{lemma} 
\ifthenelse{\boolean{short}}{}{
  \begin{proof}	
    Recall that $i_e$ satisfies $f_{i_*} \le f_{i_e} \le \alpha f_{i_*}$ where $i_*$ is the facility with largest opening cost in $F^*$. In the modified instance, all facilities with opening cost greater than $f_{i_e}$ are removed, however, no facility from $F^*$ is removed, because $f_{i_*} \le f_{i_e}$. Therefore, $(C^*, F^*)$ is a feasible solution for the modified instance. This implies $\cost_e(C^*_e, F^*_e) \le \cost_e(C^*, F^*)$, which follows from the optimality of $(C^*_e, F^*_e)$. Finally, recall that for any facility $i \in F^*$, $f'_i = f_i$, and hence $\cost_e(C^*, F^*) = \cost(C^*, F^*)$.
  \end{proof}
}

\begin{corollary} \label{cor:modified-cost}
  Let $(C'_e, F'_e)$ be a feasible solution for the instance with modified facility opening costs, such that, $\cost_e(C'_e, F'_e) \le \beta \cdot \cost_e(C^*_e, F^*_e) + \gamma \cdot f_{i_e}$ (where $\beta \ge 1, \gamma \ge 0$). Then, $(C'_e, F'_e)$ is a $\beta + \alpha \cdot (\gamma + 1)$ approximation for the original instance.
\end{corollary}
\ifthenelse{\boolean{short}}{}{
  \begin{proof}
    Consider,
    \begingroup
    \allowdisplaybreaks
    \begin{align*}
      \cost(C'_e, F'_e) &\le \cost_e(C'_e, F'_e) + f_{i_e} \tag{if $i_e \in F'_e$} 
      \\&\le \beta \cdot \cost_e(C^*_e, F^*_e) + (\gamma+1) \cdot f_{i_e} 
      \\&\le \beta \cdot \cost(C^*, F^*) + (\gamma + 1) \cdot f_{i_e} \tag{From lemma \ref{lem:preproc}}
      \\&\le \beta \cdot \cost(C^*, F^*) + \alpha \cdot (\gamma + 1) \cdot f_{i_*} \tag{$f_{i_e} \le \alpha f_{i_*}$}
      \\&\le \beta \cdot \cost(C^*, F^*) + \alpha \cdot (\gamma + 1) \cdot \cost(C^*, F^*) \tag{$f_{i_*} \le \cost(C^*, F^*)$}
    \end{align*}
    \endgroup
  \end{proof}
}

To efficiently find a facility $i_e$ satisfying $f_{i_*} \le f_{i_e} \le \alpha f_{i_*}$, 
we partition the facilities into sets where each set contains facilities with opening costs from the range $\left[(1+\epsilon)^i, (1+\epsilon)^{i+1}\right)$. Iterating over all such ranges, and choosing a facility with highest opening cost from that range, we are guaranteed to find a facility $i_e$ such that, $f_{i_*} \le f_{i_e} \le (1 + \epsilon) f_{i_*}$ \footnote{An alternative approach would be to consider each facility one-by-one as a candidate, but for an efficient distributed implementation we can only afford $O(\log n)$ distinct guesses.}. The total number of such iterations will be \(O(\log_{1+\epsilon} \frac{f_{\max}}{f_{\min}})\), where $f_{\max}$ is the largest opening cost, and $f_{\min}$ is the smallest non-zero opening cost. Assuming that every individual item in the input (e.g., facility opening costs,
connection costs, etc.) can each be represented in $O(\log n)$ bits and that $\epsilon$ is a constant, this amounts to $O(\log n)$ iterations.

%Henceforth, we assume that we  we focus on finding a solution $(C'_e, F'_e)$ with such an approximation guarantee. 
Our facility location algorithm is described in Algorithm \ref{alg:outlierSeq}. This algorithm can be thought of as running $O(\log n)$ separate instances of a modified version of the original Mettu-Plaxton algorithm (Algorithm \ref{alg:MP}), where in each instance of the Mettu-Plaxton algorithm, the algorithm is terminated as soon as the number of outlier clients drops below the required number, following which there is some post-processing.

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{RobustFacLoc}\((F, C, p)\)}\label{alg:outlierSeq}
  \tcc{Recall: $\ell \coloneqq |C| - p$}
  
  \For{$t = 0, \cdots, O(\log n)$} {
    Let $i_e \in F$ be the most expensive facility from the facilities with opening costs in the range $[(1 + \epsilon)^t, (1 + \epsilon)^{t+1})$ for some small constant $\epsilon>0$ \label{lin:i-e_chosen}\\
    Modify the facility opening costs to be
    $$f'_i = \begin{cases}
      +\infty &\text{ if $f_i > f_{i_e}$}
      \\0 &\text{ if $i = i_e$}
      \\f_i &\text{ otherwise}
    \end{cases}$$ \\
    \tcc{Radius Computation Phase:}
    For each $i \in F$, compute $r_i \ge 0$, satisfying $f'_i = \sum_{j \in C} \max\LR{0, r_i - c_{ij}}$. \\
    \tcc{Greedy Phase:}
    Sort and renumber facilities in the non-decreasing order of $r_i$. \\
    Let $C' \gets \emptyset$, $F' \gets \emptyset$, $O' \gets C$ \\
    Let $F_0 \gets \emptyset$ \\
    \For{$i = 1, 2, \ldots$} { \label{alg1:for}
      \If{there is no facility in $F'$ within distance $2r_i$ from $i$} {
        $F' \gets F' \cup \{i\}$
      }
      $F_i \gets F_{i-1} \cup \{i\}$ \\
      Let $C_i$ denote the set of clients that are within distance \(r_i\) \label{alg1:remove_clients}\\
      $C' \gets C' \cup  C_i$, \quad $O' \gets O' \setminus  C_i$. \\
      \textbf{if} $|O'| \le \ell$ \textbf{then break} \label{alg1:break} \\
    }\label{alg1:endfor}
    \tcc{Outlier Determination Phase:}
    \If{$|O'| > \ell$} {
      Let $O_1 \subseteq O'$ be a set of $|O'| - \ell$ clients that are closest to facilities in $F'$. \\
      $C' \gets C' \cup O_1$, \quad $O' \gets O' \setminus O_1$.
    }
    \ElseIf{$|O'| < \ell$} {
      Let $O_2 \subseteq C'$ be the set of $\ell - |O'|$ clients with largest distance to open facilities $F'$. \\
      $C' \gets C' \setminus O_2$, \quad $O' \gets O' \cup O_2$.
    }
    Let $(C'_t, F'_t) \gets (C', F')$ 
  }

  \Return Return $(C'_t, F'_t)$ with the minimum cost.
\end{algorithm2e}
We abuse the notation slightly, and denote by $(C', F')$ the solution returned by the algorithm, i.e., the solution $(C'_t, F'_t)$ corresponding to the iteration $t$ of the outer loop that results in a minimum cost solution. Similarly, we denote by $i_e$ a facility chosen in line \ref{lin:i-e_chosen} in the iteration corresponding to this iteration $t$.

\ifthenelse{\boolean{short}}{}{
Moreover, we will consider the facility costs $f'$ to be the modified facility opening costs in the same iteration $t$. We can ignore the facilities with opening cost $+\infty$ and add $i_e$ to any solution with no additional cost. Therefore in our analysis, we just ignore these facilities and use the original facility costs $f$ for other facilities since they are the same as the modified costs.

Note that we exit the greedy phase if we either process all facilities or we break at line \ref{alg1:break}, each of which corresponds to the cases -- (i) $|O'| > \ell$ where some outliers become clients and (ii) $|O'| < \ell$ where some clients become outliers again, in the outlier determination phase (we are done if $|O'| = \ell$).

  Let \(C''\) and \(O''\) denote the sets \(C'\) and \(O'\) just before the outlier determination phase. Note that we we exit the greedy phase if we either process all facilities or we break at line \ref{alg1:break}, each of which corresponds to the cases -- (i) $|O| > \ell$ and (ii) $|O| < \ell$ in the outlier determination phase (we are done if $|O| = \ell$).
  
  For a client $j \in C$, let $v'_j \coloneqq \min_{i \in F} \max\{r_i, c_{ij}\}$. To make the analysis easier, we consider a more expensive solution $(\tilde{C}', F')$ where the set of clients $\tilde{C}'$ is constructed using the following modified outlier determination phase:
  
  \begin{enumerate}
  \item If $|O''| > \ell$, then let $O_1 \subseteq O''$ be a set of $|O''| - \ell$ clients that have the smallest $v'_j$-values. In this case, we let $\tilde{C'} \gets C'' \cup O_1$, \quad $\tilde{O}' \gets O'' \setminus O_1$.
    
  \item Otherwise, if $|O''| < \ell$. Let $O_2 \subseteq C_i$ be the set of clients with largest $\ell - |O''|$ $v'_j$-values ($i$ is last iteration). In this case, we let $\tilde{C}' \gets C'' \setminus O_2$, \quad $\tilde{O}' \gets O'' \cup O_2$.
  \end{enumerate}
  
  It is easy to see by an exchange argument that the $\cost_e(C', F') \le \cost_e(\tilde{C}', F')$, the outliers determined in the algorithm are at least as far from \(F'\) as ones in the modified outlier determination phase. Henceforth, we analyze the cost of the solution $(\tilde{C}', F')$ by comparing it to the cost of a feasible dual \LP solution and in order to alleviate excessive notation, we will henceforth refer to the solution $(\tilde{C}', F')$ as $(C', F')$ and $\tilde{O}'$ as $O'$. We state the standard primal and dual linear programming relaxations for the Robust \textsc{FacLoc} problem in Figure \ref{fig:outlier-primal-dual}
  
  \begin{figure}
    \centering
    \makebox[\linewidth][c]{
      
      \begin{minipage}{0.51\linewidth}
        \begin{mdframed}[backgroundcolor=gray!9]
          \fontsize{9}{9.4}\selectfont
          \begin{alignat}{3}
            \text{minimize}   \displaystyle&\sum\limits_{i \in F} f_i y_i + \sum_{i \in F, j \in C} c_{ij} x_{ij} & \nonumber\\
            \text{subject to } \displaystyle& z_j + \sum\limits_{i \in F} x_{ij} \geq 1,  \quad &\forall j \in C \label{constr:fllp-coverage} \\
            \displaystyle&\qquad\quad\ \   x_{ij} \le y_i, &\forall  i \in F, \forall j \in C  \label{constr:fllp-open}\\
            \displaystyle&\qquad\ \sum_{j \in C} z_j \le \ell, \label{constr:fllp-outlier}\\
            \displaystyle&\quad r_j, y_i, x_{ij} \in [0, 1] &\forall i \in F,\forall j \in C \label{constr:fllp-nonneg}
          \end{alignat}
          \centering Primal LP
        \end{mdframed}
      \end{minipage}
      \makebox[0.49\linewidth][c]{
        \begin{minipage}{0.49\linewidth}
          \begin{mdframed}[backgroundcolor=gray!9]
            \fontsize{9}{10}\selectfont
            \begin{alignat}{3}
              \text{maximize}\displaystyle&\qquad \sum\limits_{j \in C} v_j - \ell \cdot \q & \nonumber\\
              \text{subject to } \displaystyle&\qquad v_j \le c_{ij} + w_{ij},  \quad &\forall j \in C \label{constr:flld-alpha} \\
              \displaystyle& \sum_{j \in C} w_{ij} \le f_i &\forall i \in F \label{constr:fllpd-beta}\\
              \displaystyle&\qquad\ v_j \le \q &\forall j \in C \label{constr:fllpd-alphaub}\\
              \displaystyle&\quad v_j, w_{ij} \ge 0 &\forall i \in F,\forall j \in C \label{constr:fllp-integral}
              \\\nonumber
            \end{alignat}
            \centering Dual LP
          \end{mdframed}
        \end{minipage}}
    }
    \caption{Primal and Dual Linear Programming Relaxations for Robust \textsc{FacLoc} \label{fig:outlier-primal-dual}}
  \end{figure}
  Now, we construct a feasible dual solution $(v, w, \q)$.
  
  For a facility $i \in F$ and client $j \in C$, let  $w_{ij} \coloneqq \max\{0, r_i - c_{ij}\}$. Let $\q \coloneqq \max_{j \in C'} v'_{j}$ (recall that  $v'_j \coloneqq \min_{i \in F} \max\{r_i, c_{ij}\} = \min_{i \in F} {c_{ij} + w_{ij}}$). Now, for a client $j \in C$, define $v_j$ as follows:
  $$v_j = \begin{cases}
    v'_j &\text{if } j \in C'
    \\\q &\text{if } j \in O'
  \end{cases}$$
  
  \begin{claim} \label{claim:bound_vj}
    A client \(j \in C_i\) iff \(v_j' \le r_i\)
  \end{claim}
  \begin{proof}
    Client \(j\) is added to \(C_i\) iff for some \(i' \in F_i\), \(j \in B(i', r_i)\). This means \(c_{i'j} \le r_i\) and \(r_{i'} \le r_i\) since we process facilities in increasing order of \(r\)-value. This means \(v'_j \le \max\{r_{i'}, c_{i'j}\} \le r_i\)
  \end{proof}
  
  \begin{lemma}
    The solution $(v, w, \q)$ is a feasible solution to the dual LP relaxation \ref{fig:outlier-primal-dual}.
  \end{lemma}
  \begin{proof}
    Note that constraints \ref{constr:fllpd-beta}, \ref{constr:fllpd-alphaub}, and \ref{constr:fllp-integral} of the dual are satisfied by construction and so is constraint \ref{constr:flld-alpha} for clients $j \in C'$. Therefore, in order to show that the solution $(v, w, \q)$ is feasible, we have to show that constraint \ref{constr:flld-alpha} is satisfied for all clients $j \in O'$. To this end, we consider the following two cases.
    
    \textbf{Case 1.} We enter the outlier determination phase after iterating over all facilities in $F$. Therefore, we have $|O''| > \ell$. This means that we identified a set \(O_1 \subset O''\) of size \(|O''| - \ell\) to be marked as non-outliers.
    
    As we iterate over all the facilities, if $j \in O''$ then by claim \ref{claim:bound_vj}, $v'_j > \max_{i \in F}{r_i} \ge \max_{j \in C''}{v'_j}$.
    
    We put in \(O_1\) the clients from \(O''\) that have smallest \(v'_j\)-values. This means that for all \(j \in O'\), \(v'_j \ge \max_{j \in O_1}{v'_j} = \max_{j \in C'' \cup O_1}{v'_j} = \max_{j \in C'}{v'_j} = \q\) where the second equality is because for \(v'_j\) for \(j \in C''\) is at most \(v'_{j'}\) for \(j' \in O_1\). 
    
    Therefore, we conclude that for any $j \in O'$ and $i \in F$, $c_{ij} + w_{ij} \ge v'_j \ge \q = v_j$.
    
    \textbf{Case 2.} We enter the outlier determination phase because of the break statement on line \ref{alg1:break}. Here, $|O''| \le \ell$ and \(C' \subseteq C''\)
    
    Let \(i^*\) be the last iteration of the for loop. Therefore \(F_{i^*}\) is the set of facilities we consider in the for loop. 
    
    Recall that by the case assumption we have $|O''| \le \ell$ and therefore $C' \subseteq C''$. All clients \(j \in C''\) were part of \(C_i\) for some \(i \in F_{i^*}\) and by claim \ref{claim:bound_vj} we have \(v'_j \le r_i \le r_{i^*}\). Therefore,
    $$\q = \max_{j \in C'} v_j \le \max_{j \in C''} v_j \le r_{i^*}.$$
    
    Let $j \in O'$ be an outlier client. If $j \in O''$, then for any facility $i \in F$,
    \begin{align*}
      c_{ij} + w_{ij} &\ge v'_j \\
                      &\ge r_{i^*} \tag{Otherwise $j$ would be in $C''$ by claim \ref{claim:bound_vj}} \\
                      &\ge \q
                        = v_j
    \end{align*}
    
    Otherwise, $j \in O_2$ and was added to $O'$ because it had highest $v'_j$ value among $C_{i^*}$. Therefore, it follows that for any facility $i \in F$, $c_{ij} + w_{ij} \ge v'_j \ge \max_{j \in C'} v'_{j} = \q = v_j$
    
    From the case analysis it follows that $v_j \le c_{ij} + w_{ij}$ for all $j \in O'$ and for all $i \in F$. Therefore, we have shown that $(v, w, \q)$ is a dual feasible solution.
  \end{proof}
  
  For the approximation guarantee we can focus just on the clients in $C'$ because the only contribution that the clients in $O'$ make to the dual objective function is to cancel out the $- \ell \q$ term and hence they do not affect the approximation guarantee. We call a facility $\i$ the \emph{bottleneck} of $j$ if \(v'_j = \max\{r_{\i}, c_{\i j}\} =  c_{\i j} + w_{\i j}\). We first prove a few straightforward claims about the dual solution.
  
  \begin{claim} \label{cl:apx-1}
    For any $i \in F$ and $j \in C$, $r_i \le c_{ij} + w_{ij}$. Moreover if $w_{ij} > 0$ then $r_i = c_{ij} + w_{ij}$
  \end{claim}
  \begin{proof}
    $w_{ij} = \max\{0, r_i - c_{ij}\} \ge r_i - c_{ij}$. Now if $w_{ij} > 0$ then $r_i > c_{ij}$ and we have $w_{ij} = r_i - c_{ij}$ which implies the claim.
  \end{proof}
  
  \begin{claim} \label{cl:apx-2}
    If $\i$ is a bottleneck for $j \in C'$, then $v_j \ge r_{\i}$.
  \end{claim}
  \begin{proof}
    For $j \in C'$, $v_j = v'_j = \max\LR{c_{\i j}, r_{\i}} \ge r_{\i}$.
  \end{proof}
  
  \begin{claim} \label{cl:apx-3}
    If $\i \in F'$ is a bottleneck for $j \in C'$, then $w_{i'j} = 0$ for all $i' \in F'$, where $i' \neq \i$.
  \end{claim}
  \begin{proof}
    Assume for contradiction that $w_{i'j} > 0$, i.e., $r_{i'} \ge c_{i'j}$.
    
    If $r_{\i} \ge c_{\i j}$, then $v_j = r_{\i} \le c_{i'j} + w_{i'j} = r_{i'}$. In this case, $c_{\i i'} \le c_{\i j} + c_{i' j} \le 2 r_{i'}$.
    
    Otherwise, if $c_{\i j} > r_{\i}$, then $v_j = c_{\i j} \le r_{i'}$. Here too we have, $c_{\i i'} \le c_{\i j} + c_{i' j} \le 2r_{i'}$.
    
    In either case, $c_{\i i'} \le 2r_{i'} \le 2 \max\LR{r_{i'}, r_{\i}}$, which is a contradiction, since at most one of $i', \i$ can be added to $F'$.
  \end{proof}
  
  \begin{claim} \label{cl:apx-4}
    If a closed facility $\i \not\in F'$ is the bottleneck for $j \in C'$, and if an open facility $i' \in F'$ caused $\i$ to close, then $c_{i' j} \le 3 v_j$.
  \end{claim}
  \begin{proof}
    $\i$ is a bottleneck for $j$, so $v_j \ge c_{\i j}$, and $v_j \ge r_{\i}$.
    
    Since $i' \in F'$ caused $\i$ to close, then $c_{k i} \le 2r_{\i} \le 2 v_j$.
    Therefore, $c_{i' j} \le c_{k i} + c_{i j} \le 3 v_j$.
  \end{proof}
  
  Now we state the main lemma that uses the dual variables for analyzing the cost.
  
  \begin{lemma} \label{lem:outlier-lemma}
    For any $j \in C'$, there is some $i \in F'$ such that $c_{ij} + w_{ij} \le 3 v_j$.
  \end{lemma}
  \begin{proof}
    Fix a client $j \in C'$, and let $\i$ be its bottleneck. We consider different cases. Here, $c_{ij}$ should be seen as the connection cost of $j$, and $w_{ij}$, the cost towards opening of a facility (if $w_{ij} > 0$).
    
    \begin{itemize}
    \item \textbf{Case 1:} $\i \in F'$.
      
      In this case, by claim \ref{cl:apx-3}, $w_{i'j} = 0$ for all other $i' \in F'$.
      
      If $c_{\i j} < r_{\i}$, then $w_{\i j} > 0$, and $v_j = c_{\i j} + w_{\i j}$. Therefore, $v_j$ pays for the connection cost as well as towards the opening cost of $\i$.
      
      Otherwise, if $c_{\i j} \ge r_{\i}$, then $w_{\i j} = 0$. Also, $w_{i'j} = 0$ for all other $i' \in F'$. Therefore, $j$ does not contribute towards opening of any facility in $F'$. Also, we have $v_j = \max\LR{c_{\i j}, r_{\i}} = c_{\i j}$, i.e., $v_j$ pays for $j$'s connection cost.
      
    \item \textbf{Case 2:} $\i \notin F'$ and $w_{ij} = 0$ for all $i \in F'$.
      
      Let $i' \in F'$ be the facility that caused $\i$ to close. From claim \ref{cl:apx-4}, we have that $c_{i' j} \le 3 v_j$, i.e. $3 v_j$ pays for the connection cost of $j$.
      
    \item \textbf{Case 3:} $\i \notin F'$, and there is some $i' \in F'$ with $w_{i'j} > 0$. But $i'$ did not cause $\i$ to close.
      
      Since $w_{i'j} > 0$, by claim \ref{cl:apx-1} $w_{i'j} = r_{i'} - c_{i'j}$ and $r_{i'} > c_{i'j}$.
      
      Let $i$ be the facility that caused $\i$ to close. Therefore, $c_{\i i} \le 2r_{\i}$. Also, $c_{i j} \le c_{i \i} + c_{\i j} \le 2r_{\i} + c_{\i j} \le 3v_j$.
      
      Now, since $i$ and $i'$ both belong to $F'$, $c_{i' i} > 2\max \LR{r_i, r_{i'}} \ge 2r_{i'} = 2(c_{i' j} + w_{i'j})$.
      
      Therefore, by triangle inequality, $c_{ij} + c_{i'j} \ge c_{i'i} > 2(c_{i' j} + w_{i'j})$ which implies $c_{ij} > c_{i'j} + 2w_{i'j}$.
      
      It follows that, $c_{i' j} + w_{i'j} \le c_{i' j} + 2w_{i'j} < c_{i j} \le 3 v_j$. Therefore, $3v_j$ pays for the connection cost of $j$ to $i'$, and its contribution towards opening of $i'$.
      
    \item \textbf{Case 4:} $\i \not\in F'$, but for $i' \in F'$ that caused $\i$ to close has $w_{i'j} > 0$.
      
      Again, since $w_{i'j} > 0$, by claim \ref{cl:apx-1} $w_{i'j} = r_{i'} - c_{i'j}$ and $r_{i'} > c_{i'j}$.
      
      From claim \ref{cl:apx-2}, we have that $v_j \ge r_{\i}$. Then, since $i'$ caused $\i$ to close, we have $r_{\i} \ge r_{i'} = c_{i'j} + w_{i'j}$. This implies $v_j \ge c_{i'j} + w_{i'j}$, i.e., $v_j$ pays for the connection cost of $j$ to $i'$, and its contribution towards opening of $i'$.
    \end{itemize}  
  \end{proof}

  Now we are ready to prove the approximation guarantee of the algorithm.
}
\begin{theorem} \label{thm:outlier-theorem}
  $\cost_e(C', F') \le 3 \cdot \cost_e(C^*_e, F^*_e) + f_{i_e}$
\end{theorem}
\ifthenelse{\boolean{short}}{}{
  \begin{proof}
    Recall that $f_{i_e}$ denotes the cost of the most expensive facility in an optimal solution. Furthermore, notice that for any facility $i \in F' \setminus \{i^*\}$, the clients in the ball $B(i, r_i) \subseteq C'$. However, if $i^* \in F'$, some of the clients in $B(i^*, r_{i^*})$ may have been removed in the outlier determination phase, and therefore it may not get paid completely by the dual variables $v_j$. Therefore,
    
    \begingroup
    \allowdisplaybreaks
    \begin{align*}
      \cost_e(C', F') &= \sum_{j \in C'} d(j, F) + \sum_{i \in F' \setminus \{i^*\} } f_i + f_{i^*}
      \\&\le 3 \cdot \sum_{j \in C'} v_j + f_{i^*} \tag{From lemma \ref{lem:outlier-lemma}}
      \\&= 3 \cdot \lr{\sum_{j \in C} v_j - \q\ell} + f_{i^*} \tag{For $j \in O'$, $v_j = \q$, and $|O'| = \ell$}
      \\&\le 3 \cdot \lr{\sum_{j \in C} v_j - \q\ell} + f_{i_e} \tag{Since $i_e$ is the most expensive facility}
    \end{align*}
    \endgroup
    Since $(v, w, \q)$ is a feasible dual solution, its cost is a lower bound on the cost of any integral optimal solution. Therefore, the theorem follows.
  \end{proof}
}

\noindent
Applying Corollary \ref{cor:modified-cost} with $\alpha = 1 + \epsilon, \beta = 3, \gamma = 1$ yields the following approximation guarantee.
\begin{theorem}
  The solution returned by Algorithm \ref{alg:outlierSeq} is a \(5 + \epsilon\) approximation to the Robust \textsc{FacLoc} problem.
\end{theorem}

\subsection{Facility Location with Penalties} \label{sec:seq-fl-wp}
For the penalty version, each client $j$ comes with a penalty $p_j$ which is the cost we pay if we make $j$ an outlier. Therefore, the radius computation for a facility changes because if a facility $i$ is asking client $j$ to contribute more than $p_j - c_{ij}$ then it is cheaper for $j$ to mark itself as an outlier and pay its penalty. Therefore, for each facility $i \in F$, let $r_i \ge 0$ be a value such that $f_i = \sum_{j \in C} \max\LR{\min \LR{r_i - c_{ij}, p_j - c_{ij}}, 0}$, if it exists. Notice that if for a facility $i \in F$, such an $r_i$ does not exist, then it must be the case that for all $j \in C$, $p_j \le c_{ij}$. That is, it is for any client, it is cheaper to pay the penalty than to connect it to this facility. Therefore, removing such a facility from consideration does not affect the cost of any solution, and hence we assume that for all $i \in F$, an $r_i \ge 0$ exists such that $f_i = \sum_{j \in C} \max\LR{\min \LR{r_i - c_{ij}, p_j - c_{ij}}, 0}$.
The algorithm for \textsc{FacLoc} with Penalties is shown in Algorithm \ref{alg:penaltiesSeq}.

\begin{algorithm2e}\caption{\textsc{PenaltyFacLoc}\((F, C, p)\)}\label{alg:penaltiesSeq}
  \tcc{Radius Computation Phase:}
  Compute $r_i$ for each $i \in F$ satisfying $f_i = \sum_{j \in C} \max\LR{\min\LR{r_i - c_{ij}, p_j - c_{ij}}, 0}.$ \\
  \tcc{Greedy Phase:}
  Sort and renumber facilities in the non-decreasing order of $r_i$. \\
  $C' \gets \emptyset$, \quad $F' \gets \emptyset$, \quad $O' \gets \emptyset$. \\
  \For{$i = 1, 2, \ldots$} {
    \If{there is no facility in $F'$ within distance $2r_i$ from $i$} {
      $F' \gets F' \cup \{i\}$
    }
  }
  \tcc{Outlier Determination Phase:}
  \For{each client $j$} {
    Let $i$ be the closest facility to $j$ in $F'$ \\
    \textbf{if} $c_{ij} \le p_j$ \textbf{then} $C' \gets C' \cup \{j\}$ \\
    \textbf{else} $O' \gets O' \cup \{j\}$
  }
  \Return $(C', F')$ as the solution.
\end{algorithm2e}

\ifthenelse{\boolean{short}}{}{
  We state the standard primal and dual linear programming relaxations for \textsc{FacLoc} with Penalties in Figure \ref{fig:penalty-primal-dual}. For $j \in C$ and $i \in F$, define $w_{ij} \coloneqq \max\LR{\min \LR{r_i - c_{ij}, p_j - c_{ij}}, 0}$ and for $j \in C$, let $v_j \coloneqq \min_{i \in F} c_{ij} + w_{ij}$. Note that $v_j = \min_{i \in F}\max\LR{c_{ij}, \min\LR{r_i, p_j}}$. If $\i$ is a facility realizing the minimum $v_j = c_{\i j} + w_{\i j} = \max\LR{c_{\ij}, \min\LR{r_\i, p_j}}$, then we say that $\i$ is the bottleneck of $j$.
  
  To make the analysis easier, we consider a more expensive solution $(\tilde{C}', F')$ where the set of clients $\tilde{C}'$ is constructed using the following modified outlier determination phase: for each client $j$, if $\max\LR{r_\i, c_{\ij}} \le p_j$ then $j \in \tilde{C}'$ and otherwise $j \in \tilde{O}'$ where $\i$ is the bottleneck of $j$.
  
  It is easy to see that for any client $j \in C$, the ``cost'' paid by the client (i.e., connection cost, or its penalty) in the solution $(C', F')$ is at most the cost paid by it in the solution $(\tilde{C}', F')$. So henceforth, we analyze the cost of the solution $(\tilde{C}', F')$ by comparing it to the cost of a feasible dual \LP solution and in order to alleviate excessive notation, we will henceforth refer to the solution $(\tilde{C}', F')$ as $(C', F')$ and $\tilde{O}'$ as $O'$.
  
  \begin{figure}
    \centering
    \makebox[\linewidth][c]{
      
      \begin{minipage}{0.51\linewidth}
        \begin{mdframed}[backgroundcolor=gray!9]
          \fontsize{9}{10}\selectfont
          \begin{alignat}{3}
            \text{minimize}   \displaystyle&\sum\limits_{i \in F} f_i y_i + \sum_{i \in F, j \in C} &c_{ij} x_{ij} + \sum_{j \in C} p_j z_j & \nonumber\\
            \text{subject to } \displaystyle& z_j + \sum\limits_{i \in F} x_{ij} \geq 1,   &\quad \forall j \in C \label{constr:penlpp-coverage} \\
            \displaystyle&\qquad\quad\ \   x_{ij} \le y_i, &\quad \forall  i \in F, \forall j \in C  \label{constr:penlpp-open}\\
            \displaystyle&\quad z_j, y_i, x_{ij} \in [0, 1] &\quad \forall i \in F,\forall j \in C \label{constr:penlpp-nonneg}
            \\\nonumber
          \end{alignat}
          \centering Primal LP
        \end{mdframed}
      \end{minipage}
      \makebox[0.51\linewidth][c]{
        \begin{minipage}{0.51\linewidth}
          \begin{mdframed}[backgroundcolor=gray!9]
            \fontsize{9}{9}\selectfont
            \begin{alignat}{3}
              \text{maximize}\displaystyle&\qquad \sum\limits_{j \in C} v_j
              \\\text{subject to } \displaystyle&\qquad\ \ v_j \le c_{ij} + w_{ij},   &\forall j \in C \label{constr:penlpd-alpha} \\
              \displaystyle&\ \sum_{j \in C} w_{ij} \le f_i &\forall i \in F \label{constr:penlpd-beta}\\
              \displaystyle&\ \  v_{j} \le p_j &\forall j \in C \label{constr:penlpd-penalty}\\
              \displaystyle& v_j, w_{ij} \ge 0 &\forall i \in F,\forall j \in C \label{constr:penlpd-integral}
            \end{alignat}
            \centering Dual LP
          \end{mdframed}
        \end{minipage}}
    }
    \caption{Primal and Dual Linear Programming Relaxations for \textsc{FacLoc} with Penalties \label{fig:penalty-primal-dual}}
  \end{figure}
  
  Because of the way we choose the outliers in the solution we consider for the analysis \((C', F')\) we have the following property (where $\i$ is the bottleneck of $j$) --
  $$v_j = \begin{cases}
    \max\LR{c_{\ij}, r_\i} &\text{if } j \in C' \\
    p_j  &\text{if } j \in C \setminus C'
  \end{cases}$$
  
  We simultaneously prove feasibility of the dual solution we constructed, and show how it can be used to pay for the integral solution. We consider different cases regarding a fixed client $j \in C$ with bottleneck facility $\i$. We first prove a few straightforward claims.
  
  \begin{claim} \label{cl:p-1}
    If $\i \in F'$ is the bottleneck for $j \in C$, then $w_{i'j} = 0$ for all $i' \in F'$, where $i' \neq \i$.
  \end{claim}
  \begin{proof}
    Suppose there exists a facility $i' \in F'$ with $w_{i'j} > 0$. That is, $\min\LR{r_{i'} - c_{i'j}, p_j - c_{i'j}} > 0$, which further implies that $c_{i'j} < \min\LR{p_j, r_{i'}} \le r_{i'}$.
    
    If $c_{\ij} > r_{\i}$, then $v'_j = c_{\ij} + \max\LR{0, \min\LR{r_{\i} - c_{\ij}, p_j - c_{\ij} }} = c_{\ij}$. However, since $w_{i'j} > 0$, $c_{\ij} = v'_j \le c_{i'j} + w_{i'j} = \min\LR{p_j, r_{i'}} \le r_{i'}$
    
    Otherwise, $c_{\ij} \le r_{\i}$.
    
    Therefore in either case, $c_{\i i'} \le c_{\i j} + c_{j i'} \le 2 \max\LR{r_{i'}, r_{\i}}$, which is a contradiction since at most one of $\i, i'$ can belong to $F'$.
  \end{proof}
  
  \begin{claim} \label{cl:p-2}
    If $\i \not\in F'$ is the bottleneck of $j \in C$ and $\max\LR{r_\i, c_{\ij}} \le p_j$, and $i' \in F'$ caused $\i$ to close, then $c_{i' j} \le 3 v_j$.
  \end{claim}
  \begin{proof}
    Note that since we assume $\max\LR{r_\i, c_{\ij}} \le p_j$, we have $j \in C'$, $v_j = v'_j$, $c_{\ij} \ge p_j$, and $r_\i \le p_j$.
    Since $i' \in F'$ caused $\i$ to close, $r_{i'} \le r_{\i}$. Furthermore, $c_{i' \i} \le 2 r_{\i}$. Therefore, $c_{i' j} \le c_{\i i'} + c_{\i j} \le 2 r_{\i} + c_{\ij}$.
    
    If $c_{\ij} \ge r_{\i}$, then $w_{\ij} = 0$, and $c_{\ij} = v_j$. This means $2r_{\i} + c_{\ij} \le 3c_{\ij} = 3v_{j}$
    Otherwise, $c_{\ij} < r_{\i}$. Here, $v_j = \min\LR{r_{\i}, p_j}$ Then $2r_{\i} + c_{\ij} < 3r_{\i} = 3 \min\LR{r_{\i}, p_j} \le 3v_{j}$
    
    In either case, $c_{i' j} \le 3v_{j}$.
  \end{proof}
  
  
  \begin{claim} \label{cl:p-3}
    If $\i$ is the bottleneck of $j$, with $\max\LR{r_\i, c_{\ij}} \le p_j$, then $v_j \ge r_\i$.
  \end{claim}
  \begin{proof}
    Again, since we assume $\max\LR{r_\i, c_{\ij}} \le p_j$, we have $j \in C'$, $v_j = v'_j$, $c_{\ij} \ge p_j$, and $r_\i \le p_j$.
    
    Recall that $v_j = v'_j = \max\LR{c_{\ij}, \min\LR{r_\i, p_j}} = \max\LR{c_{\ij}, r_{\i}} \ge r_{\i}$ and the claim follows.
  \end{proof}
  
  We are now ready to prove the feasibility and approximation guarantee
  
  \begin{lemma}
    The solution $(v, w)$ is a feasible solution to the dual LP relaxation \ref{fig:penalty-primal-dual}.
  \end{lemma}
  \begin{proof}
    First note that constraints \ref{constr:penlpd-beta}, \ref{constr:penlpd-penalty}, and \ref{constr:penlpd-integral} are satisfied by construction for all $i \in F$ and $j \in C$ and so is constraint \ref{constr:penlpd-alpha} for all $j \in C'$.
    
    All that is left to show is that constraint \ref{constr:penlpd-alpha} is satisfied for all $j \in O'$. Since $j \in O'$, $\max\LR{r_\i, c_{\ij}} > p_j$.
    
    We have, $v_j \coloneqq p_j < \max\LR{r_\i, c_{\ij}} =  \max\LR{c_{\ij}, \min\LR{r_{\i}, p_j}} = v'_j \le c_{ij} + w_{ij}$ for any $i \in F$.
  \end{proof}
  
  
  \begin{lemma}
    For any $j \in C'$, there is some $i \in F'$ such that $c_{ij} + w_{ij} \le 3 v_j$.
  \end{lemma}
  \begin{proof}
    In all the cases, we assume that $\max\LR{r_{\i}, c_{\ij}} \le p_j$ and therefore $j \in C'$. This also implies $v_j = v'_j = \max \LR{c_{\ij}, \min\LR{p_j, r_{\i}}} = \max \LR{c_{\ij}, r_{\i}}$. Therefore, we can just disregard the penalties in the analysis.
    \begin{itemize}
      
    \item \textbf{Case 1.} $\i \in F'$
      
      Connect $j$ to $\i$. From claim \ref{cl:p-1}, we know that $w_{i'j} = 0$ for all other $i' \in F'$.
      
      \begin{enumerate}
      \item If $c_{\ij} < r_{\i}$, then $v_j = c_{\ij} + w_{\ij}$. In this case, $v_j$ pays for connecting $j$ to $\i$ and also for $j$'s contribution to opening cost of $\i$ which is exactly $w_{\ij}$.
        
      \item Otherwise $c_{\ij} \ge r_{\i}$, then $w_{\ij} = 0$, which is $j$'s contribution towards $\i$. We have $v_j = c_{\ij}$ and therefore $v_j$ pays for connecting $j$ to $\i$.
      \end{enumerate}
      
    \item \textbf{Case 2.} $\i \notin F'$ and $w_{ij} = 0$ for all $i \in F'$.
      
      Let $i'$ be the facility that caused $\i$ to close. Connect $j$ to $i'$. From claim \ref{cl:p-2}, we have $c_{i' j} \le 3v_{j}$. Therefore, $3v_j$ pays for the connection to $i'$.
      
    \item \textbf{Case 3.} $\i \notin F'$, there is some $i' \in F'$ with $w_{i'j} > 0$, but $i'$ did not cause $\i$ to close.
      
      We connect $j$ to $i'$. By assumption $w_{i'j} = r_{i'} - c_{i'j} > 0$. Furthermore, let $i$ be the facility that caused $\i$ to close. By claim \ref{cl:p-2} we have $c_{ij} \le 3 v_j$.
      
      We have $c_{i'j} + w_{i'j} = r_{i'}$. Also, $c_{i i'} > 2r_{i'}$, since $i', i$ both were added to $F'$.
      
      Now, $2(c_{i' j} + w_{i'j}) = 2r_{i'} \le c_{i i'} \le c_{i' j} + c_{i j}$. Subtracting $c_{i'j}$ from both sides, we get $c_{i' j} + 2w_{i'j} \le c_{i j} \le 3v_j$. Therefore, $3v_j$ pays for the connection cost of $j $ to $i'$ and also for (twice) $j$'s contribution towards opening $i'$.
      
    \item \textbf{Case 4.} $\i \notin F'$ and $i' \in F'$ with $w_{i'j} > 0$ caused $\i$ to close.
      
      We connect $j$ to $i'$. From claim \ref{cl:p-3}, we have that $v_j \ge r_\i$.
      
      Since $i'$ caused $\i$ to close, $r_\i \ge r_{i'} \ge c_{i'j} + w_{i'j}$. Therefore, $c_{i'j} + w_{i'j} \le r_{i'} \le r_{\i} \le v_j$. That is, $v_j$ pays for the connection cost of $j$ to $i'$, as well as its contribution towards opening of $i'$.
    \end{itemize}
  \end{proof}
  
  Thus, $(v, w)$ is a feasible dual solution. 
  We use the above analysis to conclude with the following theorem.
}

A primal-dual analysis of Algorithm \ref{alg:penaltiesSeq} leads to the following upper bound.

\begin{theorem}
$\cost(C', F') \le 3 \cdot \cost(C^*, F^*)$.
\end{theorem}
\ifthenelse{\boolean{short}}{}{
  \begin{proof}
    We show $\cost(C', F') \le 3 \lr{\sum_{j \in C} v_j}$, which is sufficient since $(v, w)$ is a feasible dual solution, and cost of any feasible dual solution is a lower bound on the cost of an integral optimal solution.
    
    As we have argued previously, for any $j \in C \setminus C'$, we have $p_j = v_j$, and that for any $j \in C'$, we have $d(j, F') + s(j)$, where $s(j) \ge 0$ is the contribution of $j$ towards opening a single facility in $F'$. We have also argued that any $j \in C'$ contributes $s(j)$ for at most one open facility from $F'$. It follows that,
    
    \begingroup
    \allowdisplaybreaks
    \begin{align*}
      \cost(C', F') &= \sum_{i \in F'} f_i + \sum_{j \in C'} d(j, F') + \sum_{j \in C \setminus C'} p_j
      \\&= \lr{\sum_{j \in C'} s(j) + d(j, F')} + \sum_{j \in C \setminus C'} p_j
      \\&\le 3 \sum_{j \in C'} v_j + \sum_{j \in C \setminus C'} v_j
      \\&\le 3 \lr{\sum_{j \in C} v_j}
    \end{align*}
    \endgroup
  \end{proof}
}
