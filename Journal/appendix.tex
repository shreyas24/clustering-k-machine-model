\appendix
\section{Technical Proofs from Section \ref{subsec:primaldual}} \label{appendix:beta-mp-proofs}
In this appendix, we give some of the technical proofs required to prove Lemma \ref{lem:mp-beta}, and then give its proof.

Throughout this section, we condition on the event that the outcome of all the randomized algorithms is as expected (i.e. the ``bad'' events do not happen). Note that this happens with w.h.p. We first need the following facts along the lines of \cite{Thorup2001}. 

\begin{Lemma}[Modified From Lemma 8 Of \cite{Thorup2001}] \label{lem:lem8}
	There exists a total ordering $\prec$ on the vertices in $V$ such that $u \prec v \implies \rdown_u \le \rdown_v$, and $v$ is added to $S$ if and only if there is no previous $u \prec v$ in $S$ such that $d(u, v) \le 2(1+\epsilon) \rdown_v$.
\end{Lemma}

% \begin{Claim}[Modified From Claim 9.1 Of \cite{Thorup2001}] \label{cl:cl9.1}
% 	For any $v \in V$, there is a vertex with $u \preceq v$ and $d(u, v) \le 2\rdown_v$.
% \end{Claim}

\begin{Claim}[Modified From Claim 9.2 Of \cite{Thorup2001}] \label{cl:cl9.2}
	For any two distinct vertices $u, v \in S$, we have that $d(u, v) \ge 2(1+\epsilon)^3 \cdot \max\{\rdown_u, \rdown_v\}$.
\end{Claim}

In the rest of the section, we follow the primal-dual analysis of \cite{ArcherRSESA2003}, again with necessary modifications arising from various approximations. For completeness, we state the primal and dual LP relaxations below. We reserve the subscript \(i\) for facilities and \(j\) for clients. Note that in our case, \(i, j \in V\).
\begin{align*}
  \text{min. } \sum_{i} f_i y_i + &\sum_{i, j} d(i, j) \cdot x_{ij} & \text{max. } \sum_{j} v_j& \\
  \text{s.t.} \quad \sum_{i} x_{ij} &= 1 \quad \forall j & \text{s.t.} \quad \sum_{j} w_{ij} - f_i &\le 0 \quad \forall i \\
  y_{i} - x_{ij} &\ge 0 \quad \forall i, j & v_j - w_{ij} - d(i ,j) &\le 0 \quad \forall i, j \\
  y_i, x_{ij} & \ge 0 \quad \forall i, j & v_j, w_{ij} & \ge 0 \quad \forall i, j
\end{align*}


Let $\beta \ge 1$ be the parameter that is used in the Facility Location algorithm. Set $w_{ij} = \frac{1}{\beta} \max\{0, \rsim_i - \dup(i, j)\}$. Say that $j$ contributes to $i$ if $w_{ij} > 0$. Then, set $v_j = \min_{i \in V} d(i, j) + w_{ij}$. It is easy to see that the $v$ and $w$ values are dual feasible.

Define for each $j \in V$, $s_j = w_{ij}$ if there exists an $i \in S$ with $w_{ij} > 0$ and $0$ otherwise. Note that $s_j$ is uniquely defined, if it is not zero. This is because of the fact that the balls $B(v, \rdown_v)$ and $B(u, \rdown_u)$ are disjoint using Claim \ref{cl:cl9.2}. Also note that $f_i = \sum_{j \in V} w_{ij}$, therefore, $\sum_{i \in S} f_i = \sum_{j \in V} s_j$.

For $j \in V$, call the facility $i \in P$ that determines the minimum in $v_j = \min_{i \in V} d(i, j) + w_{ij}$, the \emph{bottleneck} of $j$. We say that a facility (or a vertex) is \emph{closed} if it does not belong to the set $S$, and it is opened otherwise. Furthermore, we say that a facility $v \in S$ \emph{caused} another facility $u \notin S$ was closed, if at the time $u$ was removed in the Algorithm \ref{alg:MettuPlaxtonPhase2}, Line \ref{alg4:remove-vertices}, $d(u, v) \le 2(1+\epsilon)^i$. Before showing the approximation guarantee, we need the following four lemmas the first three of which we state without proof. (cf. Lemmas 1-4 from \cite{ArcherRSESA2003})

\begin{Lemma} \label{lem:duallemma1}
	For any $i, j \in V$, we have that 
	$\tilde{r}_i \le (1+\epsilon)^3 \cdot (\beta w_{ij} + d(i, j))$. Furthermore, if for some $i, j \in V, w_{ij} > 0$, then $\tilde{r}_i \ge \frac{1}{(1+\epsilon)^3} (\beta w_{ij} + d(i, j))$.
\end{Lemma}
% \begin{proof}
% 	We have that $\beta w_{ij} \ge \rsim_i - \dup(i, j)$. Now using the appropriate upper and lower bounds (cf. inequalities \ref{ineq:dist-up} and \ref{ineq:r-down}) for $\rsim_i$, and $\dup(i, j)$ in terms of $r_i$, and $d(i, j)$ yield the desired inequality.
% \end{proof}

\begin{Lemma} \label{lem:duallemma2}
	If $\beta \le 3$, and if $i$ is a bottleneck for $j$, then $3(1+\epsilon)^3 v_j \ge 2\tilde{r}_i$.
\end{Lemma}
% \begin{proof}
% 	\begin{align*}
% 	v_j &= d(i, j) + w_{ij} \tag{Since $i$ is the bottleneck for $j$}
% 	\\\implies 3(1+\epsilon)^4 v_j &\ge (1+\epsilon)^4 (2 \cdot d(i, j) + 2 \cdot \beta) \tag{Using the fact that $\beta \le \frac{3(1+\epsilon)^4}{2}$}
% 	\\&\ge 2 \cdot r_i \tag{Using Lemma \ref{lem:duallemma1}.}
% 	\end{align*}
% \end{proof}

\begin{Lemma} \label{lem:duallemma3}
	If an open facility is a bottleneck for $j$, then $j$ cannot contribute to any other open facility.
\end{Lemma}
% \begin{proof}
% 	Suppose $i' \in S$ be $j$'s bottleneck. Also assume that $j$ contributes to another $i \in S$, i.e. $w_{ij} > 0$. Using triangle inequality, we have that $d(i, i') \le d(i, j) + d(i', j) \le \dup(i, j) + d(i', j) < \rsim_i + d(i', j)$. In the last inequality, we use the fact that $w_{ij} > 0$, which means that $\dup(i, j) < \rsim_i$. Now there are two cases, depending on whether $w_{i'j} > 0$ or $w_{i'j} = 0$.  
	
% 	In the first case, if $w_{i'j} > 0$, then again using similar reasoning, we have that $\dup(i', j) \le \rsim_{i'}$. However, this implies that $d(i, i') < \rsim_i + \rsim_{i'} \le 2(1+\epsilon)^2 (\rdown_i + \rdown_{i'}) \le 2(1+\epsilon^2) \max\{\rdown_i, \rdown_{i'}\}$, which is a contradiction to Claim \ref{cl:cl9.2}.
	
% 	In the second case, $w_{i'j} = 0$. However, since $i'$ is also a bottleneck for $j$, this implies that $v_j = \min_{i \in V} d(i, j) = d(i', j)$. That is, $i'$ is the closest vertex to $j$, i.e. $d(i, j) \le d(i', i)$. However, this implies $d(i, i') \le 2d(i', j) \le 2\dup(i', j) < 2\rsim_{i'} \le 2(1+\epsilon)^2 \rdown_{i'}$, which is again a contradiction to Claim \ref{cl:cl9.2}.
% \end{proof}

\begin{Lemma} \label{lem:duallemma4}
	If a closed facility $i \notin S$ is a bottleneck for $j \in V$, and $k \in S$ is the open facility that caused $i$ to close, then 
	$\max \{2\beta, 3\} \cdot (1+\epsilon)^7 \cdot v_j \ge d(k, j)$.
\end{Lemma}
% \begin{proof}
% 	\begin{align*}
% 	d(k, j) &\le d(k, i) + d(i, j) \tag{Triangle inequality}
% 	\\&\le 2(1+\epsilon)^3 \rdown_i + d(i, j) \tag{$k$ caused $i$ to close, so using Lemma \ref{lem:lem8}.}
% 	\\&\le 2(1+\epsilon)^3 r_i + d(i, j)
% 	\\&\le 2(1+\epsilon)^3 \cdot (1+\epsilon)^4 (\beta w_{ij} + d(i, j)) + d(i, j) \tag{Using Lemma \ref{lem:duallemma1}.}
% 	\\&\le 2\beta (1+\epsilon)^7 w_{ij} + (2(1+\epsilon)^7 + 1) \cdot d(i, j)
% 	\\&\le \max \{2\beta (1+\epsilon)^7, 2(1+\epsilon)^7 + 1 \} \cdot v_j \tag{Since $i$ is the bottleneck for $j$}
% 	\\&\le \max \{2\beta, 3\} \cdot (1+\epsilon)^7 \cdot v_j
% 	\end{align*}
% \end{proof}

\begin{proof}
	\begin{align*}
		d(k, j) &\le d(k, i) + d(i, j) \tag{Triangle inequality}
		\\&\le 2(1+\epsilon) r_i + d(i, j) \tag{Using Lemma \ref{lem:lem8}}.
		\\&\le 2(1+\epsilon)^4 \tilde{r}_i + d(i, j) \tag{Using Lemma \ref{lem:facloc-guarantee}}
		\\&\le 2(1+\epsilon)^7 (\beta w_{ij} + d(i,j)) + d(i, j) \tag{Using Lemma \ref{lem:duallemma1}}
		% \\&\le 2\beta (1+\epsilon)^7 w_{ij} + (2(1+\epsilon)^7 + 1) d(i, j)
		% \\&\le \max\{2\beta, 3\} \cdot (1+\epsilon)^7 \cdot (w_{ij} + d(i, j) )
		\\&= \max\{2\beta, 3\} \cdot (1+\epsilon)^7 \cdot v_j \tag{Because $i$ is the bottleneck for $j$}
	\end{align*}
\end{proof}

We are finally ready to prove the main guarantee of the modified MP-$\beta$ algorithm, as in \cite{ArcherRSESA2003}. The basic idea is to show that $(3+O(\epsilon))$ times the dual variable $v_j$ pays for the distance traveled by $j$, as well as, $\beta s_j$, which is a part of the facility opening cost. We formalize this in the following lemma.

\begin{Lemma} \label{lem:mp-beta}
	For any vertex $j \in V$, there exists a facility $c(j) \in S$ such that $3 (1+O(\epsilon)) v_j \ge d(j, c(j)) + \beta s_j$.
\end{Lemma}
\begin{proof}
	Consider a vertex $j \in V$. We prove the theorem by doing a careful case analysis. 
	
	\textbf{Case 1.} Some open facility $i \in S$ is the bottleneck for $j$. Connect $j$ to $i$.
	\\If $d(i, j) \le \rsim_i$, we have that $0 < w_{ij} = s_j$. Also, $v_j = d(i, j) + s_j$.
	\\Otherwise, $w_{ij} = 0$, and $v_j = d(i, j)$.
	
	\textbf{Case 2.} Some closed facility $i \notin S$ is the bottleneck for $j$, and $j$ does not contribute to any open facility (i.e. $s_j = 0$).
	\\There must be some open facility $k \in S$ that caused $i$ to close. Connect $j$ to $k$. By Lemma \ref{lem:duallemma4}, we know that $3(1+\epsilon)^7 v_j \ge d(k, j)$.
	
	\textbf{Case 3.} Some closed facility $i \notin S$ is the bottleneck for $j$, and there exists an open facility $\ell \in S$ with $w_{\ell j} > 0$, but $\ell$ was not the reason why $i$ was closed.
	\\Since $w_{\ell j} > 0$, $s_j = w_{\ell j}$, by the uniqueness of $s_j$. Connect $j$ to $\ell$.
	\\By Lemma \ref{lem:duallemma1}, we have that $\tilde{r}_{\ell} \ge \frac{1}{(1+\epsilon)^3} (d(\ell, j) + w_{\ell j})$. Also, there must be some open facility $k \in S$ which prevented $i$ from opening. Using similar reasoning as in the previous case, we have that $d(k, j) \le 3(1+\epsilon)^7 v_j$. Now, 
	\begin{align*}
	&d(\ell, k) \ge 2(1+\epsilon)^3 r_{\ell} \ge 2 (d(\ell, j) + \beta w_{\ell j}) \tag{Using Claim \ref{cl:cl9.2} and Lemma \ref{lem:duallemma1}.}
	\\&\implies 2 (d(\ell, j) + \beta w_{\ell j}) \le d(\ell, k) \le d(\ell, j) + d(k, j) \tag{Triangle inequality}
	\\&\implies  d(\ell, j) + 2 \beta w_{\ell j} \le d(k, j) \le 3(1+\epsilon)^7 v_j
	\end{align*}
	
	\textbf{Case 4.} Some closed facility $i \notin S$ is the bottleneck for $j$. Furthermore, there is an open facility $k \in S$ such that $w_{kj} > 0$, and $k$ caused $i$ to be closed. Connect $j$ to $k$.
	\\Again, by uniqueness of $s_j$, we have that $s_j = w_{kj}$. Also, from Lemma \ref{lem:duallemma2}, $3(1+\epsilon)^3 v_j \ge 2\tilde{r}_i$. Since $k$ caused $i$ to be closed, we have that $\tilde{r}_i \ge \tilde{r}_k \ge \frac{1}{(1+\epsilon)^3} (d(k, j) + \beta w_{kj}) = \frac{1}{(1+\epsilon)^3} (d(k, j) + \beta s_j)$, by Lemma \ref{lem:duallemma2}. Combining the previous inequalities yields, $3(1+\epsilon)^6 v_j \ge d(k, j) + 2\beta s_j$.
	
	Finally, we use the well-known fact that for any $\epsilon \in (0, 1)$, $(1+\epsilon)^7 \le (1+c\epsilon)$ for some constant $c$, and the lemma follows.
\end{proof} 