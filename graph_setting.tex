\section{Clustering in the Graph Metric Setting}
\label{section:ClusteringGraphMetric}

In this section we present constant-factor approximation algorithms in the $k$-machine model for the facility location problem
(\facloc), the $p$-median problem ($p$\textsc{Median}), and the $p$-center ($p$\textsc{Center}).
All of these algorithms run in $\to(n/k)$ rounds w.h.p.~and due to the lower bound results of Section \ref{section:lowerBounds},
these are all optimal, to within polylogarithmic factors.

\subsection{Preliminaries}
\label{section:preliminaries}

Since the input metric is only implicitly provided, as an edge-weighted graph, computng shortest path distances to learn
parts of the metric space turns out to be a key elemnt of our algorithms.
The \textit{Single Source Shortest Path (SSSP)} problem has been considered in the $k$-machine model in Klauck et al.~\cite{KlauckNPRSODA15}.
They consider two versions of the problem; the \textit{distance} version in which the output consists only of shortest path distances
to the source and the \textit{tree} version in which the output also includes a shortest path tree.
(Here we only need the distance version of the problem.)
For both versions of the problem Klauck et al.~ describe $(1+\epsilon)$-approximation algorithms that run in the $k$-machine model in 
$\to(n/\sqrt{k})$ rounds.
This is too slow for our purpose, since we are looking for an overall running time of $\to(n/k)$.
We instead turn to a recent result of Becker at al.~\cite{BeckerKKLarxiv16} and using this we can easily obtain an $\to(n/k)$-round
algorithm.
While Becker et al.~do not work in the $k$-machine model, they present the following result that is useful to us.
\begin{theorem} \textbf{(Becker et al.~\cite{BeckerKKLarxiv16})}
For any $0 < \epsilon \le  1$, in the Broadcast Congested Clique model, a deterministic $(1 + \epsilon)$-approximation to 
SSSP problem (distance version) in undirected graphs with non-negative edge-weights can be computed in 
$\epsilon^{-9} \cdot \mbox{poly}(\log n)$ rounds.
\end{theorem}
It is easy to see that any Congested Clique algorithm that runs in $T$ rounds can be simulated in the $k$-machine model in
$T \cdot \to(n/k)$ rounds. This is because each of the $T$ Congested Clique rounds can be simulated in $\to(n/k)$ rounds 
in the $k$-machine model.
In this simulation, each machine will compute and communicate on behalf of each of the vertices it hosts, sequentially.
However, all $k$ machines will do this in parallel. Thus, a Congested Clique round can be simulated in the $k$-machine
model is as many rounds as the maximum number of vertices that a machine hosts. Since this is $\to(n/k)$ w.h.p.~the claim 
follows.
A more general version of this claim is proved in Klauck et al.~in the Conversion Theorem (Theorem 4.1 \cite{KlauckNPRSODA15}).
So we get the following result about the SSSP problem in the $k$-machine model.
\begin{theorem} 
\label{theorem:SSSP}
For any $0 < \epsilon \le  1$, there is a deterministic $(1 + \epsilon)$-approximation algorithm in the $k$-machine model for solving 
the SSSP problem (distance version) in undirected graphs with non-negative edge-weights in
$\epsilon^{-9} \cdot \mbox{poly}(\log n)$ rounds.
\end{theorem} 

%We denote the approximate distances returned by the SSSP algorithm with the notation \(\widetilde{dist}\). When we need the size to query \(| \Gamma_{\le d}(v) \cap W |\) for some vertex \(v\) and \(W \subseteq V\), we will get a size estimate with respect to \(\widetilde{dist}\) and not the actual distances.

In addition to SSSP, our clustering algorithms require an efficient solution to a more general problem that we
call \textit{Multi-Source Shortest Paths} (in short, MSSP).
The input to MSSP is an edge-weighted graph $G = (V, E)$, with non-negative edge-weights, and a set $T \subseteq V$ of sources.
The output is required to be a distance $d(v, T) = \min\{d(v, u) \mid u \in T\}$ for each vertex $v$.
In the following we show that MSSP problem can also be solved efficiently in the $k$-machine model.
\begin{Observation} \label{obs:SetSSSP}
	Given a set \(T \subseteq V\) which is known to the machines (i.e., each machine knows the vertices that it hosts that belong to \(T\)),
	There is a deterministic $(1+\epsilon)$-approximation algorithm for the MSSP problem in the $k$-machine model
	running in $\to(n/k)$ rounds.
\end{Observation}
\begin{proof}
	The idea is to introduce a dummy vertex \(s\), with zero-weight edges to each vertex \(u \in T\).
	We pick a machine (arbitrarily) as the host of $s$ and this machine broadcasts a label for $s$.
	Then each machine broadcasts the label of each vertex in $T$ that it hosts. This takes $\to(n/k)$ rounds.
	Now the modfied graph, with the new vertex $s$ and its incident edges, are known to the machines in the
	format required by the $k$-machine model.
	Now, we start an approximate SSSP computation with source \(s\) and the distance value that each vertex
	$v$ acquires will be \(d(v, T)\).
\end{proof}

\subsection{Facility Location in \(\tilde{O}(n/k)\) rounds}

At the heart of our $k$-machine algorithm for \facloc\ is the well-known sequential algorithm of Mettu and Plaxton \cite{MettuPlaxtonSICOMP2003}, that computes a 
3-approximation for \facloc.
To describe the Mettu-Plaxton algorithm (MP algorithm), we need some notation. For each real $r \ge 0$ and vertex $v$, define the ``ball'' $B(v, r)$ as
the set $\{u \in V \mid d(v, u) \le r\}$.
For each vertex $v \in V$, we define a \textit{radius} $r_v$ as the value
$r$ satisfying $f_v = \sum_{u \in B(v, r)}\max(0, r - d(v, u))$.
The MP algorithm is the following simple, 2-phase, greedy algorithm:
\begin{description}
	\item[Phase 1.] For each vertex $v \in V$, compute $r_v$.
	\item[Phase 2.] Consider vertices $v \in V$ in non-decreasing order of radii $r_v$.
		Starting with \(S = \emptyset\), add $v$ to $S$ if $d(v, S) > 4r_v$.
\end{description}

%\begin{theorem} \textbf{[Mettu, Plaxton \cite{MettuPlaxtonSICOMP2003}]} $cost(S) \le 3 \cdot OPT$. \end{theorem}

We will work with a slight variant of the MP algorithm, called MP-$\beta$ in \cite{ArcherRSESA2003}.
The only difference between the MP algorithm and the MP-$\beta$ algorithm is in the definition of each radius $r_v$, which is 
defined for the MP-$\beta$ algorithm, as the value $r$ satisfying $\beta \cdot f_v = \sum_{u \in B(v, r)}\max(0, r - d(v, u))$.
(Thus, the MP-$\beta$ algorithm with $\beta = 1$ is just the MP algorithm.)

There are two challenges with implementing the MP-$\beta$ algorithm efficiently in the $k$-machine model (and more generally in a distributed or 
parallel setting): (i) The calculation of the radius $r_v$ by the machine hosting vertex $v$ requires that the machine know distances
$d(v, u)$ for all $u \in V$; however the distance metric is initially unknown and is too costly to fully calculate, and (ii) the algorithm
seems inherently sequential because it considers vertices one-by-one in increasing order of radii.
In the next two sections, we describe how to overcome these challenges.

%As in \cite{Thorup2001}, we will implement an approximate version of the facility location algorithm by Mettu and Plaxton. The set \(S\) returned by the algorithm is a \(3\)-approximate solution to the facility location problem. The algorithm runs in two phases --

\subsubsection{Reducing Radii Computation to Neighborhood Size Computation}
To deal with the challenge of cumputing radii efficiently, without full knowledge of the metric, we use Thorup's approach \cite{Thorup2001}. 
Thorup works in the sequential setting, but like us, he assumes that the distance metric is implicitly specified via an edge-weighted graph.
He shows that it is possible to implement the MP algorthm in $\to(m)$\footnote{We use $\to(f(n))$ as a shorthand for $O(f(n) \cdot \mbox{poly}(\log n))$.} time on an $m$-edge graph.
In other words, it is not necessary to compute the distance metric (e.g., by solving the All Pairs Shortest Path problem).
We now show how to translate Thorup's ideas into the $k$-machine model.
(We note here that Thorup's ideas for the \facloc\ problem have been used to design algorithms in ``Pregel-like'' distributed systems \cite{GarimellaDGSCIKM2015}.)

For some $\epsilon > 0$, we start by discretizing the range of possible radii values using powers of $(1 + \epsilon)$. 
For any vertex $v$ and for any integer $i \ge 0$, let $q_i(v)$ denote $|B(v, (1+\epsilon)^i)|$, the size of the neighborhood of 
$v$ within distance $(1+\epsilon)^i$.
Further, let $\alpha(v, r)$ denote the sum $\sum_{u \in B(v, r)}\max(0, r - d(v, u))$.
Now note that if $r$ increases from $(1+\epsilon)^i$ to $(1+\epsilon)^{i+1}$, then $\alpha(v, r)$ increases by at least 
$q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i)$. 
This implies that $\sum_{i = 0}^t q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i)$ is a lower bound on $\alpha(v, (1+\epsilon)^t)$.
This observation suggests that we might be able to use, as an approximation of $r_v$, the smallest value $(1+\epsilon)^t$
for which this lower bound on $\alpha(v, (1+\epsilon)^t)$ is at least as large as $f_v$.
Denote by $\tilde{r_v}$, this approximation of $r_v$ and note that since $\alpha(v, \tilde{r_v}) \ge f_v$, we know that
$\tilde{r_v} \ge r_v$.
While this lower bound on $\tilde{r_v}$ is immediate, it is also easy to show that $\tilde{r_v} < r_v (1 + \epsilon)^2$ (\textbf{Need to show this --Sriram}),
implying that $\tilde{r_v}$ is a good approximation to $r_v$.
Recall from the definition of $\tilde{r_v}$ that, in order to compute these values, we only require knowledge of $q_i(v)$ for all $i \ge 0$,
rather than actual distances $d(v, u)$ for all $u \in V$.
We can now state the high-level $k$-machine model algorithm for computing $\tilde{r_v}$ values.
For a machine $m$, let $H(m)$ denote the vertices that $m$ is a host for.
\begin{description}
	\item[Phase 1:] Each machine computes $q_i(v)$, for all integer $i \ge 0$ and for all vertices $v$ it hosts.
	\item[Phase 2:] Each machine computes $\tilde{r_v}$ locally, for all vertices $v$ it hosts. (Recall that $\tilde{r_v} := (1+\epsilon)^t$
		where $t \ge 0$ is the smallest integer for which $\sum_{i = 0}^t q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) \ge f_v$.)
\end{description}
Phase 2 is just local computation and we focus on Phase 1.
Phase 1 is the problem of computing neighborhood sizes and this is where Cohen's neighborhood size estimation framework \cite{Cohen1997,Cohen2015} 
comes into the picture.

\subsubsection{Cohen's Size-Estimation Framework in the $k$-machine Model}

Motivated by counting problems such as computing the sizes of sets of vertices reachable (in a directed graph) from each vertex,
Cohen \cite{Cohen1997} presents a ``size-estimation framework,'' a general randomized method (in the sequential setting) for solving this and other related 
counting problems.
The application of Cohen's method that is of particular interest to us is the following: given a directed edge-weighted graph,
with non-negative edge weights, compute the size of $B(v, d)$ for each vertex $v$ and positive real $d$.
Cohen's method yields a Monte Carlo algorithm that runs in $O(m \log n + n \log^2 n)$ time on $n$-vertex, $m$-edge graphs,
producing a compact data structure that can, for any vertex $v$ and real $d$, return an estimate of $|B(v, d)|$, with very small relative  
error.
We now show how to efficiently implement Cohen's size estimation framework in the $k$-machine model.

Cohen's simple and elegant idea starts by assigning to each vertex $v$ a rank \(r(v)\) chosen uniformly from \([0, 1]\).
These ranks induce a random permutation of the vertices. 
To compute the size estimate of a neighborhood, say $B(v, d)$, for a vertex $v$ and real $d > 0$, Cohen's algorithm finds the
smallest rank of a vertex in $B(v, d)$.
It is then shown (in Section 6, \cite{Cohen1997}) that the expected value of the smallest rank in $B(v, d)$ is $1/(1 + |B(v, d)|)$.
Thus, in expectation, the reciprocal of the smallest rank in $B(v, d)$ is (almost) identical to $|B(v, d)|$.
To obtain a good estimate of $|B(v, d)|$ with high probability, Cohen simply repeats the above-described process
independently $\ell$ times and shows the following concentration result.

\begin{theorem} \textbf{(Cohen \cite{Cohen1997})} \label{thm:cohen}
	Let $v$ be a vertex and $d > 0$ a real.
	For $1 \le i \le \ell$, let $R_i$ denote the smallest rank of a vertex in $B(v, d)$ obtained in the $i$-th repitition of
	Cohen's size-estimation procedure. 
	% Let $\tilde{R}$ be the $\lfloor \ell (1 - 1/e) \rfloor$-smallest value in $R_1, R_2, \ldots, R_\ell$.
	Let $\hat{R}$ be the average of $R_1, R_2, \ldots, R_\ell$.
	Let $\mu = 1/(1 + |B(v, d)|)$. Then, for any $0 < \epsilon < 1$,
	$$\Pr(|\hat{R} - \mu| \ge \epsilon \mu) = \exp(-\Omega(\epsilon^2 \cdot \ell)).$$
\end{theorem}
This theorem implies that $\ell = O(\log n/\epsilon^2)$ repititions suffice for $(1 \pm \epsilon)$-factor estimates of the sizes
of $B(v, d)$ for all $v$ and all $d$.

Cohen proposes a modified Dijkstra's algorithm to find smallest rank vertices in each neighborhood.
Let $v_1, v_2, \ldots, v_n$ be the vertices of the graph in non-decreasing order of rank. 
Initiate Dijkstra'a algorithm, first with source $v_1$, then with source $v_2$, and so on.
During the search with source $v_i$, if it is detected that for a vertex $u$, $d(u, v_j) \le d(u, v_i)$ for some $j < i$,
then the current search can be ``pruned'' at $u$.
This is because the vertex $v_j$ has ruled out $v_i$ from being the lowest ranked vertex in any of $u$'s neighborhoods.
In fact, this is true not just for $u$, but for all vertices whose shortest paths to $v_i$ pass through $u$.
Even though this algorithm performs $n$ SSSP computations, that fact that each search is pruned by the results of
previous searches makes the overall running time much less than $n$ times the worst case running time of
an SSSP computation.
In particular, by making critical use of fact that the random vertex ranks induce a random permutation of the vertices,
Cohen is able to show than the algorithm runs in $O(m \log n + n \log^2 n)$ time, on $n$-vertex, $m$-edge graphs, with
high probability.

We don't know how to implement Cohen's algorithm, as is, efficiently in the $k$-machine model.
In particular, it is not clear how to take advantage of pruning that occurs in later searches while simultaneously taking advantage of the 
parallelism provided by the $k$ machines.
A naive implementation in the $k$-machine model could end up being equivalent to $n$ different SSSP computations, which is too expensive. 
Below we show that we can reduce Cohen's algorithm to a polylogarithmic number of SSSP computations provided
we are willing relax the requirement that we find the smallest rank in each neighborhood. 
Of course, we also show that this relaxation does not do too much damage to the computed estimates.
Before presenting our algorithm, we introduce some notation. The algorithm takes as input an error parameter \(0 < \epsilon < 1\) which can be controlled externally.
We fix an $\epsilon'$, such that $\epsilon' > 0$ and $1 + \epsilon' \le \sqrt{1+\epsilon}$ and let $t = \lceil 2 \log_{1+\epsilon'} n \rceil$.
Note that this means $(1+\epsilon')^{t-1}/n^2 < 1$ and $(1+\epsilon')^t/n^2 \ge 1$. Algorithm \ref{alg:CohenEstimates} describes our implementation of the neighborhood estimation framework in detail.

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{NeighborhoodSizeEstimates}\((G, W=(W_1, \dots, W_k), \epsilon)\)\label{alg:CohenEstimates}}
  \For{\(j = 1, \ldots, \ell\)} {
    Each machine picks a rank $r(v)$, chosen u.a.r. from $[0, 1]$ for each vertex $v \in W$ that it hosts \label{alg2:ChooseRanks}\\
    Each machine rounds $r(v)$ down to closest $(1+\epsilon')^i/n^2$ \label{alg2:RoundRanks} \\
    \For{$i = 0, 1, \ldots, t$}{
      Let \(T_i\) be the set of vertices with rank $(1+\epsilon')^i/n^2$ \\
      Machines solve \((1+\epsilon)\)-approximate MSSP using \(T_i\) as the set of sources \label{alg2:MSSP} \\
      For each vertex \(v \in W\) store the value \(d(v, T_i)\) \\
      }
  }
\end{algorithm2e}

% \begin{enumerate}
% \item Each machine picks a rank $r(v)$, chosen uniformly at random from $[0, 1]$ for each vertex $v$ it hosts.
% \item Each machine, for each vertex $v$ it hosts, rounds rank $r(v)$ down to $(1+\epsilon)^i/n^2$,
% where  $(i+\epsilon)^i/n^2 < r(v) \le (1+\epsilon)^{i+1}/n^2$.
% it falls in. We denote the rounded rank by $\widetilde{r(v)}$.
% \item For $0 \le i \le t$, let $T_i$ denote the set of vertices with rounded rank $(1 + \epsilon)^i/n^2$.
% For each $i = 0, 1, \ldots, t$ (in this order), solve (approximate) MSSP using \(T_i\) as the set of sources.
% \end{enumerate}

Steps \ref{alg2:ChooseRanks} and \ref{alg2:RoundRanks} in this algorithm are local computations.
From Theorem \ref{thm:cohen} and Observation \ref{obs:SetSSSP}, we know that Step \ref{alg2:MSSP}
runs in the $k$-machine model in $\to(n/k)$ rounds.
	
At the end of each iteration, we have computed for each vertex $v$, the sequence of distances, $\{d(v, T_i)\}_{i=0}^t$.
Note that each distance $d(v, T_i)$ is associated with the rounded rank $(1 + \epsilon')^i/n^2$.
To find the vertex with approximate smallest rank in the neighborhood $B(v, d)$, we find the smallest $i$,
such that $d(v, T_i) \le d$, and return the rounded rank $(1+\epsilon')^i/n^2$.
To get an estimate that has low relative error, we repeat this $O(\log n/\epsilon'^2)$ times and compute the average \(\tilde{R}\) of the
ranks computed in each iteration. The estimator is obtained by subtracting \(1\) from the reciprocal of \(\tilde{R}\). The following
lemma shows that \(|B(v, d)|\) is ``sandwiched'' between two estimators.

\begin{Lemma}\label{lem:cohen}
Let $s$ denote $|B(v, d)|$ for some vertex $v$ and real $d > 0$, 
For any $\epsilon$, w.h.p., algorithm \ref{alg:CohenEstimates} satisfies the following properties:
	\begin{itemize}
	\item for the query $(v, d/(1+\epsilon))$ the algorithm returns an answer that is at most $s(1+\epsilon)$.
	\item for the query $(v, d(1+\epsilon))$ the algorithm returns an answer that is at least $s/(1+\epsilon)$.
	\end{itemize}
\end{Lemma}
%Let \(\epsilon, \epsilon_C > 0\). For any particular \(W \subseteq V\), after preprocessing in \(\tilde{O}(n/k)\) rounds, whp, for any vertex \(v\) and distance \(d\) we can estimate the size of \(\Gamma_{\le d}(v) \cap W\) within a factor of \((1 \pm (\epsilon_C + \epsilon + \epsilon\epsilon_C))\).
%\end{Lemma}
\begin{proof}
  Fix a particular iteration in the algorithm and a ranking of the vertices. It is easy to see that for any query \((v, d)\), the rank computed by our algorithm (if we had access to exact MSSP), is the smallest rank in \(B(v, d)\) rounded down to closest \((1+\epsilon')^i/n^2\). For the query \((v, d)\), let \(r(v, d)\) be the smallest rank in \(B(v, d)\). For the qureies \((v, d/(1+\epsilon))\) and \((v, d(1+\epsilon))\) let the rank computed by our algorithm denoted by \(\tilde{r}(v, d/(1+\epsilon))\) and \(\tilde{r}(v, d(1+\epsilon))\) respectively. We will first show the following --
  \[\tilde{r}(v, d/(1+\epsilon)) \ge r(v, d) / (1 + \epsilon')\]

  Consider the set $B(v, d/(1+\epsilon))$. Since we are using \((1 + \epsilon)\)-approximate MSSP computation, we will only discover a subset of vertices in $B(v, d/(1+\epsilon)) \subseteq B(v, d)$. Therefore, the rank discovered by our algorithm cannot be smaller than \(r(v, d)\) (rounded down) and since the ranks are rounded down to the nearest power of \((1 + \epsilon')\), the inequality follows. Now, we show the following--
  \[\tilde{r}(v, d(1+\epsilon)) \le r(v, d) \cdot (1 + \epsilon')\]

  Since we use \((1 + \epsilon)\)-approximate MSSP computation, we will cover all vertices in \(B(v, d)\) for the query \((v, d(1+\epsilon))\) (and possibly more). This means that the rank we compute will be at most \(r(v, d)\) (rounded down) and hence at most \(r(v, d) \cdot (1 + \epsilon')\).

  By Theorem \ref{thm:cohen} (setting \(\epsilon\) in the theorem to be \(\epsilon'\)), the average of the exact ranks is \((1 \pm \epsilon') (1/(|B(v, d)| + 1))\). Since \((1 + \epsilon') = \sqrt{1 + \epsilon}\), the lemma follows.
\end{proof}

We can set \(\epsilon\) as an arbitrarily small constant to get approximate size estimates with respect to approximate distances in \(\to(n/k)\) rounds. This is because the number of rank ranges will be \(O(\log n)\) which means that the machines will perform no more than \(\Theta(\log^2(n))\) number of SSSP computations.

\subsubsection{Maximal dist-\(d\) Independent Set}
In this section we describe how to simulate the independent set algorithm in \cite{Thorup2001} in the k-machine model. The maximal dist-\(d\) independent set algorithm is a modified version of the parallel MIS algorithm of Luby \cite{Luby1986} which lends itself nicely to efficient simulation in the \(k\)-machine model. This implementation is described in Algorithm \ref{alg:DistdMIS}. 

% Note that we cannot necessarily find the closest vertex in \(U\) using the above scheme since we use the SSSP algorithm as a black box. But we will need this information in order to have the clients know which facility they are assigned to. In the next observation we show that this is also possible without modifying the SSSP algorithm. We will also use the next observation crucially in the MIS algorithm to bring down the running time.

\begin{Observation} \label{obs:ThorupTrick}
  Given a set \(U \subseteq V\) which is known to the machines (i.e. each machine knows which of its vertices belong to \(U\)), we can compute \(\dist(x, U \setminus \{x\})\) as well as the vertex that realizes this distance for all \(x \in V\) using \(O(\log n)\) SSSP computations and an additional \(\tilde{O}(n/k)\) rounds.
\end{Observation}
\begin{proof}
  First, breaking ties by machine ID, each vertex in \(U\) is assigned a \(\log |U|\) size bit vector. We create \(2\log |U|\) subsets of \(U\) by making two sets \(U_i^0\) and \(U_i^1\) for each bit position \(i\). The set \(U_i^b\) contains vertices whose \(i^{th}\) bit value is \(b\). Note that for all pairs of vertices \(v, w\), there is at least one set \(U_i^b\) such that \(v\) is in the set and \(w\) is not in the set. Now we run an SSSP algorithm for each \(U_i^b\) using observation \ref{obs:SetSSSP}. Now \(\dist(x, U \setminus \{x\})\) is just the smallest \(\dist(x, U_i^b)\) such that \(x \notin U_i^b\).

  For vertex \(x\) let \(d_x = \dist(x, U \setminus \{x\})\) and let \(u\) be the closest vertex to \(x\). For each vertex \(x\) the machines can locally retrieve the bit vector associated with the closest vertex in \(U \setminus \{x\}\) as follows -- The \(i^{th}\) bit of \(u\)'s vector is \(b\) if \(\dist(x, U_i^b) = d_x\) (if this condition fails for both \(b= 0\) and \(b=1\) then the \(i^{th}\) bit of \(u\)'s vector is the \(i^{th}\) bit of \(x\)'s vector)

  All machines can know the bit vectors and the vertices that they are assigned to in \(\tilde{O}(n/k)\) rounds. This can be done by machines just broadcasting the vertex ID's and vectors in each round. Therefore, the observation follows.
\end{proof}
% The algorithm consists of iterations. At the beginning of each iteration, let \(W\) be the active set of vertices and \(W_i\) be the subset of active vertices at machine \(i\). We compute estimates for \(\{|\Gamma_{\le d}(v) \cap W|\) in \(\tilde{O}(n/k)\) rounds as in Lemma \ref{lem:cohen}. Then each machine \(i\) computes a random subset \(R_i\) of size \(|W_i|/\delta\) where \(\delta = \max_{v \in W} \{|\Gamma_{\le d}(v) \cap W|\). Let \(R = \bigcup_{i \in [n]}{R_i}\)

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{MaximalDist-d-IndependentSet}\((G, W=(W_1, \dots, W_k), d)\)\label{alg:DistdMIS}}

  Each machine \(i\) sets \(U_i = \emptyset\) \\
  \While{\(W_i \neq \emptyset\) for some \(i\)} {
    Using Lemma \ref{lem:cohen} all machines compute \(\delta = (1 \pm \epsilon) \max_{v \in W} \{|B(v, d) \cap W|\) \label{alg1:SizeEstimation}\\
    Each machine \(i\) computes a random subset \(R_i\) of size \(|W_i|/\delta\) \\
    The machines compute a subset \(T\) of \(R = \bigcup_{i \in [k]}{R_i}\) containing vertices with no distance-\(d\) neighbor in \(R\) using Observation \ref{obs:ThorupTrick} \label{alg1:SSSP1} \\
    Machines add their part of \(T\) to \(U_i\) \\
    Using Observation \ref{obs:SetSSSP} machines remove all vertices in \(W_i\) within distance \(d\) of \(U\) \label{alg1:SSSP2} \\
  }
  \textbf{return} \(U\)
\end{algorithm2e}

\begin{Lemma} \label{lem:DistdMIS}
Algorithm \ref{alg:DistdMIS} finds a maximal approximate dist-\(d\) independent subset of a given set \(W \subseteq V\) in expected \(\tilde{O}(n/k)\) rounds.
\end{Lemma}
\begin{proof}
  By the analysis of proposition 3 in \cite{Thorup2001} the expected number of iterations of the while loop is \(O(\log^2 n)\). Note that we can get an over-estimate and an under-estimate of \(\delta\) by performing two queries for each vertex \(v\) namely \((v, d(1 + \epsilon))\) and \((v, d/(1 + \epsilon))\) respectively. All the SSSP computations are done using the approximate SSSP algorithm of \cite{Becker}. Since we are only performing \(poly\log(n)\) number of approximate SSSP computations, the total time spent on steps \ref{alg1:SSSP1} and \ref{alg1:SSSP2} is \(\tilde{O}(n/k)\) rounds. Using lemma \ref{lem:cohen}  with \(\epsilon\) as an arbirrarily small constant we can implement step \ref{alg1:SizeEstimation} in \(\tilde{O}(n/k)\) rounds. The lemma follows.
\end{proof}
  
\subsubsection{The Facility Location Algorithm}
% As in \cite{Thorup2001}, we will implement an approximate version of the facility location algorithm by Mettu and Plaxton. The set \(S\) returned by the algorithm is a \(3\)-approximate solution to the facility location problem. The algorithm runs in two phases --

% \begin{itemize}
% \item \textit{Phase 1.} For each \(x \in V\), we find \(r_x\) such that \(\tvalue(x, r_x) = \fcost(x)\), where  \(\tvalue(x, r) = \sum_{y \in V, dist(x,y) \le r}{(r - dist(x, y))}\).
% \item \textit{Phase 2.} Starting with \(S = \emptyset\), we visit each vertex \(x \in V\) in order of increasing \(r_x\) and add \(x\) to \(S\) if \(dist(x, S) > 4r_x\).
% \end{itemize}


We now describe how to approximately implement both phases of the \(\beta\)-MP algorithm in the \(k\)-machine model. Phase 1 is described in Algorithm \ref{algo:MettuPlaxtonPhase1} and the simulation of phase 2 is described in Algorithm \ref{algo:MettuPlaxtonPhase2}. Let \(\epsilon > 0\) be an arbitrarily small constant such that \(2\) is an integral power of \(1 + \epsilon\) for some integer \(i\). We will view all distances as rounded up to the nearest integral power of \(1 + \epsilon\). We use \(dist^{\uparrow}\) to denote this distance function, and let \(\tvalue^{\uparrow}\) denote the corresponding change in value, that is, \(\tvalue^{\uparrow}(x, r) = \sum_{y \in P, dist^{\uparrow}(x,y) \le r}{(r-dist^{\uparrow}(x, y))}\).

When we use \(r_x\) in Phase 2, all we care about is which vertices are within distance \(4r_x\) from \(x\). Let \(i\) be such that \((1 + \epsilon)^i \le r_x < (1 + \epsilon)^{i+1}\). Since 2 is an integral power of \((1 + \epsilon)\), there are no rounded distances between \(4(1 + \epsilon)^i\) and \(4(1 + \epsilon)^{i+1}\). Thus, we can round \(r_x\) down to the nearest power of \((1 + \epsilon)\), even if this implies a decrease in \(\tvalue^{\uparrow}(x, r_x)\). We denote the rounded down value of \(r_x\) as \(r_x^{\downarrow}\).

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\(\beta\)-\textsc{MettuPlaxton-Phase1}\((G)\)\label{algo:MettuPlaxtonPhase1}}
  Each machine \(j\) sets \(f_v = 0\) for all \(v \in V_j\) \\
  Let \(U = V\) and \(i = 1\) \\
  \While{\(U \not= \emptyset\)}{
    Using lemma \ref{lem:cohen} with \(W = V\) and small constant \(\epsilon\), estimate for each vertex in \(U\), the number \(q_u\) of vertices within approximate distance \((1+\epsilon)^i\) from \(u\) \\
    \For{each \(u \in U\) locally}{
      \(f_u \leftarrow f_u + ((1+\epsilon)^i - (1+\epsilon)^{i-1})q_u\) \\
      \If{\(f_u > \beta\fcost(u)\)}{
        Set \(r_u^{\downarrow} \leftarrow (1+\epsilon)^{i-1}\) \label{MP1:setrvalue} \\
        Remove \(u\) from \(U\)
      }
    }
    \(i \leftarrow i + 1\)
  }
  \textbf{return} \(r_u^{\downarrow}\) for each node \(u \in V\)

\end{algorithm2e}

The following lemma shows that Algorithm \ref{algo:MettuPlaxtonPhase1} is an approximate implementation of Phase 1 of \(\beta\)-MP.

\begin{Lemma}
  For every \(u\), there exists an \(r^{\approx}_u\) with \(\tvalue^{\uparrow}(u, r^{\approx}) = \beta \fcost(u)\) such that \(r^{\downarrow}_u/(1 + \epsilon)^2 \le r^{\approx}_u \le (1 + \epsilon)^2 r^{\downarrow}_u\).
\end{Lemma}
\begin{proof}
  If for each iteration \(i\), we had \(q_u = |B(u, (1+\epsilon)^i)|\) then \(f_u = \tvalue^{\uparrow}(u, (1+\epsilon)^i)\). By Lemma \ref{lem:cohen}, we have --
  \[\frac{1}{(1+\epsilon)} | B(u, (1+\epsilon)^{i-1}) | \le q_u \le (1 + \epsilon) | B(u, (1 + \epsilon)^{i+1}) |\]
  Similar bounds will apply for the terms \(((1+\epsilon)^i - (1+\epsilon)^{i-1})q_u\). Since, \(f_u\) is the sum of such terms, we have --
  \[\tvalue^{\uparrow}(u, (1+\epsilon)^{i-1}) \le f_u \le \tvalue^{\uparrow}(u, (1+\epsilon)^{i+1})\]
  Suppose we set \(r^{\downarrow}_u =(1 + \epsilon)^{i-1}\) in step \ref{MP1:setrvalue}. In the last iteration \(i-1\), we had \(f_u \le \beta \fcost(u)\), and hence \(\tvalue^{\uparrow}(v, (1 + \epsilon)^{i-2}) \le \beta \fcost(u)\). However, in the current iteration \(i\), we have \(f_u > \beta \fcost(u)\), and hence \(\tvalue^{\uparrow}(u, (1 + \epsilon)^{i+1}) > \beta \fcost(u)\). It follows that there is an \(r^{\approx}_u \in [(1+\epsilon)^{i-2}, (1+\epsilon)^{i+1})\) such that \(\tvalue^{\uparrow}(u, r^{\approx}_u) = \beta \fcost(u)\).
\end{proof}

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\(\beta\)-\textsc{MettuPlaxton}\((G)\)\label{algo:MettuPlaxtonPhase2}}
  \(r \leftarrow \beta\)-\textsc{MettuPlaxton-Phase1}\((G)\) \\
  \tcc{Start Phase 2 of the \(\beta\)-MP algorithm}
  Let \(S = \emptyset\) \\
  \For{\(i = 0, 1, 2, \dots\)}{
    Let \(W\) be the set of vertices \(w \in V\) across all machines with \(r_w^{\downarrow} = (1 + \epsilon)^i\) \\
    Using observation \ref{obs:ThorupTrick}, remove all vertices from \(W\) within distance \(4(1 + \epsilon)^i\) from \(S\) \\
    \(I \leftarrow \textsc{MaximalDist-d-IndependentSet}(G, W, 4(1 + \epsilon)^i)\) \\
    \(S \leftarrow S \cup I\)
  }
  \textbf{return} \(S\)

\end{algorithm2e}

\begin{theorem} \label{thm:FacLocGuarantee}
  In expected \(\tilde{O}(n/k)\) rounds, whp, Algorithms \ref{algo:MettuPlaxtonPhase1} and \ref{algo:MettuPlaxtonPhase2} find a factor \(3 + O(\epsilon)\) approximate solution \(S\) to the facility location problem. Furthermore, if \(F\) is the facility cost of the algorithm's solution, \(C\) is the algorithm's connection cost, and \(OPT\) is the optimal solution cost, then \(C + 2\beta F \le (1 + \epsilon) \sum_{j}{v_j} \le 3(1+\epsilon)OPT\)
\end{theorem}
\begin{proof}
  Recall that all \(\epsilon\) values are small constants. This means that there are at most \(\O(log_{1 + \epsilon}{nN}) = O((\log nN)) = O(\log n)\) possible values of \(i\) and hence at most that many iterations in both the algorithms (where \(N = poly(n)\) is the largest edge weight). Each iteration in both algorithms takes \(\tilde{O}(n/k)\) rounds in expectation therefore we conclude that the overall running time is \(\tilde{O}(n/k)\) rounds.

  The approximation guarantee follows from the inequality in the theorem statement. We reprove the primal-dual analysis of \cite{ArcherRSESA2003} to get the inequality.
\end{proof}

Currently our solution to the facility location problem is found in \(\tilde{O}(n/k)\) expected time. To get a solution in worst-case time, we can search \(\Theta(\log^2 n)\) solutions in parallel, returning the first solution found, and give up if no search terminates within twice the expected time. Since the solutions of the original searches were ``good'' whp, our parallel search finds a good solution whp in \(\tilde{O}(n/k)\) worst-case time.

\input{p_median_graph}

\begin{comment}
\section{MIS in \(k\) machine model in O(n/k) rounds}
This is a simple mechanism that can be used to simulate sequential greedy local algorithms like the one for MIS. In the first step, machine 1 computes an MIS on the vertices that it receives as input and sends these vertices to all machines in \(O(n/k^2)\) rounds. This is possible because machine 1 will send at most \(O(n/k)\) vertices (or \(\tilde{O}(n/k)\) vertices whp) and it can send \(k\) different vertices to its neighbors in one round and they will broadcast these vertices in the next round. Once the MIS vertices are broadcasted, machine 2 will compute an MIS on its input vertices that doesn't conflict with the MIS computed by machine 1. This will continue till machine \(k\) computes and broadcasts its local MIS and the entire procedure will take \(O(n/k)\) rounds (or \(\tilde{O}(n/k)\) rounds whp if we are in the RVP setting).
\end{comment}
