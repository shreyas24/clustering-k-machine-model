\section{Lower Bound Results}
\label{section:lowerBounds}

In this section, we derive $\tilde{\Omega}(n/k)$ lower bounds for achieving \(n^{c}\)-factor approximation algorithms (for any constant \(c\)) in the $k$-machine model for all three problems considered in this paper. Our lower bounds are inspired by the \(\Omega(n/k)\) lower bound result from \cite{KlauckNPRSODA15} for the \textit{Spanning Tree Computation} problem.

To prove the lower bounds we describe a family of lower bound graphs \(F_b(X, Y)\) where \(X\) and \(Y\) are sampled from the same distribution as the one used in \cite{KlauckNPRSODA15}. That is, \((X, Y)\) is chosen uniformly at random from $\{0, 1\}^b \times \{0, 1\}^b$, satisfying the constraint that for every $i \in [b]$, $X_i + Y_i \ge 1$. Let \(b = n/2 - 1\) and let \(L = \gamma n^{c+1}\) for some large enough constant \(\gamma\). The graph \(F_b(X, Y)\) has \(2b + 2\) vertices \(u, w, u_1, \dots, u_b, w_1, \dots, w_b\). 
See Figure \ref{figure:lowerBoundGraph}.
We fix the ID's of the vertices to be the first \(n\) natural numbers which means that each machine knows whether a vertex \(v\) is \(u, w, u_i, w_i\) just by knowing \(ID(v)\). For every \(i \in [b]\) there are three edges in the graph  of the form \(\{u, u_i\}, \{u_i, w_i\}, \{w_i, w\}\) and the weights of these edges depend on the bit values of \(X_i\) and \(Y_i\) where \(X, Y \in \{0, 1\}^b\). In particular, we assign weights to \((\{u, u_i\}, \{u_i, w_i\}, \{w_i, w\})\) as follows -- if \(X_i = 1\) and \(Y_i = 0\), the weights are \((1, 1, L)\), if \(X_i = 0\) and \(Y_i = 1\), the weights are \((L, 1, 1)\), and if \(X_i = 1\) and \(Y_i = 1\), the weights are \((1, L, 1)\). There is no weight assignment for the case when \(X_i = Y_i = 0\) because the distribution of \((X, Y)\) places no probability mass on this case.
\begin{figure}
\begin{center}
	\includegraphics[width=8cm]{lb_graph.pdf}
\end{center}
	\caption{This figure shows the structure of the graph family $F_b(X, Y)$ and how edge weights are assigned as a function of the length-$b$ binary vectors
	$X$ and $Y$.}
\label{figure:lowerBoundGraph}
\end{figure}


In the following lemma we show that any protocol that reveals \(X\) and \(Y\) to a single machine must do so by making it receive large messages from other machines. The proof is the same as the entropy argument made in Theorem 2.1 in \cite{KlauckNPRSODA15} with the added simplification that the entropy at the end of the protocol is zero. Nevertheless, we prove the lemma for completeness.

\begin{lemma} \label{lem:entropymessage}
  Let \(\Pi\) be a public-coin $\epsilon$-error randomized protocol in the $k$-machine model \((k \ge 4)\) on an \(n\)-vertex input graph sampled uniformly at random from \(F_b(X, Y)\). If a machine knows both \(X\) and \(Y\) at the end of the protocol \(\Pi\) then it must receive \(\Omega(b)\) bit messages in expectation from other machines.
\end{lemma}
\begin{proof}
  Let \(m\) be the machine that knows both \(X\) and \(Y\) at the end of the protocol. Since \(X\) and \(Y\) are encoded in the edge weights of the graph, if the machine \(m\) hosts \(u\) then it knows the string \(X\) via the edges \(\{u, u_i\}\) and similarly it knows \(Y\) if it hosts \(w\). But if \(m\) hosts both \(u\) and \(w\) then it knows \(X\) and \(Y\) before the protocol even begins. This is a bad event so we condition on the event that no machine hosts both \(u\) and \(w\) which happens with probability \(1 - 1/k\).

  Before the first round of communication, it can be shown that the entropy \(H(X, Y) \ge H(Y \mid X) = H(X \mid Y) =  2b/3\). The machine \(m\) also hosts some vertices \(u_i\) and \(w_i\) giving it access to some bits of \(X\) and \(Y\). If \(m\) hosts at most \((1 + \zeta)2b/k\) \(u_i\)'s and \(w_i\)'s for \(\zeta = 0.01\), it cannot know more than \((1 + \zeta)2b/k\) bits of \(X\) and \(Y\) by virtue of hosting these vertices. The Chernoff bound gives us that the event where \(m\) hosts more than \((1 + \zeta)2b/k\) vertices happens with probability at most \(2^{-\zeta^2 2b/(3k)} \cdot b = o(1)\), for \(b\) large enough. Therefore, this event cannot decrease the entropy by more than \(o(1)\). Hence, the entropy of \(X, Y\) given this initial information (which we denote by a random variable \(A\)) is \(H(X, Y \mid A) \ge 2b/3 - (1 + \zeta)2b/k - o(1)\). Note that if \(m\) hosts either \(u\) or \(w\) then \(A\) will contain information about either \(X\) or \(Y\) respectively but that does not affect our lower bound on the initial entropy.

  Let \(\Pi_m\) be the messages received by the machine \(m\) during the course of the protocol \(\Pi\). With probability \(1 - \epsilon\), \(m\) knows both \(X\) and \(Y\) at the end of the protocol and therefore \(H(X, Y \mid \Pi_m, A) = 0\). This means that \(I(X, Y; \Pi_m | A) = H(X, Y | A) \ge  2b/3 - (1 + \zeta)b/k - o(1)\). With the rest of the probabiliyy \(\epsilon\), we could have \(I(X, Y; \Pi_m | A) = 0\). Therefore \(|\Pi_m| \ge H(\Pi_{m}) \ge I(\Pi_m; X, Y | A) = I(X, Y; \Pi_m | A) \ge (1 - \epsilon) \cdot \Omega(b)\). This calculation was done under the assumption that different machines host \(u\) and \(w\), therefore, the expected number of messages received by \(m\) must be at least \((1 - 1/k) \cdot (1 - \epsilon) \cdot \Omega(b) = \Omega(b)\).
\end{proof}


\begin{lemma}\label{lem:lbfl}
Let \(c \ge 0\) be an arbitrary constant. For any $1 \le \alpha \le n^{c}$, every public-coin $\epsilon$-error randomized protocol in the $k$-machine model that computes an $\alpha$-factor approximate solution of \facloc\ on an $n$-vertex input graph has an expected round complexity $\tilde{\Omega}(n/k)$.
\end{lemma}
\begin{proof}
  To prove the lemma we consider the family of lower bound graphs \(F_b(X, Y)\) with the additional property that the vertices \(u\) and \(w\) have facility opening cost \(0\) and every other vertex has opening cost \(L\). 

  Consider the solution \(\mathcal{S}\) to \facloc\ where we open the vertices \(u\) and \(w\) and connect all other vertices to the closest open facility.
  % This means that for all \(i \in [b]\), \(u_i\) and \(w_i\) will be connected to \(u\) if \(X_i = 1\) and \(Y_i = 0\), to \(w\) if \(X_i = 0\) and \(Y_i = 1\), and to \(u\) and \(w\) respectively if \(X_i = 1\) and \(Y_i = 1\).
  The cost of this solution is \(O(n)\) whereas any other solution will incur a cost of at least \(\Omega(L)\). By our choice of \(L = \gamma n^{c+1}\) for a large enough constant \(\gamma\), the solution \(\mathcal{S}\) is optimal and any \(\alpha\)-approximate solution is forced to have the same form as \(\mathcal{S}\).

  After the facility location algorithm terminates, with probability \(1 - \epsilon\), the machine \(m\) hosting \(u\) will know the ID's of the \(w_i\)'s that \(u\) serves in \(\mathcal{S}\). This allows \(u\) to figure out \(Y\) because \(Y_i = 0\) if \(u\) serves \(w_i\) and \(Y_i = 1\) otherwise. Therefore, machine \(m\) knows \(X\) (since it is hosting \(u\)) and \(Y\) at the end of the algorithm. By Lemma \ref{lem:entropymessage}, machine \(m\) receives \(\Omega(b)\) bit messages in expectation throughout the course of the algorithm. This implies an \(\tilde{\Omega}(n/k)\) lower bound on the expected round complexity.
\end{proof}

\begin{lemma}
Let \(c \ge 0\) be an arbitrary constant. For any $1 \le \alpha \le n^{c}$, every public-coin $\epsilon$ error randomized protocol on a $k$-machine network that computes an $\alpha$-factor approximate solution of $\pmedian$ and $\pcenter$ on an $n$-vertices input graph has an expected round complexity of $\tilde{\Omega}(n/k)$.
\end{lemma}

\begin{proof}
  We show the lower bound for \(p=2\) on graphs that come from the family \(F_b(X, Y)\). An optimal solution in a graph from this family is to open \(u\) and \(w\) which gives a solution of cost \(O(n)\) for \(\pmedian\) and \(O(1)\) for \(\pcenter\). But, we need to be a bit more careful because the \(\pmedian\) or \(\pcenter\) algorithms can choose to open some of the \(u_i\)'s and \(w_j\)'s instead of \(u\) and \(w\) with only a constant factor increase in the cost of the solution. More specifically, there are four possible cases where we can open different pairs of vertices to get an \(O(1)\)-approximate solution -- \((u, w)\), \((u_i, w)\), \((u_i, w_j)\), and \((u, w_j)\) where \(u_i\) and \(w_j\) are connected by an edge of weight \(1\) to \(u\) and \(w\) respectively. In all these cases, each open vertex knows both \(X\) and \(Y\) at the end of the algorithm by virtue of knowing the vertices that it serves in the final solution. This is because the value of \(L\) is high enough to ensure that the two clusters formed in any \(\alpha\)-approximate solution are the same as the optimal solution no matter what centers are chosen. Therefore, we can apply Lemma \ref{lem:entropymessage} to all these cases which gives us that the machine hosting one of these vertices will receive \(\Omega(b)\) bit messages in expectation during the course of the algorithm. This means that the expected round complexity for both the algorithms is \(\tilde{\Omega}(n/k)\).

\end{proof}
