\section{A $p$-center algorithm}
\label{section:pCenter}

In this section, we describe a constant factor approximation algorithm for the $p$-center problem. It is a well-known that (see for example \cite{GonzalezTCS1985}) if $d^*$ is an optimal $p$-center cost, then any distance-$2d^*$ MIS is a $2$-approximation for the $p$-center. But since we do not know how to compute a distance-$d$ MIS efficiently in the $k$-machine model, we show in the following Lemma that an $(\epsilon, 2 \cdot (1+\epsilon)d^*)$-approximate MIS suffices to get an $O(1)$-approximation.

\begin{lemma} \label{lem:apx-pcenter}
	For a graph $G = (V, E)$, if $d^*$ is an optimal $p$-center cost, then any $(\epsilon, 2(1+\epsilon)d^*)$-approximate MIS is an $2(1+\epsilon)^2$ approximation.
\end{lemma}
\begin{proof}
	Let $O = \{o_1, o_2, \cdots, o_p\} \subseteq V$ be an optimal $p$-center solution (we assume without loss of generality that $O$ contains exactly $p$ centers). Define a partition $\{V_i\}$ of the vertex set $V$, by defining the set $V_i$ for each $o_i \in O$ as follows. For each $o_i \in O$, let $V_i \subseteq V$ be the set of vertices, for which $o_i$ is the closest center in $O$. Here we break ties arbitrarily, so that each vertex appears in exactly one of the sets $V_i$. Note that if $v \in V_j$ for some $j$, then $d(v, o_j) = d(v, O) \le d^*$.
	
	Now let $I \subseteq V$ be any $(\epsilon, 2(1+\epsilon)d^*)$-approximate MIS. We first show that $I$ is feasible, i.e. $|I| \le p$, by showing that for any $i \in \{1, 2, \cdots, p\}$, $|V_i \cap I| \le 1$. Assume this is not the case, i.e. for some $i$, there exist distinct $v_1, v_2 \in V_i \cap I$. But this implies that $d(v_1, v_2) \le d(v_1, o_i) + d(o_i, v_2) \le 2d^*$, which is a contradiction to the fact that $I$ is an $(\epsilon, 2(1+\epsilon)d^*)$-approximate MIS.
	
	Finally, the approximation guarantee follows from the definition of an approximate MIS -- for any $v \in V$, there exists an $u \in I$ such that $d(u, v) \le 2(1+\epsilon)^2 d^*$.
\end{proof}

Although we do not know the optimal $p$-center cost $d^*$ we can find it by doing a binary search to get the largest $d$ such that an $(\epsilon, 2(1+\epsilon)d)$-approximate MIS has size at most $p$. There are at most $O(\log n)$ iterations of the binary search because of our assumption that the distances bounded by $\mbox{poly}(n)$. This along with Lemma \ref{lem:DistdMIS} gives us the following theorem.

\begin{theorem}
	For any constant $\epsilon > 0$, there exists a randomized algorithm to obtain a $(2+\epsilon)$-factor approximation to the $p$-center problem in the $k$-machine model in $\tilde{O}(n/k)$ rounds w.h.p.
\end{theorem}
