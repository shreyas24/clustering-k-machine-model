\section{Facility Location in \(\tilde{O}(n/k)\) rounds}
\label{section:facilityLocation}

At the heart of our $k$-machine algorithm for \facloc\ is the well-known sequential algorithm of Mettu and Plaxton \cite{MettuPlaxtonSICOMP2003}, that computes a 
3-approximation for \facloc.
To describe the Mettu-Plaxton algorithm (henceforth, MP algorithm), we need some notation. For each real $r \ge 0$ and vertex $v$, define the ``ball'' $B(v, r)$ as
the set $\{u \in V \mid d(v, u) \le r\}$.
For each vertex $v \in V$, we define a \textit{radius} $r_v$ as the solution
$r$ to the equation $f_v = \sum_{u \in B(v, r)} (r - d(v, u))$. (Note that
$r_v$ is well-defined for every vertex $v$.)
The MP algorithm is the following simple, 2-phase, greedy algorithm:
\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{MP} Algorithm\label{alg:MP}}
	\textbf{Radius Computation Phase.} For each vertex $v \in V$, compute $r_v$.\\
	\textbf{Greedy Phase.} Consider vertices $v \in V$ in non-decreasing order of radii $r_v$.  Starting with \(S = \emptyset\), add $v$ to $S$ if $d(v, S) > 2r_v$.
\end{algorithm2e}

%\begin{theorem} \textbf{[Mettu, Plaxton \cite{MettuPlaxtonSICOMP2003}]} $cost(S) \le 3 \cdot OPT$. \end{theorem}

We will work with a slight variant of the MP algorithm, called MP-$\beta$ in \cite{ArcherRSESA2003}.
The only difference between the MP algorithm and the MP-$\beta$ algorithm is in the definition of each radius $r_v$, which is 
defined for the MP-$\beta$ algorithm, as the value $r$ satisfying $\beta \cdot f_v = \sum_{u \in B(v, r)} (r - d(v, u))$.
(Thus, the MP-$\beta$ algorithm with $\beta = 1$ is just the MP algorithm.)

There are two challenges to implementing the MP-$\beta$ algorithm efficiently in the $k$-machine model (and more generally in a distributed or 
parallel setting): (i) The calculation of the radius $r_v$ by the machine hosting vertex $v$ requires that the machine know distances
$\{d(v, u)\}_{u \in V}$; however the distance metric is initially unknown and is too costly to fully calculate, and (ii) the Greedy Phase
seems inherently sequential because it considers vertices one-by-one in non-decreasing order of radii; implementing this algorithm as-is would be too slow.
In the next three sections, we describe how to overcome these challenges and we end the section
with a complete description of our \facloc\ algorithm in the $k$-machine model.

%As in \cite{Thorup2001}, we will implement an approximate version of the facility location algorithm by Mettu and Plaxton. The set \(S\) returned by the algorithm is a \(3\)-approximate solution to the facility location problem. The algorithm runs in two phases --

\subsection{Reducing Radius Computation to Neighborhood-Size Computation}
To deal with the challenge of computing radii efficiently, without full knowledge of the metric, we use Thorup's approach \cite{Thorup2001}. 
Thorup works in the sequential setting, but like us, he assumes that the distance metric is implicitly specified via an edge-weighted graph.
He shows that it is possible to implement the MP algorithm in $\to(m)$ time on an $m$-edge graph.
In other words, it is possible to implement the MP algorithm without computing the 
full distance metric (e.g., by solving the \textit{All Pairs Shortest Path (APSP)} problem).
We now show how to translate Thorup's ideas into the $k$-machine model.
(We note here that Thorup's ideas for the \facloc\ problem have already been used to design algorithms in ``Pregel-like'' distributed systems \cite{GarimellaDGSCIKM2015}.)

For some $\epsilon > 0$, we start by discretizing the range of possible radii values using non-negative integer powers of $(1 + \epsilon)$.\footnote{Without loss of generality we assume that all numbers in the input,
i.e., $\{f_v\}_{v \in V}$ and $d(u, v)_{u, v \in V}$, are all at least 1. $O(1)$ rounds of preprocessing suffices
to normalize the input to satisfy this property. This guarantees that the minimum radius $r_v \ge 1$.}
For any vertex $v$ and for any integer $i \ge 1$, let $q_i(v)$ denote $|B(v, (1+\epsilon)^i)|$, the size of the neighborhood of 
$v$ within distance $(1+\epsilon)^i$.
Further, let $\alpha(v, r)$ denote the sum $\sum_{u \in B(v, r)} (r - d(v, u))$.
Now note that if $r$ increases from $(1+\epsilon)^i$ to $(1+\epsilon)^{i+1}$, then $\alpha(v, r)$ increases by at least 
$q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i)$. 
This implies that $\sum_{i = 0}^{t-1} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i)$ is a lower bound on $\alpha(v, (1+\epsilon)^t)$.
This observation suggests that we might be able to use, as an approximation to $r_v$, the smallest value $(1+\epsilon)^{t-1}$
for which this lower bound on $\alpha(v, (1+\epsilon)^t)$ exceeds $f_v$.
Denote by $\tilde{r}_v$, this approximation of $r_v$.
In other words, $\tilde{r}_v := (1+\epsilon)^{t-1}$, where $t \ge 1$ is the smallest integer such that 
$\sum_{i = 0}^{t-1} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) > f_v$.
It is not hard to show that $\tilde{r}_v$ is a good approximation to $r_v$ in the following
sense.
\begin{lemma}
\label{lemma:approxRadii1}
For all $v \in V$, $\frac{r_v}{1+\epsilon} \le \tilde{r}_v \le r_v(1+\epsilon)$.
\end{lemma}
\begin{proof}
  We can interpret $\sum_{i = 0}^{t-1} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i)$ as $\sum_{u \in B(v, (1+\epsilon)^t)} ((1+\epsilon)^t - d^{\uparrow}(v, u))$ where $d^{\uparrow}(v, u)$ is $d(v, u)$ rounded up to nearest power of \((1 + \epsilon)\). Thus, it is easy to see that --
  \[\alpha(v, (1+\epsilon)^{t-1}) \le \sum_{i = 0}^{t-1} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) \le \alpha(v, (1+\epsilon)^t)\]
  Note that by definition of $\tilde{r}_v$, if $\tilde{r}_v = (1+\epsilon)^{t-1}$ then $\sum_{i = 0}^{t-1} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) > f_v$ and $\sum_{i = 0}^{t-2} q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) \le f_v$. Thus, there has to exist a value $r_v \in [(1 + \epsilon)^{t-2}, (1 + \epsilon)^t]$ such that $\alpha(v, r_v) = f_v$ and this is the \(r\)-value computed by the MP algorithm. Since $\tilde{r}_v = (1+\epsilon)^{t-1}$, the lemma follows.
\end{proof}
From the definition of $\tilde{r}_v$ one can see that in order to compute these values, we only require knowledge of $q_i(v)$ for all $i \ge 0$,
rather than actual distances $d(v, u)$ for all $u \in V$.
We now state the high-level $k$-machine model algorithm (Algorithm \ref{alg:RC}) for computing $\tilde{r}_v$ values.
%For a machine $m$, let $H(m)$ denote the vertices that $m$ is a host for.
\RestyleAlgo{boxruled}  
\begin{algorithm2e}\caption{\textsc{RadiusComputation} Algorithm (Version 1)\label{alg:RC}}
	\textbf{Neighborhood-Size Computation.} Each machine $m_j$ computes $q_i(v)$, for all integers $i \ge 0$ and for all vertices $v \in H(m_j)$.\\
	\textbf{Local Computation.} Each machine $m_j$ computes $\tilde{r}_v$ locally, for all vertices $v \in H(m_j)$. (Recall that $\tilde{r}_v := (1+\epsilon)^{t-1}$ where $t \ge 1$ is the smallest integer for which $\sum_{i = 0}^t q_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) > f_v$.)
\end{algorithm2e}

In Algorithm \ref{alg:RC}, step 2 is just local computation, so we focus on Step 1 which requires the solution to the problem of computing neighborhood sizes. More specifically,  
we define the problem \textsc{NbdSizeComputation} as follows:
given an edge-weighted graph, with non-negative edge weights, compute the size of $B(v, d)$ for each vertex $v$ and positive real $d$.
The output to the problem in the $k$-machine model is required to be a distributed data structure 
(distributed among the $k$ machines) such that
each machine $m_j$ can answer any query ``What is $|B(v, d)|$?'' for any $v \in H(m_j)$
and any positive real $d$, using local computation.
Note that a ``trivial'' way of solving \textsc{NbdSizeComputation} is to solve APSP, but as
mentioned earlier this is too costly.
In the next subsection we show how to solve a ``relaxed'' version of this problem in the 
$k$-machine model in $\to(n/k)$ rounds, making only $O(\mbox{poly}(\log n))$ calls to
a $k$-machine SSSP algorithm.


\subsection{Neighborhood-Size Estimation in the $k$-machine Model}
\label{section:nbdSizeEstimation}

To solve \textsc{NbdSizeComputation} efficiently in the $k$-machine model, 
we turn to an elegant idea due to Cohen \cite{Cohen1997,Cohen2015}.
Motivated by certain counting problems, Cohen \cite{Cohen1997} presents a ``size-estimation 
framework,'' a general randomized method in the sequential setting.
%The application of Cohen's method that is of particular interest to us is the following: given a directed edge-weighted graph,
%with non-negative edge weights, compute the size of $B(v, d)$ for each vertex $v$ and positive real $d$.
%Cohen's method yields a Monte Carlo algorithm that runs in $O(m \log n + n \log^2 n)$ time on $n$-vertex, $m$-edge graphs,
%producing a compact data structure that can, for any vertex $v$ and real $d$, return an estimate of $|B(v, d)|$, with very small relative  
%error.
%We now show how to efficiently implement Cohen's size estimation framework in the $k$-machine model.
Cohen's algorithm starts by assigning to each vertex $v$ a rank \(\r(v)\) chosen uniformly from \([0, 1]\).
These ranks induce a random permutation of the vertices. 
To compute the size estimate of a neighborhood, say $B(v, d)$, for a vertex $v$ and real $d > 0$, Cohen's algorithm finds the
smallest rank of a vertex in $B(v, d)$.
It is then shown (in Section 6, \cite{Cohen1997}) that the expected value of the smallest rank in $B(v, d)$ is $1/(1 + |B(v, d)|)$.
Thus, in expectation, the reciprocal of the smallest rank in $B(v, d)$ is (almost) identical to $|B(v, d)|$.
To obtain a good estimate of $|B(v, d)|$ with high probability, Cohen simply repeats the above-described procedure
independently a bunch of times and shows the following concentration result on the 
average estimator.

\begin{theorem} \textbf{(Cohen \cite{Cohen1997})} \label{thm:cohen}
	Let $v$ be a vertex and $d > 0$ a real.
	For $1 \le i \le \ell$, let $R_i$ denote the smallest rank of a vertex in $B(v, d)$ obtained in the $i$-th repetition of
	Cohen's neighborhood-size estimation procedure. 
	Let $\hat{R}$ be the average of $R_1, R_2, \ldots, R_\ell$.
	Let $\mu = 1/(1 + |B(v, d)|)$. Then, for any $0 < \epsilon < 1$,
	$$\Pr(|\hat{R} - \mu| \ge \epsilon \mu) = \exp(-\Omega(\epsilon^2 \cdot \ell)).$$
\end{theorem}
This theorem implies that $\ell = O(\log n/\epsilon^2)$ repetitions suffice for obtaining $(1 \pm \epsilon)$-factor estimates w.h.p.~of the sizes of $B(v, d)$ for all $v$ and all $d$.

Cohen proposes a modified Dijkstra's SSSP algorithm to find smallest rank vertices in each neighborhood.
Let $v_1, v_2, \ldots, v_n$ be the vertices of the graph in non-decreasing order of rank. 
Initiate Dijkstra's algorithm, first with source $v_1$, then with source $v_2$, and so on.
During the search with source $v_i$, if it is detected that for a vertex $u$, $d(u, v_j) \le d(u, v_i)$ for some $j < i$,
then the current search can be ``pruned'' at $u$.
This is because the vertex $v_j$ has ruled out $v_i$ from being the lowest ranked vertex in any of $u$'s neighborhoods.
In fact, this is true not just for $u$, but for all vertices whose shortest paths to $v_i$ pass through $u$.
Even though this algorithm performs $n$ SSSP computations, the fact that each search is pruned by the results of
previous searches makes the overall running time much less than $n$ times the worst case running time of
an SSSP computation.
In particular, by making critical use of the fact that the random vertex ranks induce a random permutation of the vertices,
Cohen is able to show that the algorithm runs in $O(m \log n + n \log^2 n)$ time, 
on $n$-vertex, $m$-edge graphs, w.h.p.

We don't know how to implement Cohen's algorithm, as is, efficiently in the $k$-machine model.
In particular, it is not clear how to take advantage of pruning that occurs in later searches while simultaneously taking advantage of the 
parallelism provided by the $k$ machines.
A naive implementation of Cohen's algorithm in the $k$-machine model is equivalent to $n$ different SSSP computations, which is too expensive. 
Below, in Algorithm \textsc{NbdSizeEstimates} (Algorithm \ref{alg:CohenEstimates}), we show that we can reduce Cohen's 
algorithm to a polylogarithmic number of SSSP computations provided
we are willing to relax the requirement that we find the smallest rank in each neighborhood. 
We now discuss two aspects of this algorithm.
%This relaxation does not preserve approximation; sometimes the neighborhood-size estimates may be way
%off, but (as shown in Lemma \ref{lem:cohen}) neighborhood sizes are guaranteed to be ``sandwiched'' between
%neighborhood-size estimates for nearby distances.

%Before presenting our algorithm, we introduce some notation. The algorithm takes as input an error parameter \(0 < \epsilon < 1\) which can be controlled externally.
%We fix an $\epsilon'$, such that $\epsilon' > 0$ and $1 + \epsilon' \le \sqrt{1+\epsilon}$ and let $t = \lceil 2 \log_{1+\epsilon'} n \rceil$.
%Note that this means $(1+\epsilon')^{t-1}/n^2 < 1$ and $(1+\epsilon')^t/n^2 \ge 1$. Algorithm \ref{alg:CohenEstimates} describes our implementation of the neighborhood estimation framework in detail.

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{NbdSizeEstimates}\((G, \epsilon)\)\label{alg:CohenEstimates}}
	$\epsilon' := \epsilon/(\epsilon + 4)$; $t = \lceil 2 \log_{1+\epsilon'} n \rceil$; $\ell := \lceil c \log n/(\epsilon')^2 \rceil$\\
  \For{\(j := 1, \ldots, \ell\)} {
	  \textbf{Local Computation.} Each machine $m_j$ picks a rank $\r(v)$, for each vertex $v \in H(m_j)$,
	  chosen uniformly at random from $[0, 1]$. Machine $m_j$ then rounds $\r(v)$ down to the closest 
	  $(1+\epsilon')^i/n^2$ for integer $i \ge 0$\label{alg2:ChooseRanks} \\
    \For{$i := 0, 1, \ldots, t-1$}{
      $T_i := \{v \in V \mid \r(v) = (1+\epsilon')^i/n^2\}$\\
	Compute a \((1+\epsilon)\)-approximate solution to MSSP using \(T_i\) as the set of sources \label{alg2:MSSP}; let $\tilde{d}(v, T_i)$ denote the computed approximate distances\\
	\textbf{Local Computation.} Machine $m_j$ stores $\tilde{d}(v, T_i)$ for each \(v \in H(m_j)\)\\
      }
  }
\end{algorithm2e}

% \begin{enumerate}
% \item Each machine picks a rank $r(v)$, chosen uniformly at random from $[0, 1]$ for each vertex $v$ it hosts.
% \item Each machine, for each vertex $v$ it hosts, rounds rank $r(v)$ down to $(1+\epsilon)^i/n^2$,
% where  $(i+\epsilon)^i/n^2 < r(v) \le (1+\epsilon)^{i+1}/n^2$.
% it falls in. We denote the rounded rank by $\widetilde{r(v)}$.
% \item For $0 \le i \le t$, let $T_i$ denote the set of vertices with rounded rank $(1 + \epsilon)^i/n^2$.
% For each $i = 0, 1, \ldots, t$ (in this order), solve (approximate) MSSP using \(T_i\) as the set of sources.
% \end{enumerate}

\noindent
\textbf{Running time.} There are $\ell \cdot t$ calls to the subroutine solving MSSP. By Corollary \ref{theorem:SSSP},
each of these calls takes $\to(1/\mbox{poly}(\epsilon) \cdot n/k)$ rounds.
Since $\ell \cdot t = O((1/\mbox{poly}(\epsilon') \cdot \log^2 n)$, the overall round complexity of this algorithm
in the $k$-machine model is $\to(1/\mbox{poly}(\epsilon) \cdot n/k)$.

\noindent
\textbf{Answering queries.}
At the end of each iteration, each machine $m_j$ holds, for each vertex $v \in H(m_j)$, the sequence of 
distances, $\{\tilde{d}(v, T_i)\}_{i=0}^{t-1}$. Over $\ell$ repetitions, machine $m_j$ holds $\ell$ such sequences
for each vertex $v \in H(m_j)$.
Note that each distance $\tilde{d}(v, T_i)$ is associated with the rounded rank $(1 + \epsilon')^i/n^2$.
For any vertex $v \in V$ and real $d > 0$, let us denote the query ``What is the size of $B(v, d)$?'' by $Q(v, d)$.
To answer query $Q(v, d)$, we consider one of the $\ell$ sequences $\{\tilde{d}(v, T_i)\}_{i=0}^{t-1}$ and
find the smallest $i$, such that $\tilde{d}(v, T_i) \le d$, and return the rounded rank $(1+\epsilon')^i/n^2$.
To get an estimate that has low relative error, we repeat this over the $\ell$ sequences and compute the 
average \(\overline{R}\) of the
ranks computed in each iteration. 
The estimator is obtained by subtracting \(1\) from the reciprocal of \(\overline{R}\). 

The following lemma shows the correctness of Algorithm \ref{alg:CohenEstimates} in the sense
that even though we might not get an approximately correct answer to $Q(v, d)$,
the size \(|B(v, d)|\) is guaranteed to be ``sandwiched'' between the answers to 
two queries with nearby distances.
This guarantee is sufficient to ensure that the \textsc{RadiusComputation} Algorithm produces
approximately correct radii (see Section \ref{section:radiusComputation}). The proof of the following lemma appears in the full version \cite{BandyapadhyayArxiv2017}.


\begin{lemma}\label{lem:cohen}
Let $s$ denote $|B(v, d)|$ for some vertex $v$ and real $d > 0$. 
For any $0 < \epsilon < 1$, w.h.p., Algorithm \ref{alg:CohenEstimates} satisfies the following properties:
	\begin{itemize}
	\item for the query $Q(v, d/(1+\epsilon))$, the algorithm returns an answer that is at most $s(1+\epsilon)$.
	\item for the query $Q(v, d(1+\epsilon))$, the algorithm returns an answer that is at least $s/(1+\epsilon)$.
	\end{itemize}
\end{lemma}
% \begin{proof}
% Fix a particular repetition $j$, $1 \le j \le \ell$, of the algorithm and a ranking of the vertices. 
% Let $\r_j(v, d)$ denote the smallest rank in $B(v, d)$ in repetition $j$.
% To answer query $Q(v, d/(1+\epsilon))$, the algorithm examines the sequence of approximate
% distances $\{\tilde{d}(v, T_i)\}_{i=0}^{t-1}$, finds the smallest $i$ such that 
% $\tilde{d}(v, T_i) \le d/(1+\epsilon)$, and uses $R_j := (1+\epsilon')^i/n^2$ as an approximation for 
% $\r_j(v, d)$.
% Since $\tilde{d}(v, T_i) \le d/(1+\epsilon)$ there is a vertex $u \in T_i$ such that $\tilde{d}(v, u) \le d/(1+\epsilon)$.
% Since we compute a $(1+\epsilon)$-approximate solution to MSSP, the actual distance $d(v, u) \le d$. 
% Thus the rank of $u$ is at least $\r_j(v, d)$ and therefore the rounded-rank of $u$ is at least $\r_j(v, d)/(1+\epsilon')$.
% Since $u \in T_i$, the rounded-rank of $u$ is simply $R_j$ and so we get that $R_j \ge \r_j(v, d)/(1+\epsilon')$.

% Over all $\ell$ repetitions, the algorithm computes the average $\overline{R}$ of the sequence $\{R_j\}_{j=1}^\ell$.
% Letting $\overline{r}(v, d)$ denote the average of $\r_j(v, d)$ over all $\ell$ repetitions, we see that 
% $\overline{R} \ge \overline{r}(v, d)/(1+\epsilon')$. From Theorem \ref{thm:cohen}, we know that w.h.p.~$\overline{r}(v, d) 
% \ge (1 - \epsilon')/(1 + s)$. Combining these two inequalities, we get
% \begin{eqnarray*}
% \frac{1}{\overline{R}} & \le & \left(\frac{1+\epsilon'}{1-\epsilon'}\right) \cdot (s + 1)\\
% \frac{1}{\overline{R}} - 1     & \le & \left(\frac{1+\epsilon'}{1-\epsilon'}\right) \cdot s + \left(\frac{2\epsilon'}{1-\epsilon'}\right)\\ 
% 	                       & \le & \left(\frac{1+3\epsilon'}{1-\epsilon'}\right) \cdot s\\ 
% 	                       & \le & (1+\epsilon) \cdot s.
% \end{eqnarray*}
% The second last inequality above follows from the fact $s \ge 1$, since $v \in B(v, d)$.
% The last inequality follows from the setting $\epsilon' = \epsilon/(\epsilon + 4)$.

% Now we consider query $Q(v, d \cdot (1+\epsilon))$.
% Again, fix a repetition $j$, $1 \le j \le \ell$, of the algorithm and a ranking of the vertices. 
% Let $u \in B(v, d)$ be a vertex with rank equal to $\r_j(v, d)$. 
% We get two immediate implications: (i) the rounded-rank of $u$ is at most $\r_j(v, d)$ and
% (ii) $\tilde{d}(v, u) \le d(1 + \epsilon)$. Together these imply that $R_j$, the approximate
% rank computed by the algorithm in repetition $j$ is at most $\r_j(v, d)$.
% Averaging over all $\ell$ repetitions we get that $\overline{R} \le \overline{r}(v, d)$.
% Using Theorem \ref{thm:cohen}, we know that w.h.p.~$\overline{r}(v, d) 
% \le (1 + \epsilon')/(1 + s)$. Combining these two inequalities, we that get $\overline{R} \le (1 + \epsilon')/(1+s)$.
% This leads to
% \begin{eqnarray*}
% \frac{1}{\overline{R}} - 1 & \ge & \left(\frac{1 + s}{1 + \epsilon'}\right) - 1\\
%                            & \ge & \left(\frac{1 - \epsilon'}{1 + \epsilon'}\right) \cdot s\\
%                            & \ge & \frac{s}{1 + \epsilon}.
% \end{eqnarray*}
% The second last inequality follows from the fact that $s \ge 1$.
% A little bit of algebra shows that $\epsilon' = \epsilon/(\epsilon + 4)$ implies
% that $(1-\epsilon')/(1+\epsilon') \ge 1/(1+\epsilon)$ and the last inequality
% follows from this.
% \end{proof}


\subsection{Radius Computation Revisited}
\label{section:radiusComputation}

Having designed a $k$-machine algorithm that returns approximate neighborhood-size estimates
we restate the \textsc{RadiusComputation} algorithm (Algorithm \ref{alg:RC}) below.
\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{RadiusComputation} Algorithm (Version 2)\label{alg:RC2}}
	\textbf{Neighborhood-Size Computation.} Call the \textsc{NbdSizeEstimates} algorithm (Algorithm \ref{alg:CohenEstimates}) to obtain \textit{approximate} neighborhood-size estimates $\tilde{q}_i(v)$
for all integers $i \ge 0$ and for all vertices $v$.\\
	\textbf{Local Computation.} Each machine $m_j$ computes $\tilde{r}_v$ locally, for all vertices $v \in H(m_j)$ using the formula $\tilde{r}_v := (1+\epsilon)^{t-1}$ where $t \ge 1$ is the smallest integer for which $\sum_{i = 0}^t \tilde{q}_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) > f_v$.
\end{algorithm2e}

We show below that even though the computed neighborhood-sizes are approximate, in the sense
of Lemma \ref{lem:cohen}, the radii that are computed by the \textsc{RadiusComputation} algorithm (Version 2)
are a close approximation of the actual radii.
\begin{lemma} \label{lem:facloc-guarantee}
	For every $v \in V$, $\frac{r_v}{(1+\epsilon)^3} \le \tilde{r}_v \le (1+\epsilon)^3 r_v$.
\end{lemma}
\begin{proof}
  By Lemma \ref{lem:cohen}, we have the following bounds on \(\tilde{q}_i(v)\):
  \[\frac{1}{(1+\epsilon)}q_{i-1}(v)  \le \tilde{q}_i(v) \le (1 + \epsilon) q_{i+1}(v)\]
  Similar bounds will apply for the terms \(((1+\epsilon)^{i+1} - (1+\epsilon)^{i})\tilde{q}_i(v)\). Adding up the inequalities for each iteration and by using the same technique as lemma \ref{lemma:approxRadii1} we have --
  \[\alpha(v, (1+\epsilon)^{t-2}) \le \sum_{i = 0}^{t-1} \tilde{q}_i(v) \cdot ((1+\epsilon)^{i+1} - (1+\epsilon)^i) \le \alpha(v, (1+\epsilon)^{t+1})\]
  The rest of the proof follows from the proof of lemma \ref{lemma:approxRadii1}
\end{proof}

\subsection{Implementing the Greedy Phase}
Referring to the two phases in the MP Algorithm (Algorithm \ref{alg:MP}), we have now completed the implementation of the 
Radius Computation Phase in the $k$-machine model. Turning to the Greedy Phase, we note that discretizing the radius values results in $O(\log_{1+\epsilon} n)$
distinct values. If we can efficiently process each batch of vertices with the same (rounded) radius in the $k$-machine model,
that would yield an efficient $k$-machine implementation of the Greedy Phase as well.
Consider the set $W$ of vertices with (rounded) radius $\tilde{r}$.
Note that a set $I \subseteq W$ is opened as facilities by the Greedy Phase iff $I$ satisfies two properties: (i) for any two vertices
$u, v \in I$, $d(u, v) > 2 \tilde{r}$ and (ii) for any $w \in W \setminus I$, $d(w, I) \le 2 \tilde{r}$.
Thus the set $I$ can be identified by computing a \textit{maximal independent set (MIS)} in the graph $G_{\tilde{r}}[W]$,
where $G_{\tilde{r}}$ is the graph with vertex set $V$ and edge set $E_{\tilde{r}} = \{\{u, v\} \mid u, v \in V, d(u, v) \le \tilde{r}\}$.
($G_{\tilde{r}}[W]$ denotes the subgraph of $G_{\tilde{r}}$ induced by $W$.)

The well-known distributed MIS algorithm of Luby \cite{Luby1986} runs in $O(\log n)$ rounds w.h.p.~and it can be easily implemented in the $k$-machine model in
$O(n/k \cdot \log n)$ rounds.
However, Luby's algorithm assumes that the graph on which the MIS is being computed is provided explicitly. This is not possible here because
explicitly providing the edges of a graph $G_{d}$ would require pairwise-distance computation, which we're trying to avoid.
Another problem with using Luby's algorithm is that it uses randomization, where the probabilities of certain events depend on vertex-degrees.
The degree of a vertex $v$ in $G_d[W]$ is exactly $|B(v, d) \cap W|$ and this is the quantity we would need to estimate.
Unfortunately, the correctness guarantees for Algorithm \ref{alg:CohenEstimates} proved in Lemma \ref{lem:cohen} are not strong enough
to give good estimates for $|B(v, d) \cap W|$.
We deal with these challenges by instead using the \textit{beeping model} MIS algorithm of Afek et al.~\cite{AfekABHBBScience2011},
which is quite similar to Luby's algorithm except that it does require knowledge of vertex-degrees.
In Luby's algorithm vertices ``mark'' themselves at random as candidates for joining the MIS. After this step, if a marked vertex $v$ detects that a neighbor
has also marked itself, then $v$ ``backs off.''
In the current setting, this step would require every marked vertex $v$ to detect if there is \textit{another} marked vertex within distance
$d$. We use ideas from Thorup \cite{Thorup2001} to show that this problem can be solved using $O(\log n)$ calls to a subroutine that solves
\textsc{ExclusiveMSSP} (Lemma \ref{lemma:ExclusiveMSSP}).
In Luby's algorithm marked vertices that do not back off, join the MIS (permanently).
Then, any vertex $v$ that has a neighbor who has joined the MIS will withdraw from the algorithm.
Determining the set of vertices that should withdraw in each iteration requires a call to an MSSP subroutine.
Because the calls to the \textsc{ExclusiveMSSP} and MSSP subroutines return only 
approximate shortest path distances, what Algorithm \ref{alg:DistdMIS} computes is a 
relaxation of an MIS, that we call $(\epsilon,  d)$-approximate MIS.


\begin{definition}[$(\epsilon, d)$-approximate MIS]
	For an edge-weighted graph $G = (V, E)$, and parameters $d, \epsilon > 0$, an $(\epsilon, d)$-approximate MIS is a subset $I \subseteq V$ such that
	\begin{enumerate}
		\item For all distinct vertices $u, v \in I$, $d(u, v) \ge \frac{d}{1+\epsilon}$.
		\item For any $u \in V \setminus I$, there exists a $v \in I$ such that $d(u, v) \le d \cdot (1+\epsilon)$.
	\end{enumerate}
\end{definition}




\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\textsc{ApproximateMIS}\((G, W, d, \epsilon)\)\label{alg:DistdMIS}}
	Each machine $m_j$ initializes \(U_j := \emptyset\) \\
	\tcc{Let $W_j$ denote $W \cap H(m_j)$.}
	\For{$i := 0, 1, \ldots, \lceil \log n \rceil$}{
		\For{$\lceil c \log n \rceil$ iterations}{
			Each machine \(m_j\) marks each vertex $v \in W_j$ with probability $2^i/n$ \\
			\tcc{Let \(R_j \subset W_j\) denote the set of marked vertices hosted by $m_j$, let $R := \cup_{j=1}^k R_j$} 
			Solve an instance of the \textsc{ExclusiveMSSP} problem using $R$ as the set of sources (see Lemma \ref{lemma:ExclusiveMSSP}) to obtain $(1+\epsilon)$-approximate distances $\tilde{d}$ \label{alg1:MSSP1}\\
			Each machine $m_j$ computes $T_j := \{v \in R_j \mid \tilde{d}(v, R \setminus \{v\}) > d\}$ \\
    			Each Machine $m_j$ sets $U_j := U_j \cup T_j$ \\
			\tcc{Let $T := \cup_{j=1}^k T_j$}
			Solve an instance of the \textsc{MSSP} problem using $T$ as the set of sources (see Lemma \ref{lemma:MSSP}) to obtain $(1+\epsilon)$-approximate distances $\tilde{d}$ \label{alg1:MSSP2} \\
			Each machine $m_j$ computes $Q_j = \{v \in W_j \mid \tilde{d}(v, T) \le d\}$ \\
			Each machine $m_j$ sets $W_j := W_j \setminus (T_j \cup Q_j)$
		}
	}
  	\textbf{return} \(U := \cup_{j = 1}^k U_j\)
\end{algorithm2e}
We formalize the correctness of Algorithm \textsc{approximateMIS} (Algorithm \ref{alg:DistdMIS}) in the following Lemma. The proof of the Lemma is deferred to the full version \cite{BandyapadhyayArxiv2017}.

\begin{lemma} \label{lem:DistdMIS}
For a given set $W \subseteq V$, Algorithm \ref{alg:DistdMIS} finds an $(O(\epsilon), d)$-approximate MIS $I$ 
of $G[W]$ whp in \(\tilde{O}(n/k)\) rounds.
\end{lemma}
% \begin{proof}
%   We first bound the running time of the algorithm. The double nested
%   loop runs for $O(\log^2 n)$ iterations. In each iteration, Steps
%   \ref{alg1:MSSP1} and \ref{alg1:MSSP2} run in $\to(n/k)$ rounds via
%   Lemmas \ref{lemma:ExclusiveMSSP} and \ref{lemma:MSSP} respectively and
%   all other steps are local computations. This means that the overall
%   running time is \(\tilde{O}(n/k)\).

%   By the analysis in \cite{AfekABHBBScience2011} and the guarantees
%   provided by the solution to \textsc{ExclusiveMSSP}, no two vertices in
%   $W$ at distance at most $d/(1+\epsilon)$ end up in $T$.  Similarly, by
%   the analysis in \cite{AfekABHBBScience2011} and the guarantees
%   provided by the solution to \textsc{MSSP}, every vertex in $W
%   \setminus U$ is at distance at most $d(1+\epsilon)$ from $U$.  Thus
%   $U$ is an $(O(\epsilon), d)$-approximate MIS and it is computed in the
%   $k$-machine model in $\to(n/k)$ rounds.
% \end{proof}

  
\subsection{Putting It All Together}  \label{subsec:primaldual}

\RestyleAlgo{boxruled}
\begin{algorithm2e}\caption{\(\beta\)-\textsc{MettuPlaxton}\((G)\)\label{alg:MettuPlaxtonPhase2}}
  \tcc{Start Phase 1 of the \(\beta\)-MP algorithm}
  Call the \textsc{RadiusComputation} algorithm Version 2 (Algorithm \ref{alg:RC2}) to compute  approximate radii. \label{alg4:radius-computation}\\
  \tcc{Start Phase 2 of the \(\beta\)-MP algorithm}
  Let \(S = \emptyset\) \\
  \For{\(i = 0, 1, 2, \dots\)}{
    Let \(W\) be the set of vertices \(w \in V\) across all machines with \(\tilde{r}_w = \tilde{r} = (1 + \epsilon)^i\) \\
    Using Lemma \ref{lemma:MSSP}, remove all vertices from \(W\) within distance \(2(1 + \epsilon)^2\cdot \tilde{r}\) from \(S\) \label{alg4:remove-vertices}\\
    \(I \leftarrow \textsc{ApproximateMIS}(G, W, 2(1 + \epsilon)^3 \cdot \tilde{r}, \epsilon)\) \\
    \(S \leftarrow S \cup I\)
  }
  \textbf{return} \(S\)

\end{algorithm2e}

Our $k$-machine model algorithm for \facloc\ is shown in Algorithm \ref{alg:MettuPlaxtonPhase2}.
We could analyze the algorithm as done in \cite{Thorup2001} to show the constant approximation guarantee. However, we want to use this algorithm for obtaining a $p$-median algorithm in the next section. Therefore, we take an approach similar to \cite{JainVaziraniJACM2001} and \cite{ArcherRSESA2003}, to show a stronger approximation guarantee in Theorem \ref{thm:FacLocGuarantee}. 
To this end require several claims, which are along the lines of those in Thorup \cite{Thorup2001}, 
and Archer et al.~\cite{ArcherRSESA2003}. The details are deferred to the full version \cite{BandyapadhyayArxiv2017}
since they are technical and largely appear in Thorup \cite{Thorup2001} and Archer et al.~\cite{ArcherRSESA2003}. The following theorem states the running time and approximation guarantees of Algorithm \ref{alg:MettuPlaxtonPhase2}. The proof appears in the full version \cite{BandyapadhyayArxiv2017}.

\begin{theorem} \label{thm:FacLocGuarantee}
	In \(\tilde{O}(n/k)\) rounds, whp, Algorithm \ref{alg:MettuPlaxtonPhase2} finds a factor \(3 + O(\epsilon)\) approximate solution \(S\) to the facility location problem. Furthermore, if \(F\) is the facility cost of the algorithm's solution, \(C\) is the algorithm's connection cost, \(OPT\) is the optimal solution cost, and $\beta \in [1, 3/2]$ then \(C + 2\beta F \le 3(1+\epsilon)OPT\)
\end{theorem}
% \begin{proof}
% 	Algorithm \ref{alg:MettuPlaxtonPhase2} consists of two phases which correspond to the Radius Computation and Greedy Phases of the MP algorithm (Algorithm \ref{alg:MP}). We bound the running time of both these phases. There are at most $O(\log_{1 + \epsilon}{nN}) = O(\log nN) = O(\log n)$ possible values of \(i\) and hence at most $O(\log n)$ iterations in the two phases of Algorithm \ref{alg:MettuPlaxtonPhase2} (where \(N = \mbox{poly}(n)\) is the largest edge weight). In each iteration of Algorithm \ref{alg:RC2} consists of a call to Algorithm \ref{alg:CohenEstimates} which runs in \(\tilde{O}(n/k)\) rounds and hence Phase \(1\) of Algorithm \ref{alg:MettuPlaxtonPhase2}) requires \(\tilde{O}(n/k)\) rounds. Each iteration in Phase \(2\) of Algorithm \ref{alg:MettuPlaxtonPhase2} takes \(\tilde{O}(n/k)\) rounds therefore we conclude that the overall running time is \(\tilde{O}(n/k)\) rounds. The proof of the approximation guarantee is deferred to the full version \cite{BandyapadhyayArxiv2017}.
% \end{proof}