\section{Introduction}

The problem of \textit{clustering} data has a wide variety of applications in areas such as information retrieval,
bioinformatics, image processing, and social network analysis. In general, clustering is a key component of data mining 
and machine learning algorithms. Informally speaking, the objective of data clustering is to partition data into groups
such that data within each group are ``close'' to each other according to some similarity measure. 
For example, we might want to partition visitors to an online retail store (e.g., Amazon) into groups of
customers who have expressed preferences for similar products.
As the sizes of data sets have grown significantly over the last few years, it has become imperative that clustering
problems be solved efficiently in models of computation that allow multiple machines to process data in parallel.
Distributing input data across multiple machines is important not just for speeding up computation through parallelism, but also because
no single machine may have sufficiently large memory to hold a full data set. 
Motivated by these concerns, recent research has considered problems of designing clustering algorithms 
\cite{EneIMKDD2011,GarimellaDGSCIKM2015} in systems such as MapReduce \cite{DeanGhemawatCACM2010} and Pregel \cite{MalewiczABDHLCSIGMOD2010}. 
Clustering algorithms \cite{SilvaFBHCCompSurv2013} have also been designed for streaming models of computation 
\cite{AlonMSSTOC1996}. 

In this paper we present distributed algorithms for three of the most prominent clustering problems: 
the \textit{uncapacitated metric facility location} problem,
the \textit{$p$-median} problem, and the \textit{$p$-center} problem.
All three problems have been studied for several decades now and are well-known to be NP-hard.
On the positive side, all three problems have constant-factor (polynomial-time) approximation algorithms.
We consider these problems in the recently proposed \textit{$k$-machine model} \cite{KlauckNPRSODA15}, a synchronous, message-passing
model for large-scale distributed computation. 
This model cleanly abstracts essential features of systems such as Pregel \cite{MalewiczABDHLCSIGMOD2010} and Giraph (see \verb+http://giraph.apache.org/+) that have been designed for large-scale graph processing\footnote{Researchers at Facebook recently used Apache Giraph to process graphs with trillion edges \cite{ChingEKLMVLDB2015}.}, allowing researchers to prove
precise upper and lower bounds.
One of the main features of the $k$-machine model is that the input, consisting of $n$
items, is randomly partitioned across $k$ machines. Of particular interest are settings in which $n$ is 
much larger than $k$. Communication occurs via bandwidth-restricted
communication links between every pair of machines and thus the underlying communication network is a size-$k$ clique.
For all three problems, we present constant-factor approximation algorithms that run in $\tilde{O}(n/k)$ rounds in 
the $k$-machine model. We also show that these algorithms have optimal round complexity, to within polylogarithmic factors,
by providing complementary $\tilde{\Omega}(n/k)$ lower bounds for $\text{poly}(n)$-factor approximation algorithms\footnote{Throughout the paper, we use $\to(f(n))$ as a shorthand for $O(f(n) \cdot \mbox{poly}(\log n))$ and $\tilde{\Omega}(f(n))$ as a shorthand for $\Omega(f(n)/\mbox{poly}(\log n))$.}.
These are the first results on clustering problems in the $k$-machine model.

\subsection{Problem Definitions}
The input to the \textit{uncapacitated metric facility location} problem (in short, \facloc) is a set $V$ of points, a metric
$d: V \rightarrow \mathbb{R}^{+}$ that assigns distances to point-pairs, and a \textit{facility opening cost} $f: V \rightarrow \mathbb{R}^+$ associated
with each point $v \in V$. 
The problem is to find a subset $F \subseteq V$ of points to open (as ``facilities'') so as to minimize
the objective function  $\sum_{i \in F} f_i + \sum_{j \in V} d(j, F)$, where
$d(j, F) = \min_{x \in F} d(j, x)$.
(For convenience, we abuse notation and use $f_i$ instead of $f(i)$.)
\facloc\ is NP-hard and is in fact hard to approximate with an approximation factor better than 1.463 \cite{GuhaKhullerSODA1998}.
There are several well-known constant-factor approximation algorithms for \facloc\ including the primal-dual
algorithm of Jain and Vazirani \cite{JainVaziraniJACM2001} and the greedy algorithm of Mettu and Plaxton \cite{MettuPlaxtonSICOMP2003}.
The best approximation factor currently achieved by an algorithm for \facloc\ is 1.488 \cite{LiICALP2011}.

The input to the \textit{$p$-median} problem (in short, \pmedian) is a set $V$ of points and a metric
$d: V \rightarrow \mathbb{R}^{+}$ that assigns distances to point-pairs, and a positive integer $p$.
The problem is to find a subset $F \subseteq V$ of exactly $p$ points to open (as ``facilities'') so as to minimize
the objective function  $\sum_{j \in V} d(j, F)$.
\pmedian\ is NP-hard and and is in fact hard to approximate with an approximation factor better than $1 + \frac{2}{e}
\approx 1.736$ \cite{JainMSSTOC2002}.
A well-known approximation algorithm for the $p$-median problem is due to Jain and Vazirani \cite{JainVaziraniJACM2001}, who present
a 6-approximation algorithm. This approximation factor has been improved by subsequent results -- see 
\cite{AryaGKMMPSTOC2001}, for example.
The input to the \textit{$p$-center} problem (in short, \pcenter) is the same as the input to \pmedian,
but the objective function that is minimized is $\max_{j \in V} d(j, F)$.
Like \facloc\ and \pmedian, the \pcenter\ problem is not only NP-hard, it is in fact hard to approximate 
with an approximation factor strictly better than 2 \cite{GonzalezTCS1985}.
There is also an optimal 2-approximation algorithm for this problem \cite{GonzalezTCS1985} obtained via 
a simple, greedy technique called \textit{farthest first traversal}.

In all three problems, it is assumed that each point is ``connected'' to the nearest open facility. 
So an open facility along with the ``clients'' that are connected to it forms a cluster. 


\subsection{The $k$-machine Model and Input-Output Specification}
Let $n$ denote $|V|$. 
The $k$-machine model is a message-passing, synchronous model of distributed computation.
Time proceeds in \textit{rounds} and in each round, each of the $k$ machine performs
local computation and then sends, possibly distinct, messages to the remaining $k-1$ machines.
A fundamental constraint of the $k$-machine model is that each message is required to be small;
as is standard, we assume here that each message is of size $O(\log n)$ bits.
It is assumed that the $k$ machines have unique \texttt{ID}s, that are represented by 
$O(\log n)$-bit strings.

As per the random partition assumption of $k$-machine model \cite{KlauckNPRSODA15},
the points in $V$ are distributed uniformly at random across the $k$ machines.
This results in $\to(n/k)$ points per machine, with high probability (w.h.p.)\footnote{We use ``with high
probability'' to refer to probability that is at least $1 - 1/n^c$ for any constant $c \ge 1$.}.
We use $m_j$, $1 \le j \le k$, to denote the machines and $H(m_j)$ to denote the subset of points ``hosted''
by $m_j$.
The natural way to distribute the rest of the input, namely $d: V \times V \rightarrow \mathbb{R}^{+}$ and $f: V 
\rightarrow \mathbb{R}^{+}$ (in the case of \facloc), is for each machine $m_j$ to be given $f_i$ and $\{d(i, x)\}_{x \in V}$ for each
point $i \in H(m_j)$.
The distribution of $f$ in this manner is fine, but there is a problem with distributing $\{d(i, x)\}_{x\in V}$
in this manner.
Since $n$ is extremely large, it is infeasible for $m_j$ to hold the $\tilde{\Omega}(n^2/k)$ elements in 
$\cup_{i \in H(m_j)} \{d(i, x)\}_{x \in V}$. (Recall that $n >> k$.)
In general, this explicit knowledge of the metric space consumes too much memory, even when divided among
$k$ machines, to be feasible.
So we make, what we call the \textit{graph-metric assumption}, that the metric $d: V \times V \rightarrow \mathbb{R}^+$
is specified implicitly by an edge-weighted graph with vertex set $V$.
Let $G = (V, E)$ be the edge-weighted graph with non-negative edge weights representing the metric $d: V \times 
V \rightarrow \mathbb{R}^+$. Thus for any $i, j \in V$, $d(i, j)$ is the shortest path distance between points $i$ 
and $j$ in $G$.

Klauck et al.~\cite{KlauckNPRSODA15} consider a number of graph problems in the $k$-machine model
and we follow their lead in determining the initial distribution of $G$ across machines.
For each point $i \in H(m_j)$, machine $m_j$ knows all the edges in $G$ incident on $i$ and 
for each such edge
$(i, x)$, machine $m_j$ knows the \texttt{ID} of the machine that hosts $x$. 
Thus, $\sum_{i \in H(m_j)} \mbox{degree}_G(i)$ elements are needed at each machine $m_j$ to represent
the metric space and if $G$ is a sparse graph, this representation can be quite compact.

The graph-metric assumption fundamentally affects the algorithms we design. Since the metric $d$ is provided 
implicitly, via $G$, access to the metric is provided through shortest path computations on $G$.
In fact, it turns out that these shortest path computations are the costliest part of our algorithms.
One way to view our main technical contribution is this: we show that for all three clustering problems,
there are constant-factor approximation algorithms that only require a small (i.e., polylogarithmic) number of calls to a subroutine that
solves the \textit{Single Source Shortest Path (SSSP)} problem.

For all three problems, the output consists of $F$, the set of open facilities, and
connections between clients (i.e., points that have not been open as facilities) and their 
nearest open facilities. More precisely, for any machine $m_j$ and any point $i \in H(m_j)$:
\begin{itemize}
\item If $i \in F$, then $m_j$ knows that $i$ has been opened as a facility and furthermore
$m_j$ knows all $(x, \texttt{ID}_x)$-pairs where $x$ is a client that connects to $i$ and $\texttt{ID}_x$ is the \texttt{ID} of the machine hosting $x$.
\item If $i \in V \setminus F$, then $m_j$ knows that $i$ is a client and it also knows 
the $(x, \texttt{ID}_x)$ pair, where $x$ is the open facility that $i$ connects to and
$\texttt{ID}_x$ is the \texttt{ID} of the machine hosting $x$.
\end{itemize}

%Introduce the $k$-machine model \cite{KlauckNPRSODA15,PanduranganRSSPAA2016,PanduranganRSarxiv16} and explain why it is interesting.

\subsection{Our Results}
We first prove $\Omega(n/k)$ lower bounds (in Section \ref{section:lowerBounds}) for \facloc, \pmedian, and \pcenter.
For each problem, we show that obtaining an $\alpha$-approximation algorithm in the $k$-machine model, for any $\alpha = (\mbox{poly}(n))$, 
requires at least $\tilde{\Omega}(n/k)$ rounds.
In the subsequent three sections, we present $\to(n/k)$-round, constant-factor approximation algorithms for the \facloc, \pmedian,
and \pcenter\ problem, respectively.
Our lower bound results show that our algorithms have optimal round complexity,
at least up to polylogarithmic factors.

We bring to bear a wide variety of old and new techniques to derive our upper bound results including the facility location
algorithm of Mettu and Plaxton \cite{MettuPlaxtonSICOMP2003}, the fast version of this algorithm due to Thorup \cite{Thorup2001}, 
the neighborhood-size estimation framework of 
Cohen \cite{Cohen1997,Cohen2015}, the $p$-median Lagrangian relaxation algorithm of Jain and Vazirani \cite{JainVaziraniJACM2001} and the recent distributed shortest path algorithms 
due to Becker et al.~\cite{BeckerKKLarxiv16}.
In our view, an important contribution of this paper is to show how all of these techniques can be utilized in the $k$-machine model.

\subsection{Related Work}
\label{section:relatedWork}

Following Klauck et al.~\cite{KlauckNPRSODA15}, two other papers \cite{PanduranganRSarxiv16,PanduranganRSSPAA2016} have studied
graph problems in the $k$-machine model. In \cite{PanduranganRSSPAA2016}, the authors present an $\tilde{O}(n/k^2)$-round algorithm
for graph connectivity, which then serves as the basis for $\tilde{O}(n/k^2)$-round algorithms for other graph problems such as
minimum spanning tree (MST) and approximate min-cut. The upper bound for MST does not contradict the $\tilde{Omega}(n/k)$ lower
bounds shown for this problem in Klauck et al.~\cite{KlauckNPRSODA15} because Pandurangan et al.~\cite{PanduranganRSSPAA2016} use
a more relaxed notion of how the output MST is represented. Specifically, at the end of the algorithm in \cite{PanduranganRSSPAA2016}
every MST edge is known to \textit{some} machine, whereas Klauck et al.~\cite{KlauckNPRSODA15} use the stricter requirement
that every MST edge be known to the machines hosting the two end points of the edge.
This phenomena in which the round complexity of the problem is quite sensitive to the output representation may be
relevant to our resuts as well and is further discussed in Section \ref{section:conclusions}. 

Earlier in this section, we have mentioned models and systems for large-scale parallel computation such as MapReduce and Pregel.
Another model of large-scale parallel computation, that seems essentially equivalent to the $k$-machine model is
the \textit{Massively Parallel Computation model (MPC)} which according to \cite{YaroslavtsevVarxiv2017} is the
``most commonly used theoretical model of computation on synchronous large-scale
data processing platforms such as MapReduce and Spark.'' 

%  LocalWords:  MapReduce Pregel uncapacitated NP Giraph poly Jain
%  LocalWords:  polylogarithmic Vazirani Mettu Plaxton Klauck Thorup
%  LocalWords:  Becker
