\section{Geometric Clustering}

\subsection{Facility Location}

\subsection{$p$-centers}
In this section, we briefly discuss an $\tilde{O}(\max\{\frac{p}{k}, k\})$ round, $O(1)$-approximation algorithm for $p$-center problem, where the set of input points lie in Euclidean space of constant dimension $d$.

It is a well-known fact that the optimal $p$-center cost when the centers are required to be chosen from the given set of points is at most $2$ times the optimal $p$-center cost, when the centers can be any points from $\mathbb{R}^d$. We note that the corresponding version for the $p$-median problem was observed in \cite{Guha2000}. For completeness, we state and prove the version for the $p$-center problem with suitable modifications.

\begin{Lemma}
	Let $P \subset \mathbb{R}^d$ be the set of input points. Let $OPT$ be the optimal $p$-center cost when the set of $p$ centers has to be a subset of $P$, and $OPT'$ be the optimal $p$-center cost when the $p$ centers can be any subset of $\mathbb{R}^d$. Then, 
	$$OPT' \le OPT \le 2 \cdot OPT'.$$
\end{Lemma}
\begin{proof}
	Firstly, note that since $P \subset \mathbb{R}^d$, the first inequality follows trivially.
	
	Now, consider a set of $p$ centers $C \subset \mathbb{R}^d$ that realizes the optimal cost $OPT'$. Now, the cost of this solution is $OPT' = \max_{i \in P} d(i, C) = \max_{i \in P} \min_{j \in C} d(i, j)$. For all centers $j \in C$, let $P_j \subseteq P$ be the set of points for which $j$ is the closest center. Now, let $i_j = \arg\min_{i \in P_j} d(i, j)$ be the closest point from the center $j$. Note that for any $i' \in P_j$, $d(i, i') \le d(i, j) + d(j, i') \le 2 \cdot OPT'$. Therefore, the set $P' = \{i_j \in P | j \in C\}$ has cost at most $2 \cdot OPT'$. Finally, note that since $P' \subseteq P$ is a feasible solution, $OPT \le \text{cost}(P') \le 2 \cdot OPT'$.
\end{proof}

Therefore, in the rest of the section, we restrict the solutions that we consider to the set of input points $P \subset \mathbb{R}^d$. The following $2$ approximation for $p$-centers is well-known \cite{Gonzalez1985}.

\begin{Lemma}
	If $d^*$ is the cost of an optimal solution (where the centers have to be chosen from the set of input points), then any maximal distance-$2d^*$ independent set is a $2$ approximation for the $p$-center problem.
\end{Lemma}

It is straightforward to implement this greedy algorithm in the $k$-machine model. First, we assume that the maximum inter-point distance is polynomial in $n$, and the minimum inter-point distance is $1$. We also assume that $D$, an upper bound on maximum inter-point distance can be easily computed. We proceed as in \cite{Thorup2001}, to find a distance $d'$ such that a maximal distance-$2d'$ independent set uses at most $p$ centers, whereas a maximal distance-$d'$ independent set uses more than $p$ centers. For some $\epsilon \in (0, 1]$, we find such a value by computing a maximal distance-$d$ independent set, and reducing it to $d/(1+\epsilon)$ in the next iteration, until the number of centers exceeds $p$. We start by setting $d = D$. In the following, we show how to compute a maximal distance-$d$ independent set in $\tilde{O}(\max\{\frac{p}{k}, k\})$ rounds.

Order the $k$-machines arbitrarily as $M_1, M_2, \cdots, M_k$, and let the sets of points hosted by each of them by $P_1, P_2, \cdots, P_k$ respectively. Initially, $M_1$ computes a maximal distance-$2d$ independent set $I_1 \subseteq P_1$ of the input points that it contains, where $d$ is the current guess for the optimal cost. For this, it may use the simple greedy algorithm -- initializing $I_1$ to an empty set, and all points in $P_1$ as unmarked, add an arbitrary unmarked point from $P_1$, and mark all points from $P_1$ that are within distance $2d$ from any point in $I_1$. Finally, it broadcasts the set $I_1$ to all machines in $O(\lceil |I_1|/k\rceil)$ rounds \footnote{We can use a following simple scheme for this -- Machine $M_1$ orders the $O(|I_1|)$ bits that it has to send into chunks of $O(1)$ bits that can be sent in one round. In first round, it sends $k$ such chunks to machines $M_1, \cdots, M_k$. In the second round, each machine broadcasts the chunks it received in the second round. In this way, to broadcast $O(|I_1|)$ bits, it takes $O(\lceil |I_1|/k \rceil)$ rounds.}. 

Machine $M_j \in \{M_2, \cdots, M_k\}$ also uses a similar scheme, the only difference being that it initially marks all points from $P_j$ that are within distance $2d$ from any of the points from the independent set computed as of now, i.e. $I_1 \cup I_2 \cup \cdots \cup I_{j-1}$. If the size of set $I_1 \cup I_2 \cdots I_j$ exceeds $p$ at this point, it stops and broadcasts failure, which implies that the independent set computed for the previous value of $d$ is the approximate solution (here we assume that the machines remember the candidate solutions computed in this as well as the previous value of $d$). Otherwise (i.e. the number of centers opened is $\le p$), it broadcasts $I_j$ in $O(\lceil |I_j|/k\rceil)$ rounds.

For any value of $d$ except the last, it holds that $\sum_{j = 1}^k |I_j| \le p$. On the other hand, for the value of $d$ for which we detect failure, we stop as soon as the number of centers becomes more than $p$. Therefore, assume for notational convenience that $|I_j| = |I_{j+1}| = \cdots = |I_k| = 0$, where $M_j$ is the machine that detects the failure. Therefore, the number of rounds for this value of $d$ is $O(\sum_{j = 1}^k \lceil |I_j|/k \rceil) = O(\sum_{j = 1} (|I_j|/k) + 1) = O(p/k + k) = O(\max\{\frac{p}{k}, k\})$. Finally, note that each time we reduce the value of $d$ by a factor of $(1+\epsilon)$, so the final guess may be at most $(1+\epsilon)$ times that of the optimal cost. Finally, there are $\log_{1+\epsilon}(d)$ iterations, which is $O(\log n)$, if $\epsilon$ is a constant. We summarize this in the following theorem.

\begin{theorem}
	There exists a $\tilde{O}(\max\{\frac{p}{k}, k\})$ round algorithm in the $k$-machine model for the $p$-center problem, which takes input a set $P \subset \mathbb{R}^d$ of points, and a constant $\epsilon > 0$, and returns a $(4+\epsilon)$-approximation.
\end{theorem}




\subsection{An \(\tilde{O}(p/k)\)-round algorithm for \(p\)-median using coresets}
We are working in the standard \(k\)-machine model where there is a clique network of \(k\) nodes. 
Each machine is given \(n/k\) points in \(\mathbb{R}^d\) where \(d\) is some constant and the objective is to compute 
a \(p\)-median clustering of the \(n\) points.
The cluster centers can be any points in \(\mathbb{R}^d\) since this version will only decrease the objective by at most a factor of two.

We will use coresets to communicate the information about points in each machine. An \(\epsilon\)-coreset for a set of points \(P\) 
with respect to a center-based cost function is a set of points \(C\) and a set of weights \(w : S \rightarrow \mathbb{R}\) 
such that for any set of \(p\) centers \(x\) --
\[(1-\epsilon)cost(P,x) \le \sum_{y \in C}{w(y)cost(y,x)} \le (1+\epsilon)cost(P,x)\]

Coresets have the following two useful properties which we will use in our algorithm --
\begin{itemize}
\item If \(C_1\) and \(C_2\) are the \(\epsilon\)-coresets for disjoint sets \(P_1\) and \(P_2\) respectively, then \(C_1 \cup C_2\) is a \(\epsilon\)-coreset for \(P_1 \cup P_2\).
\item If \(C_1\) is \(\epsilon\)-coreset for \(C_2\), and \(C_2\) is a \(\delta\)-coreset for \(C_3\),then \(C_1\) is a \((\epsilon + \delta + \epsilon\delta)\)-coreset for \(C_3\)
\end{itemize}

In particular, we will (locally) use a modified version of the \(O(p/\epsilon^2)\) \(\epsilon\)-coreset construction of 
\cite{Feldman2011} that works on weighted point sets. This trivially gives an \(O(p)\)-round \((1 + \epsilon)\)-approximation scheme to the \(p\)-median problem in which each of the local coresets are aggregated to a single machine and that machine computes an exact solution on the coreset points that it receives.

The algorithm proceeds in \(O(\log k)\) phases. Initially, all machines are active. In phase \(i\) the active machines have \((3i \cdot \epsilon)\)-coresets of size \(O(p/\epsilon^2)\) each of which represent disjoint sets of points such that the union of all these points is the entire point set. The active machines form disjoint pairs. For each pair \((s, t)\), machine \(s\) sends its coreset points \(S\) to machine \(t\) in \(O(p/k)\) rounds (this can be done simultaneously for all pairs by using Lenzen's routing protocol). Once machine \(t\) receives the coreset points, it computes an \((\epsilon/3i)\)-coreset on the set \(S \cup T\) where \(T\) is the coreset \(t\) has at the beginning of the phase. This means that machine \(t\) has an \((3(i+1)\epsilon)\)-coreset on the points that \(s\) and \(t\) are responsible for. The machine \(s\) deactivates itself and the algorithm proceeds to phase \(i+1\).

Since the number of machines halves in each phase, after \(\log k\) phases, one machine will contain an \((\epsilon \cdot 3\log k)\)-coreset of all the points. This means that we can set \(\epsilon\) to be \(\epsilon'/3\log{k}\). This will make all the coresets have size \(\tilde{O}(p)\) and the routing steps will take \(\tilde{O}(p/k)\) rounds. Therefore over all phases we will require  \(\tilde{O}(p \log k /k) = \tilde{O}(p/k)\) rounds.
